﻿using DataService;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using VG.Common;

namespace TestLib
{
    class Program
    {
        static void Main(string[] args)
        {
            //string searchString = "Title: {MyTitle} Incident Description: {MyDescription} Incident Level: {MyLevel}";
            //Regex r1 = new Regex(@"{\w*\}");
            //MatchCollection match = r1.Matches(searchString);
            //for (int i = 0; i < match.Count; i++)
            //{
            //    Console.WriteLine(match[i].Value);
            //}
            //Console.Read();



            //string searchString = "Họ và tên: {MyName} Địa chỉ: {MyAddress} Công việc: {MyJob}";
            //List<string> lstParam = TemplateEngine.GetListDynamicParams(searchString);
            //foreach(string param in lstParam)
            //{
            //    Console.WriteLine(param);
            //}

            //Console.WriteLine("****************************");
            //List<ParamValue> data = new List<ParamValue>();
            //data.Add(new ParamValue() { strParam = "{MyName}", strValue = "Lê Mạnh Quân" });
            //data.Add(new ParamValue() { strParam = "{MyAddress}", strValue = "Ecopark" });
            //data.Add(new ParamValue() { strParam = "{MyJob}", strValue = "LTV" });

            //string _resultTemp = TemplateEngine.FillTemplate(data, searchString);
            //Console.WriteLine(_resultTemp);


            //long percentStock = ((long)50000000 * 100) / (long) 2328818410;
            //Console.WriteLine(percentStock);


            #region GenBarcode Basse64
            //List<tblStockholderInvite> listNoneBarcode = DataAccess.GetStockholderInviteNoneBarcode(6);
            //Console.WriteLine(listNoneBarcode.Count().ToString());
            //foreach (tblStockholderInvite cus in listNoneBarcode)
            //{
            //    Console.WriteLine("Gen for:"+cus.SHID);
            //    cus.BarCode = CommonHelper.BarcodeToBase64(CommonHelper.HashEncodeId(cus.SHID));
            //}
            //DataAccess.GenBarcodeStockHolderInviteBulk(listNoneBarcode);
            #endregion

            #region GenPDF_TemplateEngine
            //tblStockholderEvent objEvent = DataAccess.GetEventByID(6);
            //tblMasterPL objPL = DataAccess.GetPLByID(objEvent.PL_ID);

            //string TemplateVNSource = InlineTemplateEngine.GetTemplate("InviteVN.html");
            //string TemplateENSource = InlineTemplateEngine.GetTemplate("InviteEN.html");
            //string _ressult = "";

            //int webPageWidth = 1024;
            //SelectPdf.PdfPageSize pageSize = (SelectPdf.PdfPageSize)Enum.Parse(typeof(SelectPdf.PdfPageSize), "A4", true);
            //SelectPdf.PdfPageOrientation pdfOrientation = (SelectPdf.PdfPageOrientation)Enum.Parse(typeof(SelectPdf.PdfPageOrientation), "Portrait", true);
            //SelectPdf.HtmlToPdf converter = new SelectPdf.HtmlToPdf();
            //converter.Options.PdfPageSize = pageSize;
            //converter.Options.PdfPageOrientation = pdfOrientation;
            //converter.Options.WebPageWidth = webPageWidth;


            //List<tblStockholderInvite> listcus = DataAccess.GetStockholderInvite(6).Where(o => o.sType == 2).Skip(0).Take(1).ToList();
            //List<tblStockholderInvite> listcus = DataAccess.GetStockholderInvite(6).Skip(0).Take(1).ToList();


            //Console.WriteLine("----Start Gen {0} thu moi----", listcus.Count());
            //tblStockholderInvite cus = DataAccess.GetStockholderInviteByID(93369);
            //foreach (tblStockholderInvite cus in listcus)
            //{

            //    List<ParamValue> fillData = new List<ParamValue>();
            //    if (cus.sType == 1)
            //    {
            //        fillData.Add(new ParamValue()
            //        {
            //            strParam = "{PL_NAME_UPPER}",
            //            strValue = objPL.PL_Name.ToUpper()
            //        });
            //        fillData.Add(new ParamValue()
            //        {
            //            strParam = "{PL_NAME}",
            //            strValue = objPL.PL_Name
            //        });
            //        fillData.Add(new ParamValue()
            //        {
            //            strParam = "{EVENT_NAME_UPPER}",
            //            strValue = objEvent.Event_Name.ToUpper()
            //        });
            //        fillData.Add(new ParamValue()
            //        {
            //            strParam = "{EVENT_NAME}",
            //            strValue = objEvent.Event_Name
            //        });
            //        fillData.Add(new ParamValue()
            //        {
            //            strParam = "{INVITE_FULLNAME}",
            //            strValue = cus.FullName
            //        });
            //        fillData.Add(new ParamValue()
            //        {
            //            strParam = "{INVITE_ADDRESS}",
            //            strValue = cus.Address
            //        });
            //        fillData.Add(new ParamValue()
            //        {
            //            strParam = "{INVITE_ID_CODE}",
            //            strValue = CommonHelper.HashEncodeId(cus.SHID)
            //        });
            //        fillData.Add(new ParamValue()
            //        {
            //            strParam = "{INVITE_BARCODE}",
            //            strValue = cus.BarCode
            //        });
            //        fillData.Add(new ParamValue()
            //        {
            //            strParam = "{EVENT_LOCATION}",
            //            strValue = objEvent.Location
            //        });
            //        fillData.Add(new ParamValue()
            //        {
            //            strParam = "{EVENT_FROMDATE}",
            //            strValue = VnDateConvert.ConvertDateTimeTempVN(objEvent.FromDate)
            //        });

            //        _ressult = TemplateEngine.FillTemplate(fillData, TemplateVNSource);
            //    }
            //    else
            //    {
            //        fillData.Add(new ParamValue()
            //        {
            //            strParam = "{PL_NAME_UPPER}",
            //            strValue = objPL.PL_Name_En.ToUpper()
            //        });
            //        fillData.Add(new ParamValue()
            //        {
            //            strParam = "{PL_NAME}",
            //            strValue = objPL.PL_Name_En
            //        });
            //        fillData.Add(new ParamValue()
            //        {
            //            strParam = "{EVENT_NAME_UPPER}",
            //            strValue = objEvent.Event_Name_En.ToUpper()
            //        });
            //        fillData.Add(new ParamValue()
            //        {
            //            strParam = "{EVENT_NAME}",
            //            strValue = objEvent.Event_Name_En
            //        });
            //        fillData.Add(new ParamValue()
            //        {
            //            strParam = "{INVITE_FULLNAME}",
            //            strValue = cus.FullName
            //        });
            //        fillData.Add(new ParamValue()
            //        {
            //            strParam = "{INVITE_ADDRESS}",
            //            strValue = cus.Address
            //        });
            //        fillData.Add(new ParamValue()
            //        {
            //            strParam = "{INVITE_ID_CODE}",
            //            strValue = CommonHelper.HashEncodeId(cus.SHID)
            //        });
            //        fillData.Add(new ParamValue()
            //        {
            //            strParam = "{INVITE_BARCODE}",
            //            strValue = cus.BarCode
            //        });
            //        fillData.Add(new ParamValue()
            //        {
            //            strParam = "{EVENT_LOCATION}",
            //            strValue = objEvent.Location_En
            //        });
            //        fillData.Add(new ParamValue()
            //        {
            //            strParam = "{EVENT_FROMDATE}",
            //            strValue = VnDateConvert.ConvertDateTimeTempEN(objEvent.FromDate)
            //        });

            //        _ressult = TemplateEngine.FillTemplate(fillData, TemplateENSource);
            //    }


            //    SelectPdf.PdfDocument docuri = converter.ConvertHtmlString(_ressult);
            //    if (cus.sType == 1)
            //        docuri.Save("D:/VRE_INVI2/VN/" + CommonHelper.HashEncodeId(cus.SHID) + "_" + CommonHelper.UnicodeToKoDauAndGach(cus.FullName) + "_Invi.pdf");
            //    else
            //        docuri.Save("D:/VRE_INVI2/EN/" + CommonHelper.HashEncodeId(cus.SHID) + "_" + CommonHelper.UnicodeToKoDauAndGach(cus.FullName) + "_Invi.pdf");

            //    Console.WriteLine("--DONE for " + CommonHelper.UnicodeToKoDauAndGach(cus.FullName));
            //}

            //Console.WriteLine("----End Gen thu moi----");
            #endregion


            #region Check file con thieu trong thu muc de Gen bu
            //tblStockholderEvent objEvent = DataAccess.GetEventByID(12);
            //tblMasterPL objPL = DataAccess.GetPLByID(objEvent.PL_ID);

            //string TemplateVNSource = InlineTemplateEngine.GetTemplate("InviteEN.html");

            //int webPageWidth = 1024;
            //SelectPdf.PdfPageSize pageSize = (SelectPdf.PdfPageSize)Enum.Parse(typeof(SelectPdf.PdfPageSize), "A4", true);
            //SelectPdf.PdfPageOrientation pdfOrientation = (SelectPdf.PdfPageOrientation)Enum.Parse(typeof(SelectPdf.PdfPageOrientation), "Portrait", true);
            //SelectPdf.HtmlToPdf converter = new SelectPdf.HtmlToPdf();
            //converter.Options.PdfPageSize = pageSize;
            //converter.Options.PdfPageOrientation = pdfOrientation;
            //converter.Options.WebPageWidth = webPageWidth;

            //List<tblStockholderInvite> listVN = DataAccess.GetStockholderInviteStype(12);
            //Console.WriteLine("file in DB {0}", listVN.Count());

            //int countMiss = 0;

            //foreach (tblStockholderInvite cus in listVN)
            //{
            //    string _fileName = "D:/VIC_INVI/FixTemplateEn/EN/" + CommonHelper.UnicodeToKoDauAndGach(cus.FullName) + "_" + CommonHelper.HashEncodeId(cus.SHID) + "_Invi.pdf";
            //    //string _fileName = CommonHelper.HashEncodeId(cus.SHID) + "_" + CommonHelper.UnicodeToKoDauAndGach(cus.FullName) + "_Invi.pdf";
            //    if (!File.Exists(_fileName))
            //    {
            //        countMiss++;
            //        List<ParamValue> fillData = new List<ParamValue>();
            //        fillData.Add(new ParamValue()
            //        {
            //            strParam = "{PL_NAME_UPPER}",
            //            strValue = objPL.PL_Name.ToUpper()
            //        });
            //        fillData.Add(new ParamValue()
            //        {
            //            strParam = "{PL_NAME}",
            //            strValue = objPL.PL_Name
            //        });
            //        fillData.Add(new ParamValue()
            //        {
            //            strParam = "{EVENT_NAME_UPPER}",
            //            strValue = objEvent.Event_Name.ToUpper()
            //        });
            //        fillData.Add(new ParamValue()
            //        {
            //            strParam = "{EVENT_NAME}",
            //            strValue = objEvent.Event_Name
            //        });
            //        fillData.Add(new ParamValue()
            //        {
            //            strParam = "{INVITE_FULLNAME}",
            //            strValue = cus.FullName
            //        });
            //        fillData.Add(new ParamValue()
            //        {
            //            strParam = "{INVITE_ADDRESS}",
            //            strValue = cus.Address
            //        });
            //        fillData.Add(new ParamValue()
            //        {
            //            strParam = "{INVITE_MOBILE}",
            //            strValue = cus.Mobile
            //        });
            //        fillData.Add(new ParamValue()
            //        {
            //            strParam = "{INVITE_ID_CODE}",
            //            strValue = CommonHelper.HashEncodeId(cus.SHID)
            //        });
            //        fillData.Add(new ParamValue()
            //        {
            //            strParam = "{INVITE_BARCODE}",
            //            strValue = cus.BarCode
            //        });
            //        fillData.Add(new ParamValue()
            //        {
            //            strParam = "{EVENT_LOCATION}",
            //            strValue = objEvent.Location
            //        });
            //        fillData.Add(new ParamValue()
            //        {
            //            strParam = "{EVENT_FROMDATE}",
            //            strValue = VnDateConvert.ConvertDateTimeTempVN(objEvent.FromDate)
            //        });

            //        string _ressult = TemplateEngine.FillTemplate(fillData, TemplateVNSource);

            //        SelectPdf.PdfDocument docuri = converter.ConvertHtmlString(_ressult);
            //        docuri.Save(_fileName);
            //        Console.WriteLine(_fileName);
            //    }
            //}
            //Console.WriteLine("******Gen bo xung {0}", countMiss);
            #endregion

            //DataAccess.CopyDocToEvent(8);

            //List<tblEventDoc> listDoc = DataAccess.GetAllDocOfEvent(8);
            //foreach (tblEventDoc eventDoc in listDoc)
            //{
            //    if (eventDoc.DocTemplate.Trim() != "[HTML Template]")
            //    {
            //        List<tblEventDocDynParam> listParams = new List<tblEventDocDynParam>();

            //        List<ParamValue> paramValues = TemplateEngine.GetListDynamicParams(eventDoc.DocTemplate);
            //        paramValues.ForEach(t => listParams.Add(new tblEventDocDynParam()
            //        {
            //            EventDoc_ID = eventDoc.EventDoc_ID,
            //            Param_name = t.strParam
            //        }));
            //        DataAccess.InsertEventDocParams(listParams);
            //    }
            //}

            //GetParam();
            //long xxx = DataAccess.SumStockAttend(9);
            //Console.WriteLine(xxx.ToString());

            //string Test = "toi ten la {quanlm}, loop {quanlm} la ten";
            //List<ParamValue> getParam = TemplateEngine.GetListDynamicParams(Test);
            //getParam.ForEach(f => Console.WriteLine(f.strParam));

            //getParam.ForEach(f => f.strValue = "QuanLeManh");

            //string _result = TemplateEngine.FillTemplate(getParam, Test);
            //Console.WriteLine(_result);

            //List<string> abc = DataAccess.GetLstVotedBarcode(94315, 9);
            //abc.ForEach(f => Console.WriteLine(f));
            //InitVote();

            Console.OutputEncoding = Encoding.UTF8;
            //List<tblStockholderInvite> listCus = DataAccess.GetStockholderInviteCustom(15, 1);
            int _eventID = 14;
            List<tblStockholderAttend> listCus = DataAccess.GetStockholderAttendCustom(_eventID, 1);

            int rowcount = 1;
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            foreach (var item in listCus)
            {
                sb.AppendLine(ToCusAttend(item));
                rowcount++;
            }

            Console.WriteLine(sb.ToString());
            System.IO.File.WriteAllText(
                System.IO.Path.Combine(
                AppDomain.CurrentDomain.BaseDirectory, _eventID+"_event.csv"),
                sb.ToString());

            Console.WriteLine(rowcount);
            Console.ReadLine();

            //double xxx = 0.60;
            //Console.WriteLine(DecimalPlaceNoRounding(xxx, 2));
            //Console.Read();

            //MoveNoiBo();
            //Console.Read();
        }

        public static string DecimalPlaceNoRounding(double d, int decimalPlaces = 2)
        {
            d = d * Math.Pow(10, decimalPlaces);
            d = Math.Truncate(d);
            d = d / Math.Pow(10, decimalPlaces);
            return string.Format(CultureInfo.CreateSpecificCulture("vi-VN"),"{0:N" + Math.Abs(decimalPlaces) + "}", d);
        }

        public static string ToCusString(tblStockholderInvite item)
        {
            return item.SHID + ";" + item.FullName.Replace(";","") + ";" + item.CardID.ToString().Replace(";", "") + ";" + item.Quantity;
        }

        public static string ToCusString2(tblStockholderInvite item)
        {
            string _fileName =CommonHelper.UnicodeToKoDauAndGach(item.FullName) + "_" + CommonHelper.HashEncodeId(item.SHID) + "_Invi.pdf";
            return item.SHID + ";" + CommonHelper.HashEncodeId(item.SHID) + ";" + _fileName.Replace(";", "") + ";" + item.FullName.Replace(";", "") + ";" + item.Address.ToString().Replace(";", "") + ";" + item.Mobile.Replace(";", "") + ";" + item.Quantity;
        }

        public static string ToCusString3(tblStockholderInvite item)
        {
            string _fileName = CommonHelper.UnicodeToKoDauAndGach(item.FullName) + "_" + CommonHelper.HashEncodeId(item.SHID) + "_Invi.pdf";
            return item.SHID + ";" + CommonHelper.HashEncodeId(item.SHID) + ";" + _fileName.Replace(";", "") + ";" + item.FullName.Replace(";", "") + ";" + item.Quantity;
        }

        public static string ToCusAttend(tblStockholderAttend item)
        {
            var Stockholder = DataAccess.GetStockholderInviteByID(item.SHID);
            return item.SHID + ";" + item.Status + ";" + Stockholder.FullName.ToString().Replace(";", "") + ";" + Stockholder.CardID.ToString().Replace(";", "") + ";" + Stockholder.Quantity;
        }

        public static void TestCur()
        {
            long i = 789432456;
            // Create a CultureInfo object for English in Belize.
            CultureInfo bz = new CultureInfo("vi-VN");
            // Display i formatted as currency for bz.
            Console.WriteLine(i.ToString("0:0", bz));
            // Create a CultureInfo object for English in the U.S.
            CultureInfo us = new CultureInfo("en-US");
            // Display i formatted as currency for us.
            Console.WriteLine(i.ToString("0:0", us));
        }

        public static void InitVote()
        {
            List<VoteTemp> voteTopic = new List<VoteTemp>();
            voteTopic.Add(new VoteTemp()
            {
                VoteVN = "Thông qua Báo cáo hoạt động của Hội đồng Quản trị.",
                VoteEN = "Approval of the Report of the Board of Directors (“BoD”)."
            });
            voteTopic.Add(new VoteTemp()
            {
                VoteVN = "Thông qua Báo cáo của Ban Giám đốc về tình hình kinh doanh năm 2018 và kế hoạch kinh doanh năm 2019.",
                VoteEN = "Approval of the Report of Management on 2018 business performance and 2019 business plan."
            });
            voteTopic.Add(new VoteTemp()
            {
                VoteVN = "Thông qua Báo cáo của Ban Kiểm soát về kết quả kinh doanh của Công ty, về kết quả hoạt động của Hội đồng Quản trị và Ban Giám đốc năm 2018.",
                VoteEN = "Approval of the Report of the Supervisory Board on business results of the Company, and activities of the BoD and Management in 2018."
            });
            voteTopic.Add(new VoteTemp()
            {
                VoteVN = "Thông qua Báo cáo Tài chính năm 2018 đã được kiểm toán.",
                VoteEN = "Approval of the 2018 Audited Financial Statements."
            });
            voteTopic.Add(new VoteTemp()
            {
                VoteVN = "Thông qua phương án sử dụng lợi nhuận sau thuế lũy kế năm 2018",
                VoteEN = "Approval of the Appropriation Plan."
            });
            voteTopic.Add(new VoteTemp()
            {
                VoteVN = "Phê duyệt thù lao cho thành viên Hội đồng Quản trị và Ban Kiểm soát.",
                VoteEN = "Approval of the Remuneration for the members of BoD and the Supervisory Board."
            });

            int num = 1;
            //foreach(var item in voteTopic)
            //{
            //    tblVoteTopic vote = new tblVoteTopic();
            //    vote.VoteTopicId = 3;
            //    vote.VoteName = item.VoteVN;
            //    vote.VoteName_En = item.VoteEN;
            //    vote.VoteContent = item.VoteVN;
            //    vote.VoteContent_En = item.VoteEN;
            //    vote.Note = "";
            //    vote.Note_En = "";
            //    vote.Status = true;
            //    vote.VoteOrder = num;
            //    vote.CreatedDate = DateTime.Now;
            //    vote.Label1 = "Tán thành";
            //    vote.Label2 = "Không tán thành";
            //    vote.Label3 = "Không có ý kiến";


            //    DataAccess.InsertVoteTopic(vote);
            //    Console.WriteLine(num);
            //    num++;
            //}
        }


        public static void GetNoiBo()
        {
            string CardIDRaw = "011821593,031188001889,013236450,001089018892,001184020585,012359182,142130498,182219623,B8030775,001172003115,001078000018,012150617,013262904,012943744,121213349,017309990,182062271,001086017036,012296700,011668240,013459917,017312609,012982656,013344592,017202490,042179000278,036184000177,012002909,013644777,019181000027,013640786,090718807,011787919,079078006280,B3268630,111491494,111491304,111119121,011607979,038082002535,001180001928,011689053,011899209,001183000112,112271779,121430578,031186000018,012782120,013110608,012354206,030958721,012894081,012919257,013379950,012564660,038085000067,001061002261,164372713,011897194,031074384,024083000377,111564127,012236765,183620726,186720534,111487942,012888793,010491590,011783797,001082005028,112293295,011815653,201466729,VSDVRE012457071,001079006164,012227177,017496955,012017595,001079003833,001091010068,001091007489,001183021623,011689049,001083021987,012988071,VSDVRE012464835,013631256,341730618,012769812,001172010132,031185000804,031477718,012220687,033086000939,027188000117,031796966,013263172,B2801888,027171000050,012934199,031186006572,011887631,025185000503,010131054,011639066,131125817,012073196,019185000014,013241233,042181000092,012928797,172580395,013514299,023762400,031275200,145094094,060717479,011840675,013053104,013491526,00117503862,013454593,001147003609,001183006603,143005894,145018536,145476933,001178008139,013678475,001067008608,030088001249,036081003858,135313719,036083000087,171624879,143006465,163232469,034072000707,031424586,012389132,012047116,201523804,012964479,013339239,030992545,183943363,162329589,013314956,011879050,168000231,010565094,B5630972,011674868,013314352,186197970,013309164,012073015,186740682,090783737,163196637,034078004729,011204447,VSDVRE012430837,001081005027,013195082,031184001903,013585350,012528032,011934411,013321617,012036050,011938292,012500592,112195195,001172000904,025122813,001080010936,163122519,013005518,090731587,031103762,131299796,011786332,VSDVRE031186000003,011941323,038073000059,012861969,092085003751,036082000290,038180000218,036083000068,011865150,001080021344,011353303,001188012041,012572676,001176002226,012314658,013461927,162963805,031083000039,025515213,031190062,011529995,173356692,173263194,001185002142,001184012160,038182000064,230594573,001084009687,001186006431,100816333,012778966,B7324800,001077013209,011984291,B4873415,001186015222,011991998,013188373,017258080,024074000153,112028617,112032732,001085003737,037086000337,013570162,001184007665,160063013,173064140,013527764,030901051,031606906,037179000024,013221195,001177000028,164307270,031271920,012684971,011911506,B3429434,110929864,036082004852,163026253,012196703,001085019498,012750081,054084000013,023311087,017323492,012203409,013091318,031182001732,034081000006,112271660,E5376191E";
            string[] listCard = CardIDRaw.Split(',');
            foreach (string xxx in listCard)
            {
                var result = DataAccess.GetStockholderInvite(6, xxx, null);
                if (result.Count == 1)
                {
                    tblStockholderInvite obj = result.FirstOrDefault();
                    obj.IsInternal = true;
                    DataAccess.UpdateStockholderInvite(obj);
                }
                else
                {
                    result.ToList().ForEach(t => Console.WriteLine(t.SHID + ","));
                }
            }
        }

        public static void MoveNoiBo()
        {
            //List<tblStockholderInvite> listVN = DataAccess.GetStockholderInviteNoiBo(6);
            List<tblStockholderInvite> listVN = DataAccess.GetStockholderInviteCustom(13, 1);
            Console.WriteLine("file in DB {0}", listVN.Count());

            int countMiss = 0;

            foreach (tblStockholderInvite cus in listVN)
            {
                string _fileName = "E:/VHM_INVI/VHM_INVI/Total/" + CommonHelper.UnicodeToKoDauAndGach(cus.FullName) + "_" + CommonHelper.HashEncodeId(cus.SHID) + "_Invi.pdf";
                string _fileName_HSBC = "E:/VHM_INVI/VHM_INVI/HSBC/" + CommonHelper.UnicodeToKoDauAndGach(cus.FullName) + "_" + CommonHelper.HashEncodeId(cus.SHID) + "_Invi.pdf";
                //string _fileName = CommonHelper.HashEncodeId(cus.SHID) + "_" + CommonHelper.UnicodeToKoDauAndGach(cus.FullName) + "_Invi.pdf";
                if (File.Exists(_fileName))
                {
                    File.Copy(_fileName, _fileName_HSBC);
                    Console.WriteLine(_fileName);
                }
            }
            Console.WriteLine("******Gen bo xung {0}", countMiss);
        }


        public static void GetParam()
        {
            List<ParamValue> fillData = new List<ParamValue>();
            string TemplateVNSource = InlineTemplateEngine.GetTemplate("ThamTraVN.html");
            Regex r1 = new Regex(@"{\w*\}");
            MatchCollection match = r1.Matches(TemplateVNSource);
            for (int i = 0; i < match.Count; i++)
            {
                if (!fillData.Any(p => p.strParam == match[i].Value))
                {
                    fillData.Add(new ParamValue()
                    {
                        strParam = match[i].Value,
                        strValue = string.Empty
                    });
                }
            }

            fillData.ForEach(f => Console.WriteLine(f.strParam));
            Console.Read();
        }
    }

    public class InlineTemplateEngine
    {
        public static string GetTemplate(string fileName)
        {
            string fileContents = string.Empty;
            string path = System.Reflection.Assembly.GetExecutingAssembly().Location;
            var directory = System.IO.Path.GetDirectoryName(path);
            fileContents = System.IO.File.ReadAllText(directory + "\\Content\\InvitationTemplates\\" + fileName);
            return fileContents;
        }
    }

    public class VoteTemp
    {
        public string VoteVN { get; set; }
        public string VoteEN { get; set; }
    }
}
