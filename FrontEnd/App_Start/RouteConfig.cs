﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace DHCDFrontEnd
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute("home",
                            "home/",
                            new { controller = "Home", action = "Index" });

            routes.MapRoute("masterdata",
                            "masterdata/",
                            new { controller = "MasterData", action = "MasterDataPL" });

            routes.MapRoute("mastereventlist",
                            "mastereventlist/",
                            new { controller = "StockholderEvent", action = "MasterList", id = UrlParameter.Optional });

            routes.MapRoute("Logout",
                           "logout/",
                           new { controller = "Account", action = "Logout" });

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
