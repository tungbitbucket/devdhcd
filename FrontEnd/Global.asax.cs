﻿using DHCDFrontEnd.App_Start;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace DHCDFrontEnd
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            log4net.Config.XmlConfigurator.Configure(new FileInfo(Server.MapPath("~/Web.config")));
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            var req = HttpContext.Current.Request;
        }

        protected void Application_AcquireRequestState(object sender, EventArgs e)
        {
            if (Session["LocationID"] == null || Session["LocationAddress"] == null)
            {
                if(Request.FilePath != "~/logout" && Request.FilePath != "/Account/login")
                    Response.RedirectToRoute("Logout");
            }
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();
            Console.WriteLine(ex);
        }
    }
}
