﻿using DataService;
using DataService.ActionServices;
using DataService.DomainModel;
using DHCDFrontEnd.Extensions;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using VG.Common;

namespace DHCDFrontEnd.Controllers
{
    [Authorize]
    public class StockholderEventController : ControllerExtension
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private IAuthenticationService authenticationService;
        public StockholderEventController()
        {
            authenticationService = new AuthenticationService(new HttpContextWrapper(System.Web.HttpContext.Current));
        }

        [CompressFilter(Order = 1)]
        public ActionResult MasterList(string p)
        {
            ListEventModel EventModel = new ListEventModel();
            try
            {
                List<tblMasterPL> listPL = new List<tblMasterPL>();
                listPL = DataAccess.ListPL(1);
                int _seleted = 0;
                tblMasterPL objSeleted = new tblMasterPL();

                if (!string.IsNullOrEmpty(p))
                {
                    if (CommonHelper.HashDecodeId(p, out _seleted))
                    {
                        try
                        {
                            objSeleted = listPL.SingleOrDefault(o => o.PL_ID == _seleted);
                        }
                        catch
                        {
                            objSeleted = listPL.FirstOrDefault();
                        }
                    }
                }
                else
                {
                    objSeleted = listPL.FirstOrDefault();
                }
                EventModel.listPL = listPL;
                EventModel.SeletedPL = objSeleted;
                EventModel.listEventByPL = DataAccess.GetEventByPL(objSeleted.PL_ID, "created_date");
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToJson(false));
            }
            return View(EventModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult SubmitEvent()
        {
            try
            {
                string violationChaRegex = "^[^`~!@#$%\\^{}'\"]+$";
                Regex re = new Regex(violationChaRegex);
                int _countError = 0;
                ValidModel validModel = new ValidModel();
                tblStockholderEvent objEvent = new tblStockholderEvent();

                int _plID = 0;
                if (string.IsNullOrEmpty(Request.Form["hPLID"]))
                {
                    validModel.listViewisValid.Add("ID không tìm thấy");
                    _countError++;
                }else if(!CommonHelper.HashDecodeId(Request.Form["hPLID"], out _plID))
                {
                    validModel.listViewisValid.Add("ID không tìm thấy");
                    _countError++;
                }
                else
                {
                    objEvent.PL_ID = _plID;
                }
                if (string.IsNullOrEmpty(Request.Form["NumlStock"]))
                {
                    validModel.listViewisValid.Add("[Số lượng cổ phiếu] là thông tin bắt buộc");
                    _countError++;
                }
                else
                {
                    if (!re.IsMatch(Request.Form["NumlStock"]))
                    {
                        validModel.listViewisValid.Add("[Số lượng cổ phiếu] không chứa ký tự đặc biệt");
                        _countError++;
                    }
                    else
                    {
                        if (!Regex.IsMatch(Request.Form["NumlStock"], @"^\d+$"))
                        {
                            validModel.listViewisValid.Add("[Số lượng cổ phiếu] chỉ nhận số");
                            _countError++;
                        }
                        else
                        {
                            objEvent.NumlStock = long.Parse(Request.Form["NumlStock"]);
                        }
                    }
                }

                if (string.IsNullOrEmpty(Request.Form["Event_Name"]))
                {
                    validModel.listViewisValid.Add("[Tên sự kiện] là thông tin bắt buộc");
                    _countError++;
                }
                else
                {
                    if (!re.IsMatch(Request.Form["Event_Name"]))
                    {
                        validModel.listViewisValid.Add("[Tên sự kiện] không chứa ký tự đặc biệt");
                        _countError++;
                    }
                    else
                    {
                        objEvent.Event_Name = Request.Form["Event_Name"];
                    }
                }

                if (string.IsNullOrEmpty(Request.Form["Event_Name_En"]))
                {
                    validModel.listViewisValid.Add("[Tên sự kiện Eng] là thông tin bắt buộc");
                    _countError++;
                }
                else
                {
                    if (!re.IsMatch(Request.Form["Event_Name_En"]))
                    {
                        validModel.listViewisValid.Add("[Tên sự kiện Eng] không chứa ký tự đặc biệt");
                        _countError++;
                    }
                    else
                    {
                        objEvent.Event_Name_En = Request.Form["Event_Name_En"];
                    }
                }

                if (string.IsNullOrEmpty(Request.Form["Event_Year"]))
                {
                    validModel.listViewisValid.Add("[Năm sự kiện] là thông tin bắt buộc");
                    _countError++;
                }
                else
                {
                    objEvent.Event_Year = Request.Form["Event_Year"];
                }
                if (string.IsNullOrEmpty(Request.Form["Location"]))
                {
                    validModel.listViewisValid.Add("[Địa chỉ tổ chức] là thông tin bắt buộc");
                    _countError++;
                }
                else
                {
                    if (!re.IsMatch(Request.Form["Location"]))
                    {
                        validModel.listViewisValid.Add("[Địa chỉ tổ chức] không chứa ký tự đặc biệt");
                        _countError++;
                    }
                    else
                    {
                        objEvent.Location = Request.Form["Location"];
                    }
                    
                }
                if (string.IsNullOrEmpty(Request.Form["Location_En"]))
                {
                    validModel.listViewisValid.Add("[Địa chỉ tổ chức Eng] là thông tin bắt buộc");
                    _countError++;
                }
                else
                {
                    if (!re.IsMatch(Request.Form["Location_En"]))
                    {
                        validModel.listViewisValid.Add("[Địa chỉ tổ chức Eng] không chứa ký tự đặc biệt");
                        _countError++;
                    }
                    else
                    {
                        objEvent.Location_En = Request.Form["Location_En"];
                    }
                }
                if (string.IsNullOrEmpty(Request.Form["FromDate"]))
                {
                    validModel.listViewisValid.Add("[Ngày bắt đầu] là thông tin bắt buộc");
                    _countError++;
                }
                else
                {
                    objEvent.FromDate = DateTime.ParseExact(Request.Form["FromDate"], "dd/MM/yyyy", CultureInfo.InvariantCulture);
                }
                if (string.IsNullOrEmpty(Request.Form["ToDate"]))
                {
                    validModel.listViewisValid.Add("[Ngày kết thúc] là thông tin bắt buộc");
                    _countError++;
                }
                else
                {
                    objEvent.ToDate = DateTime.ParseExact(Request.Form["ToDate"], "dd/MM/yyyy", CultureInfo.InvariantCulture);
                }
                if (!string.IsNullOrEmpty(Request.Form["Note"]))
                {
                    objEvent.Note = Request.Form["Note"];
                }

                int value = DateTime.Compare(objEvent.FromDate, objEvent.ToDate);
                if (value > 0)
                {
                    validModel.listViewisValid.Add("[Ngày kết thúc] nhỏ hơn [Ngày bắt đầu]");
                    _countError++;
                }

                if (_countError > 0)
                {
                    return Json(new { status = 0, litmess = validModel });
                }
                else
                {
                    objEvent.Created_Date = DateTime.Now;
                    objEvent.Created_Acc = authenticationService.GetAuthenticatedUser().Acc_ID;
                    objEvent.Status = 1;
                    int statusUpdate = DataAccess.AddEvent(objEvent);
                    return Json(new { status = 1, rref = statusUpdate });
                }
            }
            catch(Exception ex)
            {
                return Json(new { status = -1, mes = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult ActionEvent(string p, byte s)
        {
            try
            {
                int _plID = 0;
                if (CommonHelper.HashDecodeId(p, out _plID))
                {
                    int statusUpdate = DataAccess.UpdateStatusEvent(_plID, s);
                    return Json(new { status = 1, rref = statusUpdate });
                }
                else
                {
                    return Json(new { status = 0, mes = "Không thể cập nhật sự kiện!" });
                }
            }
            catch (Exception ex)
            {
                return Json(new { status = -1, mes = ex.Message });
            }
        }
    }
}