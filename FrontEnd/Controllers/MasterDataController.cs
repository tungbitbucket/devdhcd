﻿using DataService;
using DataService.DomainModel;
using DHCDFrontEnd.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using VG.Common;

namespace DHCDFrontEnd.Controllers
{
    [Authorize]
    public class MasterDataController : ControllerExtension
    {
        [CompressFilter(Order = 1)]
        public ActionResult MasterDataPL()
        {
            List<tblMasterPL> listPL = new List<tblMasterPL>();
            try
            {
                listPL = DataAccess.ListPL();
            }
            catch (Exception ex)
            {
                //Log4net
            }
            ViewBag.CountPL = listPL.Count().ToString();
            return View(listPL);
        }



        [HttpPost]
        [ValidateAntiForgeryToken]
        [CompressFilter(Order = 1)]
        public JsonResult AddPL()
        {
            try
            {
                string violationChaRegex = "^[^`~!@#$%\\^{}'\"]+$";
                int _countError = 0;
                ValidModel validModel = new ValidModel();
                tblMasterPL objPL = new tblMasterPL();

                if (string.IsNullOrEmpty(Request.Form["plName"]))
                {
                    validModel.listViewisValid.Add("[Tên viết tắt] là thông tin bắt buộc");
                    _countError++;
                }
                else
                {
                    objPL.PL_DisplayName = Request.Form["plName"];
                }


                if (string.IsNullOrEmpty(Request.Form["plFullname"]))
                {
                    validModel.listViewisValid.Add("[Tên đầy đủ] là thông tin bắt buộc");
                    _countError++;
                }
                else
                {
                    objPL.PL_Name = Request.Form["plFullname"];
                }

                if (string.IsNullOrEmpty(Request.Form["plFullnameEn"]))
                {
                    validModel.listViewisValid.Add("[Tên đầy đủ Eng] là thông tin bắt buộc");
                    _countError++;
                }
                else
                {
                    objPL.PL_Name_En = Request.Form["plFullnameEn"];
                }

                if (!string.IsNullOrEmpty(Request.Form["plStatus"]))
                {
                    objPL.PL_Status = Request.Form["plStatus"].ToInt32OrDefault(1);
                }

                if (_countError > 0)
                {
                    return Json(new { status = 0, litmess = validModel });
                }
                else
                {
                    int _idHS = DataAccess.AddMasterPL(objPL);
                    return Json(new { status = 1, ID = _idHS });
                }
            }
            catch(Exception ex)
            {
                //Log
                return Json(new { status = -1, mes = ex.Message });
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [CompressFilter(Order = 1)]
        public JsonResult EditPL()
        {
            try
            {
                string violationChaRegex = "^[^`~!@#$%\\^{}'\"]+$";
                Regex re = new Regex(violationChaRegex);
                int _countError = 0;
                ValidModel validModel = new ValidModel();
                tblMasterPL objPL = new tblMasterPL();

                if (string.IsNullOrEmpty(Request.Form["hPLID"]))
                {
                    validModel.listViewisValid.Add("ID không tìm thấy");
                    _countError++;
                }
                else
                {
                    objPL.PL_ID = Request.Form["hPLID"].ToInt32OrDefault(0);
                }

                if (string.IsNullOrEmpty(Request.Form["plName"]))
                {
                    validModel.listViewisValid.Add("[Tên viết tắt] là thông tin bắt buộc");
                    _countError++;
                }
                else
                {
                    
                    if (!re.IsMatch(Request.Form["plName"]))
                    {
                        validModel.listViewisValid.Add("[Tên viết tắt] không chứa ký tự đặc biệt");
                        _countError++;
                    }
                    else
                    {
                        objPL.PL_DisplayName = Request.Form["plName"];
                    }
                }
                if (string.IsNullOrEmpty(Request.Form["plFullname"]))
                {
                    validModel.listViewisValid.Add("[Tên đầy đủ] là thông tin bắt buộc");
                    _countError++;
                }
                else
                {
                    if (!re.IsMatch(Request.Form["plFullname"]))
                    {
                        validModel.listViewisValid.Add("[Tên đầy đủ] không chứa ký tự đặc biệt");
                        _countError++;
                    }
                    else
                    {
                        objPL.PL_Name = Request.Form["plFullname"];
                    }
                }

                if (string.IsNullOrEmpty(Request.Form["plFullnameEn"]))
                {
                    validModel.listViewisValid.Add("[Tên đầy đủ En] là thông tin bắt buộc");
                    _countError++;
                }
                else
                {
                    if (!re.IsMatch(Request.Form["plFullnameEn"]))
                    {
                        validModel.listViewisValid.Add("[Tên đầy đủ En] không chứa ký tự đặc biệt");
                        _countError++;
                    }
                    else
                    {
                        objPL.PL_Name_En = Request.Form["plFullnameEn"];
                    }
                }

                if (!string.IsNullOrEmpty(Request.Form["plStatus"]))
                {
                    objPL.PL_Status = Request.Form["plStatus"].ToInt32OrDefault(1);
                }

                if (_countError > 0)
                {
                    return Json(new { status = 0, litmess = validModel });
                }
                else
                {
                    int statusUpdate = DataAccess.UpdateMasterPL(objPL);
                    return Json(new { status = 1, rref = statusUpdate });
                }
            }
            catch (Exception ex)
            {
                //Log
                return Json(new { status = -1, mes = ex.Message });
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeletePL()
        {
            try
            {
                if (!string.IsNullOrEmpty(Request.Form["hPLID"]))
                {
                    int _ID = Request.Form["hPLID"].ToInt32OrDefault(1);
                    int _result = DataAccess.DeleteMasterPL(_ID);
                    return Json(new { status = _result, mes = "OK" });
                }
                else
                {
                    return Json(new { status = 0, mes = "OK" });
                }
            }
            catch(Exception ex)
            {
                return Json(new { status = -1, mes = ex.Message });
            }
        }

        [HttpPost]
        [CompressFilter(Order = 1)]
        public JsonResult GetPandL()
        {
            try
            {
                if (!string.IsNullOrEmpty(Request.Form["hPLID"]))
                {
                    int _ID = Request.Form["hPLID"].ToInt32OrDefault(1);
                    tblMasterPL _result = DataAccess.GetPLByID(_ID);
                    if(_result != null)
                    {
                        return Json(new { status = 1, data = _result });
                    }
                    else
                    {
                        return Json(new { status = 0, mes = "404" });
                    }
                }
                else
                {
                    return Json(new { status = 0, mes = "404" });
                }
            }
            catch (Exception ex)
            {
                return Json(new { status = -3, mes = ex.Message });
            }
        }
    }
}