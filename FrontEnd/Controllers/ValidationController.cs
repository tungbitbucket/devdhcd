﻿using DataService;
using DataService.ActionServices;
using DataService.DomainModel;
using DHCDFrontEnd.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VG.Common;

namespace DHCDFrontEnd.Controllers
{
    [Authorize]
    public class ValidationController : ControllerExtension
    {
        private IAuthenticationService authenticationService;
        public ValidationController()
        {
            authenticationService = new AuthenticationService(new HttpContextWrapper(System.Web.HttpContext.Current));
        }

        public ActionResult barcode(string eID)
        {
            if (!string.IsNullOrEmpty(eID))
            {
                if (CommonHelper.HashDecodeId(eID, out int _eventID))
                {
                    CheckInModel CusCheckInModel = new CheckInModel();
                    tblStockholderEvent SelectedEvent = DataAccess.GetEventByID(_eventID);
                    var objPL = DataAccess.GetPLByID(SelectedEvent.PL_ID);

                    CusCheckInModel.Event_ID = SelectedEvent.Event_ID;
                    CusCheckInModel.EventHashID = CommonHelper.HashEncodeId(SelectedEvent.Event_ID);
                    CusCheckInModel.Event_Name = SelectedEvent.Event_Name.ToUpper();
                    CusCheckInModel.PL_ID = objPL.PL_ID;
                    CusCheckInModel.PLName = objPL.PL_DisplayName;
                    CusCheckInModel.HasVotingCard = SelectedEvent.HasVote;
                    CusCheckInModel.HasBSK = SelectedEvent.HasBKS;
                    CusCheckInModel.HasHDQT = SelectedEvent.HasHDQT;
                    
                    return View(CusCheckInModel);
                }
                else
                {
                    return Redirect("/NotFound");
                }
            }
            else
            {
                return Redirect("/NotFound");
            }
        }

        [HttpPost]
        [CompressFilter(Order = 1)]
        public JsonResult valivoting()
        {
            try
            {
                if (!string.IsNullOrEmpty(Request.Form["eID"]) && (!string.IsNullOrEmpty(Request.Form["bID"])))
                {
                    if (CommonHelper.HashDecodeId(Request.Form["eID"].Trim(), out int _eventID) && CommonHelper.HashDecodeId(Request.Form["bID"].Trim(), out long _shID))
                    {
                        var attended = DataAccess.StockholderIsCheckin(_eventID, _shID);
                        if (attended != null)
                        {
                            return Json(new { status = 1, mes = string.Format("/Validation/VoteCounting?eID={0}&uID={1}", Request.Form["eID"].Trim(), CommonHelper.HashEncodeId(attended.SHID)) });
                        }
                        else
                        {
                            return Json(new { status = 0, mes = "Không có cổ đông trong danh sách tham dự" });
                        }
                    }
                    else
                    {
                        return Json(new { status = -1, mes = "Mã số cổ đông không hợp lệ" });
                    }
                }
                else
                {
                    return Json(new { status = -1, mes = "Thông tin kiểm tra không đủ" });
                }
            }
            catch (Exception ex)
            {
                return Json(new { status = -3, mes = ex.Message });
            }
        }

        /// <summary>
        /// Kiểm tra khách mới có trong danh sách?
        /// </summary>
        /// <returns>1: Có</returns>
        /// <returns>5: đã checkin rồi</returns>
        [HttpPost]
        [CompressFilter(Order = 1)]
        public JsonResult valicustomer()
        {
            try
            {
                if (!string.IsNullOrEmpty(Request.Form["eID"]) && (!string.IsNullOrEmpty(Request.Form["uID"])))
                {
                    if (CommonHelper.HashDecodeId(Request.Form["eID"].Trim(), out int _eventID) && CommonHelper.HashDecodeId(Request.Form["uID"].Trim(), out long _shID))
                    {
                        try
                        {
                            var stockholder = DataAccess.FindStockHolder(_eventID, _shID);
                            stockholder.SHID = 0;
                            //Kiem tra User da CheckIn hay chua
                            var attended = DataAccess.StockholderIsCheckin(_eventID, _shID);
                            if (attended != null)
                            {
                                var attendedLocation = DataAccess.GetLocationByLocationID( (int)attended.LocationID );
                                string strAttend = (attended.Status == 1) ? "Cổ đông tham gia tại " : "Cổ đông đại diện tại ";
                                strAttend = strAttend + attendedLocation.Address;
                                return Json(new { status = 5, mes = stockholder, attend = strAttend });
                            }
                            else
                            {
                                return Json(new { status = 1, mes = stockholder });
                            }
                        }
                        catch
                        {
                            return Json(new { status = 0, mes = "Không có cổ đông trong danh sách tham dự" });
                        }
                    }
                    else
                    {
                        return Json(new { status = -1, mes = "Mã số cổ đông không hợp lệ" });
                    }
                }
                else
                {
                    return Json(new { status = -1, mes = "Thông tin kiểm tra không đủ" });
                }
            }
            catch (Exception ex)
            {
                return Json(new { status = -3, mes = ex.Message });
            }
        }

        public int getLocationID()
        {
            if (!string.IsNullOrEmpty(Session["LocationID"].ToString()))
            {
                string locationID = Session["LocationID"].ToString();
                return Int32.Parse(locationID);
            }
            return 0;
        }

        [HttpPost]
        [CompressFilter(Order = 1)]
        public JsonResult confirmcustomer()
        {
            try
            {
                if (!string.IsNullOrEmpty(Request.Form["eID"]) && (!string.IsNullOrEmpty(Request.Form["uID"])) && (!string.IsNullOrEmpty(Request.Form["status"])))
                {
                    if (CommonHelper.HashDecodeId(Request.Form["eID"].Trim(), out int _eventID) && CommonHelper.HashDecodeId(Request.Form["uID"].Trim(), out long _shID))
                    {
                        int result = DataAccess.StockHolderAttend(_eventID, _shID, byte.Parse(Request.Form["status"]), getLocationID());
                        if (result == 0)
                        {
                            return Json(new { status = 0, mes = "Mã số cổ đông không hợp lệ" });
                        }
                        else if (result == -1)
                        {
                            return Json(new { status = -5, mes = "Mã số cổ đông không hợp lệ" });
                        }
                        else
                        {
                            return Json(new { status = 1, mes = result });
                        }
                    }
                    else
                    {
                        return Json(new { status = -1, mes = "Mã số cổ đông không hợp lệ" });
                    }
                }
                else
                {
                    return Json(new { status = -1, mes = "Thông tin kiểm tra không đủ" });
                }
            }
            catch (Exception ex)
            {
                return Json(new { status = -3, mes = ex.Message });
            }
        }


        public ActionResult VoteCounting(string eID, string uID)
        {
            if (!string.IsNullOrEmpty(eID))
            {
                if (CommonHelper.HashDecodeId(eID, out int _eventID))
                {
                    VotingCardModel votingCardModel = new VotingCardModel();
                    tblStockholderEvent SelectedEvent = DataAccess.GetEventByID(_eventID);
                    var objPL = DataAccess.GetPLByID(SelectedEvent.PL_ID);
                    votingCardModel.Event_ID = SelectedEvent.Event_ID;
                    votingCardModel.EventHashID = CommonHelper.HashEncodeId(SelectedEvent.Event_ID);
                    votingCardModel.Event_Name = SelectedEvent.Event_Name;
                    votingCardModel.PL_ID = objPL.PL_ID;
                    votingCardModel.PLName = objPL.PL_Name;

                    ViewBag.Mode = 1;
                    if (!string.IsNullOrEmpty(uID))
                    {
                        CommonHelper.HashDecodeId(uID, out long _userID);
                        votingCardModel.VotingTopic = DataAccess.GetListVotingCardTopicByEvent(_eventID, 1);
                        List<tblBarcodeVote> GetBarcodeForVotingcard = DataAccess.GetBarcodeForVotingcard(_userID, _eventID);
                        List<VoteSubject> lstSubject = new List<VoteSubject>();

                        int _index = 1;
                        foreach (tblVotingCardTopic topic in votingCardModel.VotingTopic)
                        {
                            VoteSubject vote = new VoteSubject
                            {
                                SubjectNo = _index,
                                Name = topic.TopicNameVN
                            };
                            GetBarcodeForVotingcard.Where(b => b.VotingTopicID == topic.VotingTopicID).OrderBy(b => b.Result).ToList().ForEach(bc => vote.presentBarcodes.Add(new PresentBarcode()
                            {
                                BarcodeID = bc.barcodeID,
                                Barcode = CommonHelper.BarcodeToBase64(bc.barcodeID),
                                Title = CommonHelper.VoteOption(bc.Result)
                            }));

                            lstSubject.Add(vote);
                            _index++;
                        }
                        votingCardModel.VotingItems = lstSubject;
                        ViewBag.Mode = 2;

                        string disagree = _userID + ":" + _eventID + ":1";
                        ViewBag.DisagreeAll = CommonHelper.BarcodeToBase64(disagree);
                        ViewBag.DisagreeAllText = disagree;

                        string abstain = _userID + ":" + _eventID + ":2";
                        ViewBag.AbstainAll = CommonHelper.BarcodeToBase64(abstain);
                        ViewBag.AbstainAllText = abstain;

                        string invalid = _userID + ":" + _eventID + ":-1";
                        ViewBag.InvalidPerson = CommonHelper.BarcodeToBase64(invalid);
                        ViewBag.InvalidPersonText = invalid;

                        tblStockholderInvite stockholder = DataAccess.FindStockHolder(_eventID, _userID);
                        ViewBag.Quantity = stockholder.Quantity.Value;
                        ViewBag.StockholderName = stockholder.FullName;
                    }

                    return View(votingCardModel);
                }
                else
                {
                    return Redirect("/NotFound");
                }
            }
            else
            {
                return Redirect("/NotFound");
            }
        }

        [HttpPost]
        [CompressFilter(Order = 1)]
        public JsonResult SubmitVote()
        {
            try
            {
                if (!string.IsNullOrEmpty(Request.Form["bID"]))
                {
                    long barcodeID;
                    if (CommonHelper.HashDecodeId(Request.Form["bID"], out barcodeID))
                    {
                        tblBarcodeVote barcode = DataAccess.GetByBarcode(barcodeID);
                        tblVoteResult vote = new tblVoteResult();
                        vote.ShID = barcode.ShID;
                        vote.Result = barcode.Result;
                        vote.EventID = barcode.EventID;
                        vote.CreadDate = DateTime.Now;
                        vote.VoteTopicId = barcode.VotingTopicID;
                        vote.BarcodeID = barcode.barcodeID;
                        vote.LocationID = getLocationID();
                        DataAccess.InsertVoteResult(vote);

                        List<string> barcodeIDReturn = new List<string>();
                        barcodeIDReturn = DataAccess.GetLstVotedBarcode(barcode.ShID, barcode.EventID);
                        //check du so luong
                        if (barcodeIDReturn.Count() == DataAccess.CountTopicByEvent(barcode.EventID))
                        {
                            tblInvalidVote invalidVote = new tblInvalidVote()
                            {
                                ShID = barcode.ShID,
                                EventID = barcode.EventID,
                                IsInvalidVote = false
                            };
                            DataAccess.InsertInvalid(invalidVote);
                        }

                        //List<tblVoteResult> barcodeIDReturn = new List<tblVoteResult>();
                        //barcodeIDReturn = DataAccess.GetLstVotedBarcodeObj(barcode.ShID, barcode.EventID);
                        ////check du so luong
                        //if (barcodeIDReturn.Count() == DataAccess.CountTopicByEvent(barcode.EventID))
                        //{
                        //    tblInvalidVote invalidVote = new tblInvalidVote()
                        //    {
                        //        ShID = barcode.ShID,
                        //        EventID = barcode.EventID,
                        //        IsInvalidVote = false
                        //    };
                        //    DataAccess.InsertInvalid(invalidVote);
                        //}

                        //int countResultBarcode = 0;
                        //foreach(var check in barcodeIDReturn)
                        //{
                        //    if (check.Result == 3)
                        //    {
                        //        countResultBarcode++;
                        //    }
                        //}

                        //if(countResultBarcode == DataAccess.CountTopicByEvent(barcode.EventID))
                        //{
                        //    //remove het ben Result
                        //    DataAccess.RemoveFalseCheck(barcode.ShID, barcode.EventID);

                        //    tblInvalidVote invalidVote = new tblInvalidVote()
                        //    {
                        //        ShID = barcode.ShID,
                        //        EventID = barcode.EventID,
                        //        IsInvalidVote = true
                        //    };
                        //    DataAccess.InsertInvalid(invalidVote);
                        //}


                        return Json(new { status = 1, mes = barcodeIDReturn });
                    }
                    else
                    {
                        string[] sp = Request.Form["bID"].Split(':');
                        if (sp.Count() == 3)
                        {
                            try
                            {
                                long shID = long.Parse(sp[0]);
                                int eventID = int.Parse(sp[1]);
                                int result = int.Parse(sp[2]);

                                if (result == -1)
                                {
                                    tblInvalidVote invalidVote = new tblInvalidVote()
                                    {
                                        ShID = shID,
                                        EventID = eventID,
                                        IsInvalidVote = true
                                    };
                                    DataAccess.InsertInvalid(invalidVote);
                                    return Json(new { status = -1, mes = "" });
                                }
                                else
                                {
                                    List<tblBarcodeVote> GetBarcodeForVotingcard = DataAccess.GetBarcodeForVotingcard(shID, eventID);
                                    GetBarcodeForVotingcard = GetBarcodeForVotingcard.Where(o => o.Result == result).ToList();
                                    List<string> barcodeIDReturn = new List<string>();
                                    foreach (tblBarcodeVote barcode in GetBarcodeForVotingcard)
                                    {
                                        tblVoteResult vote = new tblVoteResult();
                                        vote.ShID = barcode.ShID;
                                        vote.Result = barcode.Result;
                                        vote.EventID = barcode.EventID;
                                        vote.CreadDate = DateTime.Now;
                                        vote.VoteTopicId = barcode.VotingTopicID;
                                        vote.BarcodeID = barcode.barcodeID;
                                        DataAccess.InsertVoteResult(vote);
                                        barcodeIDReturn.Add(barcode.barcodeID.ToString());
                                    }
                                    // and vaof bang VotingPerson
                                    tblInvalidVote invalidVote = new tblInvalidVote()
                                    {
                                        ShID = shID,
                                        EventID = eventID,
                                        IsInvalidVote = false
                                    };
                                    DataAccess.InsertInvalid(invalidVote);
                                    return Json(new { status = 1, mes = barcodeIDReturn });
                                }
                            }
                            catch
                            {
                                return Json(new { status = 0, mes = "Barcode không đúng" });
                            }
                        }
                        else
                        {
                            return Json(new { status = 0, mes = "Barcode không đúng" });
                        }
                    }
                }
                else
                {
                    return Json(new { status = 0, mes = "Không có thông tin Barcode" });
                }
            }
            catch(Exception ex)
            {
                return Json(new { status = 0, mes = ex.Message });
            }
        }



        public ActionResult ElectionCounting(string eID, byte type)
        {
            if (!string.IsNullOrEmpty(eID))
            {
                if (CommonHelper.HashDecodeId(eID, out int _eventID))
                {
                    if (type == 1)
                    {
                        ViewBag.Title = "Kiểm phiếu BKS";
                        ViewBag.Breadcrumb = "Kiểm phiếu Ban kiểm soát";
                    }
                    else if (type == 2)
                    {
                        ViewBag.Title = "Kiểm phiếu HDQT";
                        ViewBag.Breadcrumb = "Kiểm phiếu Hội đồng quản trị";
                    }


                    ElectionModel electionModel = new ElectionModel();
                    tblStockholderEvent SelectedEvent = DataAccess.GetEventByID(_eventID);
                    var objPL = DataAccess.GetPLByID(SelectedEvent.PL_ID);

                    electionModel.Event_ID = SelectedEvent.Event_ID;
                    electionModel.EventHashID = CommonHelper.HashEncodeId(SelectedEvent.Event_ID);
                    electionModel.Event_Name = SelectedEvent.Event_Name.ToUpper();
                    electionModel.PL_ID = objPL.PL_ID;
                    electionModel.PLName = objPL.PL_Name.ToUpper();
                    electionModel.ElectionType = type;
                    electionModel.lstCandidates = DataAccess.GetListCandidateByEventandType(_eventID, type);

                    return View(electionModel);
                }
                else
                {
                    return Redirect("/NotFound");
                }
            }
            else
            {
                return Redirect("/NotFound");
            }
        }


        [HttpPost]
        [CompressFilter(Order = 1)]
        public JsonResult customerinfo()
        {
            try
            {
                if (!string.IsNullOrEmpty(Request.Form["eID"]) && (!string.IsNullOrEmpty(Request.Form["uID"])))
                {
                    if (CommonHelper.HashDecodeId(Request.Form["eID"].Trim(), out int _eventID) && CommonHelper.HashDecodeId(Request.Form["uID"].Trim(), out long _shID))
                    {
                        try
                        {
                            ElectionModel electionModel = new ElectionModel();
                            var stockholder = DataAccess.FindStockHolder(_eventID, _shID);

                            electionModel.StockholderName = stockholder.FullName;
                            electionModel.StockHolderCode = CommonHelper.HashEncodeId(stockholder.SHID);
                            electionModel.StockHolderID = stockholder.SHID;
                            electionModel.Quantity = stockholder.Quantity.Value;

                            var lstCandidates = DataAccess.GetListCandidateByEventandType(_eventID, byte.Parse(Request.Form["type"]));
                            electionModel.CountCandidates = lstCandidates.Count;
                            electionModel.TotalQuantity = (electionModel.Quantity * electionModel.CountCandidates);

                            
                            int checkVote = DataAccess.CountCandidateVoteResult(_eventID, _shID, byte.Parse(Request.Form["type"]));
                            if (checkVote > 0)
                            {
                                return Json(new { status = 2, mes = electionModel });
                            }
                            else
                            {
                                return Json(new { status = 1, mes = electionModel });
                            }
                        }
                        catch
                        {
                            return Json(new { status = 0, mes = "Không có cổ đông trong danh sách tham dự" });
                        }
                    }
                    else
                    {
                        return Json(new { status = -1, mes = "Mã số cổ đông không hợp lệ" });
                    }
                }
                else
                {
                    return Json(new { status = -1, mes = "Thông tin kiểm tra không đủ" });
                }
            }
            catch (Exception ex)
            {
                return Json(new { status = -3, mes = ex.Message });
            }
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult ElectionSubmit()
        {
            try
            {
                int _countError = 0;
                ValidModel validModel = new ValidModel();
                int _eID = 0;
                if (string.IsNullOrEmpty(Request.Form["EventID"]))
                {
                    validModel.listViewisValid.Add("ID không tìm thấy");
                    _countError++;
                }
                else if (!CommonHelper.HashDecodeId(Request.Form["EventID"], out _eID))
                {
                    validModel.listViewisValid.Add("ID không tìm thấy");
                    _countError++;
                }

                long _ShID = 0;
                if (string.IsNullOrEmpty(Request.Form["txtBarcode"]))
                {
                    validModel.listViewisValid.Add("Mã số cổ đông không tìm thấy");
                    _countError++;
                }
                else if (!CommonHelper.HashDecodeId(Request.Form["txtBarcode"], out _ShID))
                {
                    validModel.listViewisValid.Add("Mã số cổ đông không tìm thấy");
                    _countError++;
                }
                byte _Type = byte.Parse(Request.Form["ElectionType"]);


                if (_countError > 0)
                {
                    return Json(new { status = 0, litmess = validModel });
                }
                else
                {
                    DataAccess.ClearCandidateVoteResult(_eID, _ShID, _Type);
                    var lstCandidates = DataAccess.GetListCandidateByEventandType(_eID, _Type);

                    List<tblCandidateIDVoteResult> listVote = new List<tblCandidateIDVoteResult>();
                    foreach (var item in lstCandidates)
                    {
                        string _nameElement = "UV_" + item.CandidateID;
                        tblCandidateIDVoteResult vote = new tblCandidateIDVoteResult()
                        {
                            CandidateID = item.CandidateID,
                            CreadDate = DateTime.Now,
                            EventID = _eID,
                            ShID = _ShID,
                            voteType = _Type,
                            VoteValid = true,
                            Quantity = long.Parse(Request.Form[_nameElement]),
                            LocationID = getLocationID()
                        };
                        listVote.Add(vote);
                    }

                    DataAccess.InsertCandidateVoteResult(listVote);

                    tblCandidateInvalidVote objValidVote = new tblCandidateInvalidVote() {
                        CandidateType = _Type,
                        DateVote = DateTime.Now,
                        EventID = _eID,
                        ShID = _ShID,
                        IsInvalidVote = false,
                        LocationID = getLocationID()
                    };
                    DataAccess.InsertCandicateInvalidVote(objValidVote);

                    return Json(new { status = 1, litmess = "Okie" });
                }
            }
            catch (Exception ex)
            {
                return Json(new { status = -1, mes = ex.Message });
            }
        }

        [HttpPost]
        [CompressFilter(Order = 1)]
        public JsonResult ElectionDivided()
        {
            try
            {
                int _countError = 0;
                ValidModel validModel = new ValidModel();
                int _eID = 0;
                if (string.IsNullOrEmpty(Request.Form["EventID"]))
                {
                    validModel.listViewisValid.Add("ID không tìm thấy");
                    _countError++;
                }
                else if (!CommonHelper.HashDecodeId(Request.Form["EventID"], out _eID))
                {
                    validModel.listViewisValid.Add("ID không tìm thấy");
                    _countError++;
                }

                long _ShID = 0;
                if (string.IsNullOrEmpty(Request.Form["txtBarcode"]))
                {
                    validModel.listViewisValid.Add("Mã số cổ đông không tìm thấy");
                    _countError++;
                }
                else if (!CommonHelper.HashDecodeId(Request.Form["txtBarcode"], out _ShID))
                {
                    validModel.listViewisValid.Add("Mã số cổ đông không tìm thấy");
                    _countError++;
                }
                byte _Type = byte.Parse(Request.Form["ElectionType"]);

                if (_countError > 0)
                {
                    return Json(new { status = 0, litmess = validModel });
                }
                else
                {
                    var stockholder = DataAccess.FindStockHolder(_eID, _ShID);

                    DataAccess.ClearCandidateVoteResult(_eID, _ShID, _Type);
                    var lstCandidates = DataAccess.GetListCandidateByEventandType(_eID, _Type);

                    List<tblCandidateIDVoteResult> listVote = new List<tblCandidateIDVoteResult>();
                    foreach (var item in lstCandidates)
                    {
                        tblCandidateIDVoteResult vote = new tblCandidateIDVoteResult()
                        {
                            CandidateID = item.CandidateID,
                            CreadDate = DateTime.Now,
                            EventID = _eID,
                            ShID = _ShID,
                            voteType = _Type,
                            VoteValid = true,
                            Quantity = stockholder.Quantity.Value,
                            LocationID = getLocationID()
                        };
                        listVote.Add(vote);
                    }
                    DataAccess.InsertCandidateVoteResult(listVote);

                    tblCandidateInvalidVote objValidVote = new tblCandidateInvalidVote()
                    {
                        CandidateType = _Type,
                        DateVote = DateTime.Now,
                        EventID = _eID,
                        ShID = _ShID,
                        IsInvalidVote = false,
                        LocationID = getLocationID()
                    };
                    DataAccess.InsertCandicateInvalidVote(objValidVote);

                    return Json(new { status = 1, litmess = "Okie" });
                }
            }
            catch (Exception ex)
            {
                return Json(new { status = -1, mes = ex.Message });
            }
        }


        [HttpPost]
        [CompressFilter(Order = 1)]
        public JsonResult ElectionInvalid()
        {
            try
            {
                int _countError = 0;
                ValidModel validModel = new ValidModel();
                int _eID = 0;
                if (string.IsNullOrEmpty(Request.Form["EventID"]))
                {
                    validModel.listViewisValid.Add("ID không tìm thấy");
                    _countError++;
                }
                else if (!CommonHelper.HashDecodeId(Request.Form["EventID"], out _eID))
                {
                    validModel.listViewisValid.Add("ID không tìm thấy");
                    _countError++;
                }

                long _ShID = 0;
                if (string.IsNullOrEmpty(Request.Form["txtBarcode"]))
                {
                    validModel.listViewisValid.Add("Mã số cổ đông không tìm thấy");
                    _countError++;
                }
                else if (!CommonHelper.HashDecodeId(Request.Form["txtBarcode"], out _ShID))
                {
                    validModel.listViewisValid.Add("Mã số cổ đông không tìm thấy");
                    _countError++;
                }
                byte _Type = byte.Parse(Request.Form["ElectionType"]);

                if (_countError > 0)
                {
                    return Json(new { status = 0, litmess = validModel });
                }
                else
                {
                    DataAccess.ClearCandidateVoteResult(_eID, _ShID, _Type);
                    tblCandidateInvalidVote objValidVote = new tblCandidateInvalidVote()
                    {
                        CandidateType = _Type,
                        DateVote = DateTime.Now,
                        EventID = _eID,
                        ShID = _ShID,
                        IsInvalidVote = true,
                        LocationID = getLocationID()
                    };
                    DataAccess.InsertCandicateInvalidVote(objValidVote);

                    return Json(new { status = 1, litmess = "Okie" });
                }
            }
            catch (Exception ex)
            {
                return Json(new { status = -1, mes = ex.Message });
            }
        }
    }
}