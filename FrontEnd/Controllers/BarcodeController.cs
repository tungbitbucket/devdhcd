﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VG.Common;
using ZXing;
using ZXing.QrCode;

namespace DHCDFrontEnd.Controllers
{
    public class BarcodeController : Controller
    {
        // GET: Barcode
        [AllowAnonymous]
        public ActionResult Index(string sid)
        {
            //string xxx = "80519,80519,1";
            //var content = VG.Common.EncryBase.EncryptText(sid, "H76HGSLCIE928HHTT");
            var content = sid;
            var writer = new BarcodeWriter
            {
                Format = BarcodeFormat.CODE_128,
                Options = new ZXing.Common.EncodingOptions { Height = 70, Margin=0}
            };
            var bitmap = writer.Write(content);
            Graphics g = Graphics.FromImage(bitmap);

            MemoryStream ms = new MemoryStream();
            bitmap.Save(ms, ImageFormat.Png);
            ms.Position = 0;
            return new FileStreamResult(ms, "image/png");
        }

        [AllowAnonymous]
        public ActionResult QrCode(string sid)
        {
            var content = sid;

            var options = new QrCodeEncodingOptions
            {
                DisableECI = true,
                CharacterSet = "UTF-8",
                Width = 200,
                Height = 200,
                Margin = 2
            };

            var writer = new BarcodeWriter
            {
                Format = BarcodeFormat.QR_CODE,
                Options = options
            };

            var bitmap = writer.Write(content);
            Graphics g = Graphics.FromImage(bitmap);

            MemoryStream ms = new MemoryStream();
            bitmap.Save(ms, ImageFormat.Png);
            ms.Position = 0;
            return new FileStreamResult(ms, "image/png");
        }

        [AllowAnonymous]
        public ActionResult BarcodeBase64(string sid)
        {
            var content = sid;
            var writer = new BarcodeWriter
            {
                Format = BarcodeFormat.CODE_128,
                Options = new ZXing.Common.EncodingOptions { Height = 70, Margin = 0 }
            };
            var bitmap = writer.Write(content);
            Graphics g = Graphics.FromImage(bitmap);

            MemoryStream ms = new MemoryStream();
            bitmap.Save(ms, ImageFormat.Png);
            byte[] byteImage = ms.ToArray();
            var SigBase64 = Convert.ToBase64String(byteImage);
            return Content(SigBase64);
        }

        [AllowAnonymous]
        public ActionResult BarcodeDecode(string sid)
        {
            CommonHelper.HashDecodeId(sid, out long decode);
            return Content(decode.ToString());
        }
    }
}