﻿using DataService;
using DataService.ActionServices;
using DataService.DomainModel;
using SelectPdf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VG.Common;

namespace DHCDFrontEnd.Controllers
{
    public class TemplateController : Controller
    {
        [AllowAnonymous]
        public ActionResult Invitation(string eID, string uID)
        {
            int _eventID = 0;
            long _shID = 0;
            if (CommonHelper.HashDecodeId(eID, out _eventID) && CommonHelper.HashDecodeId(uID, out _shID))
            {
                tblStockholderEvent objEvent = DataAccess.GetEventByID(_eventID);
                tblMasterPL objPL = DataAccess.GetPLByID(objEvent.PL_ID);
                tblStockholderInvite objStockHolder = DataAccess.FindStockHolder(_shID);

                InvitationModel Model = new InvitationModel() {
                    SeletedPL = objPL,
                    SeletedEvent = objEvent,
                    SelectedInvite = objStockHolder
                };
                return View(Model);
            }
            else
            {
                return Redirect("/NotFound");
            }
        }

        [AllowAnonymous]
        public ActionResult InvitationEng(string eID, string uID)
        {
            int _eventID = 0;
            long _shID = 0;
            if (CommonHelper.HashDecodeId(eID, out _eventID) && CommonHelper.HashDecodeId(uID, out _shID))
            {
                tblStockholderEvent objEvent = DataAccess.GetEventByID(_eventID);
                tblMasterPL objPL = DataAccess.GetPLByID(objEvent.PL_ID);
                tblStockholderInvite objStockHolder = DataAccess.FindStockHolder(_shID);

                InvitationModel Model = new InvitationModel()
                {
                    SeletedPL = objPL,
                    SeletedEvent = objEvent,
                    SelectedInvite = objStockHolder
                };
                return View(Model);
            }
            else
            {
                return Redirect("/NotFound");
            }
        }

        [AllowAnonymous]
        public ActionResult GetPDF(string eID, string uID)
        {
            int webPageWidth = 1024;
            PdfPageSize pageSize = (PdfPageSize)Enum.Parse(typeof(PdfPageSize), "A4", true);
            PdfPageOrientation pdfOrientation = (PdfPageOrientation)Enum.Parse(typeof(PdfPageOrientation), "Portrait", true);
            HtmlToPdf converter = new HtmlToPdf();

            converter.Options.PdfPageSize = pageSize;
            converter.Options.PdfPageOrientation = pdfOrientation;
            converter.Options.WebPageWidth = webPageWidth;
            string urlGet = string.Format(ConstKey.printInvitationGet, eID, uID);
            PdfDocument doc = converter.ConvertUrl(urlGet);
            byte[] pdf = doc.Save();

            // close pdf document
            doc.Close();

            // return resulted pdf document
            FileResult fileResult = new FileContentResult(pdf, "application/pdf");
            fileResult.FileDownloadName = uID+"_Invitation.pdf";
            return fileResult;
        }

        [Authorize]
        public ActionResult EventDocument(int docID)
        {
            string locationID = Session["LocationID"].ToString();
            int IntLocationID = Int32.Parse(locationID);
            string LocationAddress = Session["LocationAddress"].ToString();
            string _htmlResult = TemplateService.TemplateDataFill(docID, IntLocationID, LocationAddress);
            //chay cac Params rieng cua Template
            ViewBag.ContentReport = _htmlResult;
            return View();
        }

        public ActionResult EventDocumentRaw(int docID)
        {
            //fix danh sach ID báo cáo
            tblEventDoc doc = DataAccess.GetEventDoc(docID);
            return Content(doc.DocTemplate);
        }

        [Authorize]
        public ActionResult TuCachCoDong(int docID)
        {
            tblEventDoc doc = DataAccess.GetEventDoc(docID);
            tblStockholderEvent objEvent = DataAccess.GetEventByID(doc.Event_ID);
            tblMasterPL objPL = DataAccess.GetPLByID(objEvent.PL_ID);

            string searchString = doc.DocTemplate;
            List<tblEventDocDynParam> ListParams = DataAccess.GetDocEventParams(docID);

            List<ParamValue> fillData = new List<ParamValue>();
            fillData.Add(new ParamValue()
            {
                strParam = "{PL_NAME}",
                strValue = objPL.PL_Name
            });
            fillData.Add(new ParamValue()
            {
                strParam = "{PL_NAME_UPPER}",
                strValue = objPL.PL_Name.ToUpper()
            });
            fillData.Add(new ParamValue()
            {
                strParam = "{EVENT_NAME_UPPER}",
                strValue = objEvent.Event_Name_En.ToUpper()
            });
            fillData.Add(new ParamValue()
            {
                strParam = "{EVENT_NAME}",
                strValue = objEvent.Event_Name_En
            });
            fillData.Add(new ParamValue()
            {
                strParam = "{TOTAL_STOCKHOLDER_INVITE}",
                strValue = DataAccess.CountTotalInviteAttend(objEvent.Event_ID).ToString()
            });
            fillData.Add(new ParamValue()
            {
                strParam = "{TOTAL_STOCKHOLDER_ATTEND}",
                strValue = DataAccess.CountTotalInviteAttend(objEvent.Event_ID, 1).ToString()
            });
            fillData.Add(new ParamValue()
            {
                strParam = "{TOTAL_STOCKHOLDER_AUTH}",
                strValue = DataAccess.CountTotalInviteAttend(objEvent.Event_ID, 2).ToString()
            });
            fillData.Add(new ParamValue()
            {
                strParam = "{COUNT_STOCK_ATTEND}",
                strValue = DataAccess.SumStockByStatus(objEvent.Event_ID, 1).ToString()
            });
            fillData.Add(new ParamValue()
            {
                strParam = "{COUNT_STOCK_AUTH}",
                strValue = DataAccess.SumStockByStatus(objEvent.Event_ID, 2).ToString()
            });
            long _TotalStockAttend = DataAccess.SumStockAttend(objEvent.Event_ID);
            long _TotalStockCompany = DataAccess.SumStockCompany(objEvent.Event_ID);
            fillData.Add(new ParamValue()
            {
                strParam = "{TOTAL_STOCK_ATTEND}",
                strValue = _TotalStockAttend.ToString()
            });
            fillData.Add(new ParamValue()
            {
                strParam = "{TOTAL_STOCKPERCENT_ATTEND}",
                strValue = String.Format("{0:C2}", (((decimal)_TotalStockAttend * 100) / (decimal)_TotalStockCompany))
            });

            string _ressult = TemplateEngine.FillTemplate(fillData, searchString);

            ViewBag.ContentReport = _ressult;
            return View();
        }
    }
}