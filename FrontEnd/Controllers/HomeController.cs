﻿using DataService.ActionServices;
using DHCDFrontEnd.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DHCDFrontEnd.Controllers
{
    
    public class HomeController : ControllerExtension
    {
        private IAuthenticationService authenticationService;
        public HomeController()
        {
            authenticationService = new AuthenticationService(new HttpContextWrapper(System.Web.HttpContext.Current));
        }


        [Authorize]
        public ActionResult Index()
        {
            ViewBag.Message = "Home"+ authenticationService.GetAuthenticatedUser().Display_Name;
            return View();
        }


        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}