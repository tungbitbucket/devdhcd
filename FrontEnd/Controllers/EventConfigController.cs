﻿using DataService;
using DataService.ActionServices;
using DataService.DomainModel;
using DHCDFrontEnd.Extensions;
using Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VG.Common;

namespace DHCDFrontEnd.Controllers
{
    [Authorize]
    public class EventConfigController : ControllerExtension
    {
        private IAuthenticationService authenticationService;
        public EventConfigController()
        {
            authenticationService = new AuthenticationService(new HttpContextWrapper(System.Web.HttpContext.Current));
        }

        public ActionResult EConfig(string eID, string searchString, int? page)
        {
            if (!string.IsNullOrEmpty(eID))
            {
                int _eventID = 0;
                if (CommonHelper.HashDecodeId(eID, out _eventID))
                {
                    EventDetail ModelConfig = new EventDetail();

                    ModelConfig.SeletedEvent = DataAccess.GetEventByID(_eventID);
                    ModelConfig.SeletedPL = DataAccess.GetPLByID(ModelConfig.SeletedEvent.PL_ID);
                    ModelConfig.ListStockholder = DataAccess.GetStockholderInvite(_eventID, searchString, page);

                    ViewBag.Showing = "showing "+ ModelConfig.ListStockholder.FirstItemOnPage + "-"+ ModelConfig.ListStockholder.LastItemOnPage + " of "+ ModelConfig.ListStockholder.TotalItemCount + " items";



                    int totalNullBarcode = DataAccess.GetNumNotYetGenBarcode(_eventID);
                    if(totalNullBarcode > 0)
                    {
                        int TotalInvi = DataAccess.CountStockholderInvite(_eventID);
                        ViewBag.BarcodeInfo = "<div class=\"alert alert-warning alert-block\" style=\"margin-bottom:0px;padding:7px;\"><i class=\"fa fa-bell - o\"></i>Warning! Có " + totalNullBarcode + " barcode/"+ TotalInvi + " cổ đông chưa được tạo</div>";
                    }
                    else
                    {
                        ViewBag.BarcodeInfo = "<div class=\"alert alert-success\" style=\"margin-bottom:0px;padding:7px;\"><i class=\"fa fa-check fa-lg\"></i><strong>Well done!</strong> Tất cả Barcode đã được tạo.</div>";
                    }

                    
                    ViewBag.CurrentSearch = searchString;

                    return View(ModelConfig);
                }
                else
                {
                    return Redirect("/NotFound");
                }
            }
            else
            {
                return Redirect("/NotFound");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [CompressFilter(Order = 1)]
        public JsonResult UploadExcel()
        {
            var files = Request.Files;
            try
            {
                var uploadFile = files[0];
                if (uploadFile.FileName.IndexOfAny(Path.GetInvalidFileNameChars()) != -1)
                {
                    return Json(new { status = 0, mes = "File không được chấp nhận" });
                }
                if (string.IsNullOrEmpty(uploadFile.FileName)) { return Json(new { status = 0, mes = "File không được chấp nhận" }); };
                string ext = Path.GetExtension(uploadFile.FileName).Replace(".", "").ToLower();
                if (!((ext == "xlsx") || (ext == "xls") || (ext == "csv"))) { return Json(new { status = 0, mes = "File không được chấp nhận" }); };

                string stEventID = files.GetKey(0);

                string dFolder = stEventID + "/";
                string targetLocation = ConstKey.LocalDir + dFolder;
                if (!Directory.Exists(targetLocation)) Directory.CreateDirectory(targetLocation);

                var path = Path.Combine(targetLocation, CommonHelper.CreateFileName() + "_" + uploadFile.FileName);
                uploadFile.SaveAs(path);

                FileStream streamOpen = System.IO.File.Open(path, FileMode.Open, FileAccess.Read);
                IExcelDataReader excelReader = null;
                switch (ext)
                {
                    case "xls":
                        //Excel 97-03
                        excelReader = ExcelReaderFactory.CreateBinaryReader(streamOpen);
                        break;
                    case "xlsx":
                        //Excel 07
                        excelReader = ExcelReaderFactory.CreateOpenXmlReader(streamOpen);
                        break;
                }

                excelReader.IsFirstRowAsColumnNames = true;
                DataSet result = excelReader.AsDataSet();
                DataTable dtResult = result.Tables[0];

                //STEP 1: IMPORT
                //1.-Save DataTable to some where . I'm temp save to Session :(
                DataImport dataImport = new DataImport()
                {
                   dtResult = dtResult,
                   originalFileName = uploadFile.FileName,
                   excelPath = path
                };
                Session["dtStockhodler"] = dataImport;

                List<string> columnNames = dtResult.Columns.Cast<DataColumn>().Select(x => x.ColumnName).ToList();

                #region Chỉ là tạo Example data để người dùng dễ hình dung khi mapping
                int _loop = (dtResult.Rows.Count > 5) ? 5 : dtResult.Rows.Count;
                string[][] myTable = new string[_loop][];
                for(int i = 0; i < _loop; i++)
                {
                    int columnCount = 0;
                    string[] myTableRow = new string[columnNames.Count];
                    foreach (string cname in columnNames)
                    {
                        myTableRow[columnCount] = dtResult.Rows[i][cname].ToString();
                        columnCount++;
                    }
                    myTable[i] = myTableRow;
                }
                var table2DArray = myTable.ToArray();
                #endregion


                return Json(new { data = columnNames, example = table2DArray, status = 1 });
            }
            catch(Exception ex)
            {
                return Json(new { status = 0, mes = ex.Message });
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [CompressFilter(Order = 1)]
        public JsonResult SaveImportSchema()
        {
            try
            {
                int _eID = 0;
                if (string.IsNullOrEmpty(Request.Form["eID"]))
                {
                    return Json(new { status = 0, mes = "Không có ID sự kiện" });
                }
                else if (!CommonHelper.HashDecodeId(Request.Form["eID"], out _eID))
                {
                    return Json(new { status = 0, mes = "Không có ID sự kiện" });
                }

                DataImport dataImport = (DataImport) Session["dtStockhodler"];

                tblSharreholdImportBatch objBatch = new tblSharreholdImportBatch();
                objBatch.Created_Acc = authenticationService.GetAuthenticatedUser().Acc_ID;
                objBatch.Created_Date = DateTime.Now;
                objBatch.Batch_Status = 0;
                objBatch.Event_ID = _eID;
                objBatch.TotalRow = dataImport.dtResult.Rows.Count;
                objBatch.OriFileName = dataImport.originalFileName;
                objBatch.DirExcel = dataImport.excelPath;
                objBatch.NumInsert = 0;
                objBatch.NumError = 0;
                objBatch.CountNumStock = 0;


                DataAccess.InsertBatchImport(objBatch);
                int _batchID = objBatch.Batch_ID;

                List<tblStockholderInvite> listStockholder = new List<tblStockholderInvite>();
                List<ImportLog> importlong = new List<ImportLog>();

                DateTime CreatedDate = DateTime.Now;
                int accID = authenticationService.GetAuthenticatedUser().Acc_ID;

                int countError = 0;
                int rowIndex = 0;
                long TotalNumStock = 0;
                if (dataImport.dtResult != null && dataImport.dtResult.Rows.Count > 0)
                {
                    foreach (DataRow row in dataImport.dtResult.Rows)
                    {
                        rowIndex++;
                        tblStockholderInvite tempUser = new tblStockholderInvite();

                        #region nhoms NotNull return if null
                        if (Request.Form["opName"] != "---select---")
                        {
                            tempUser.FullName = string.IsNullOrEmpty(row[Request.Form["opName"]].ToString()) ? " " : row[Request.Form["opName"]].ToString();
                        }
                        else
                        {
                            return Json(new { status = -1, mes = "Mapping thiếu dữ liệu chỉ định cho cột không được phép NULL" });
                        }

                        if (Request.Form["opAddress"] != "---select---")
                        {
                            tempUser.Address = string.IsNullOrEmpty(row[Request.Form["opAddress"]].ToString()) ? " " : row[Request.Form["opAddress"]].ToString();
                        }
                        else
                        {
                            return Json(new { status = -1, mes = "Mapping thiếu dữ liệu chỉ định cho cột không được phép NULL" });
                        }

                        if (Request.Form["opQuantity"] != "---select---")
                        {
                            long QuantityValue = 0;
                            if (long.TryParse(row[Request.Form["opQuantity"]].ToString(), out QuantityValue))
                            {
                                tempUser.Quantity = QuantityValue;
                            }
                            else
                            {
                                tempUser.Quantity = 0;
                                countError++;
                                importlong.Add(new ImportLog { RowIndex = rowIndex, Value = row[Request.Form["opQuantity"]].ToString(), Mess = "Số cổ phiếu sai đinh dạng" });
                            }
                        }
                        else
                        {
                            return Json(new { status = -1, mes = "Mapping thiếu dữ liệu chỉ định cho cột không được phép NULL" });
                        }

                        if (Request.Form["opCardID"] != "---select---")
                        {
                            tempUser.CardID = string.IsNullOrEmpty(row[Request.Form["opCardID"]].ToString()) ? " " : row[Request.Form["opCardID"]].ToString();
                        }
                        else
                        {
                            return Json(new { status = -1, mes = "Mapping thiếu dữ liệu chỉ định cho cột không được phép NULL" });
                        }
                        #endregion

                        if (Request.Form["opMobile"] != "---select---")
                        {
                            tempUser.Mobile = string.IsNullOrEmpty(row[Request.Form["opMobile"]].ToString()) ? " " : row[Request.Form["opMobile"]].ToString();
                        }

                        if (Request.Form["opCardDate"] != "---select---")
                        {
                            try
                            {
                                DateTime oCardDate = CommonHelper.ParseRequestDate(row[Request.Form["opCardDate"]].ToString());
                                if (oCardDate.IsInRange())
                                {
                                    tempUser.CardDate = oCardDate;
                                }
                                else
                                {
                                    countError++;
                                    importlong.Add(new ImportLog { RowIndex = rowIndex, Value = row[Request.Form["opCardDate"]].ToString(), Mess = "Ngày cấp CMT cần năm trong (1-1-753) đến (31-12-9999)" });
                                    continue;
                                }
                            }
                            catch
                            {
                                countError++;
                                importlong.Add(new ImportLog { RowIndex = rowIndex, Value = row[Request.Form["opCardDate"]].ToString(), Mess = "Ngày cấp CMT sai định dạng" });
                                continue;
                            }
                        }

                        if (Request.Form["opCardAddress"] != "---select---")
                        {
                            tempUser.CardAddress = string.IsNullOrEmpty(row[Request.Form["opCardAddress"]].ToString()) ? " " : row[Request.Form["opCardAddress"]].ToString();
                        }

                        string abc = Request.Form["opCardResident"];
                        if (Request.Form["opCardResident"] != "---select---")
                        {
                            tempUser.CardResident = string.IsNullOrEmpty(row[Request.Form["opCardResident"]].ToString()) ? " " : row[Request.Form["opCardResident"]].ToString();
                        }

                        if (Request.Form["opBirth"] != "---select---")
                        {
                            try
                            {
                                DateTime oBirth = CommonHelper.ParseRequestDate(row[Request.Form["opBirth"]].ToString());
                                if (oBirth.IsInRange())
                                {
                                    tempUser.BirthDay = oBirth;
                                }
                                else
                                {
                                    countError++;
                                    importlong.Add(new ImportLog { RowIndex = rowIndex, Value = row[Request.Form["opBirth"]].ToString(), Mess = "Ngày sinh cần năm trong (1-1-753) đến (31-12-9999)" });
                                    continue;
                                }
                            }
                            catch
                            {
                                countError++;
                                importlong.Add(new ImportLog { RowIndex = rowIndex, Value = row[Request.Form["opBirth"]].ToString(), Mess = "Ngày sinh sai định dạng" });
                                continue;
                            }
                        }
                        if (Request.Form["opEmail"] != "---select---")
                        {
                            tempUser.Email = string.IsNullOrEmpty(row[Request.Form["opEmail"]].ToString()) ? " " : row[Request.Form["opEmail"]].ToString();
                        }
                        if (Request.Form["opTel"] != "---select---")
                        {
                            tempUser.Tel = string.IsNullOrEmpty(row[Request.Form["opTel"]].ToString()) ? " " : row[Request.Form["opTel"]].ToString();
                        }
                        if (Request.Form["opType"] != "---select---")
                        {
                            //Trong nuoc / quoc te
                            string typeValue = row[Request.Form["opType"]].ToString();
                            if (typeValue.ToUpper().Trim() == "2. NƯỚC NGOÀI" || CommonHelper.UnicodeToKoDau(typeValue.ToUpper().Trim()) == "1. NUOC NGOAI" || typeValue.Trim().Contains("2.") || typeValue.Trim().Contains("2-") || typeValue.Trim().ToLower().Contains("b.") || typeValue.Trim().ToLower().Contains("b-"))
                            {
                                tempUser.sType = 2;
                            }
                            else
                            {
                                tempUser.sType = 1;
                            }
                        }

                        if (Request.Form["opGroup"] != "---select---")
                        {
                            //Ca nhan/ To chuc
                            string groupValue = row[Request.Form["opGroup"]].ToString();
                            if (groupValue.ToUpper().Trim() == "B. TỔ CHỨC" || CommonHelper.UnicodeToKoDau(groupValue.ToUpper().Trim()) == "B. TO CHUC" || groupValue.Trim().Contains("2.") || groupValue.Trim().Contains("2-") || groupValue.Trim().ToLower().Contains("b.") || groupValue.Trim().ToLower().Contains("b-"))
                            {
                                tempUser.sGroup = 2;
                            }
                            else
                            {
                                tempUser.sGroup = 1;
                            }
                        }

                        tempUser.Batch_ID = _batchID;
                        tempUser.Event_ID = _eID;
                        tempUser.CreatedDate = CreatedDate;
                        tempUser.Status = 1;

                        TotalNumStock = TotalNumStock + tempUser.Quantity.Value;

                        listStockholder.Add(tempUser);
                    }

                    DataAccess.ImportStockHolderInvite2(listStockholder);
                    //Quet lai Bulk - kiểm tra số CMT Trùng.

                    //UpdateBatch
                    objBatch.NumError = countError;
                    objBatch.NumInsert = listStockholder.Count;
                    objBatch.CountNumStock = TotalNumStock;
                    DataAccess.UpdateBatchImport(objBatch);


                    Session["dtStockhodler"] = null;
                    return Json(new { status = 1, numerror = countError , numberprocess = dataImport.dtResult.Rows.Count, logerror = importlong });
                }

                 return Json(new { status = 0, mes = "File import không có dữ liệu" });
            }
            catch(Exception ex)
            {
                return Json(new { status = -1, mes = ex.Message + ex.InnerException });
            }
        }

        public ActionResult VotingCard(string eID)
        {
            if (!string.IsNullOrEmpty(eID))
            {
                if (CommonHelper.HashDecodeId(eID, out int _eventID))
                {
                    tblEventVotingCard obj = new tblEventVotingCard();
                    obj = DataAccess.GetEventVotingCard(_eventID);
                    return View();
                }
                else
                {
                    return Redirect("/NotFound");
                }
            }
            else
            {
                return Redirect("/NotFound");
            }
        }
    }
}