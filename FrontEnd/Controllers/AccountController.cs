﻿using DataService;
using DataService.ActionServices;
using DHCDFrontEnd.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DHCDFrontEnd.Controllers
{
    public class AccountController : ControllerExtension
    {
        private IAuthenticationService authenticationService;
        public AccountController()
        {
            authenticationService = new AuthenticationService(new HttpContextWrapper(System.Web.HttpContext.Current));
        }

        [AllowAnonymous]
        public ActionResult Login() => View();

        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(string username, string Password, string ReturnUrl)
        {
            var reponse = authenticationService.Login(username, Password, true);
            if (reponse != null)
            {
                if (!string.IsNullOrEmpty(ReturnUrl))
                {
                    tblLocation Location = DataAccess.GetLocationByAccUser(username);
                    Session["LocationID"] = Location.LocationID;
                    Session["LocationAddress"] = Location.Address;
                    return Redirect(ReturnUrl);
                }
                else
                {
                    var a = Request.Url.GetLeftPart(UriPartial.Authority);
                    return Redirect(a);
                }
            }
            else
            {
                ViewBag.Message = "User khong ton tai";
            }
            return View();
        }

        public ActionResult Logout()
        {
            try
            {
                authenticationService.SignOut();
                return Redirect("/home");
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}