﻿using DataService;
using DataService.ActionServices;
using DataService.DomainModel;
using DHCDFrontEnd.Extensions;
using EasyNetQ;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using VG.Common;

namespace DHCDFrontEnd.Controllers
{
    [Authorize]
    public class EventDasboardController : ControllerExtension
    {
        private IAuthenticationService authenticationService;
        public EventDasboardController()
        {
            authenticationService = new AuthenticationService(new HttpContextWrapper(System.Web.HttpContext.Current));
        }

        public ActionResult Dashboard(string e)
        {
            if (!string.IsNullOrEmpty(e))
            {
                int _eventID = 0;
                if (CommonHelper.HashDecodeId(e, out _eventID))
                {
                    EventDash objDashboard = new EventDash();
                    objDashboard.SeletedEvent = DataAccess.GetEventByID(_eventID);
                    objDashboard.SeletedPL = DataAccess.GetPLByID(objDashboard.SeletedEvent.PL_ID);
                    objDashboard.ListImport = DataAccess.ListBatchByEvent(_eventID);
                    objDashboard.EventVotingCard = DataAccess.GetEventVotingCard(_eventID);
                    objDashboard.HasVote = objDashboard.SeletedEvent.HasVote;
                    objDashboard.HasBKS = objDashboard.SeletedEvent.HasBKS;
                    objDashboard.HasHDQT = objDashboard.SeletedEvent.HasHDQT;

                    long _totalstorkImport = 0;
                    long _totalInsert = 0;
                    foreach (var batch in objDashboard.ListImport)
                    {
                        _totalstorkImport = _totalstorkImport + batch.CountNumStock.Value;
                        _totalInsert = _totalInsert + batch.NumInsert.Value;
                    }
                    decimal percentStock = 0;
                    try{ percentStock = ((long)_totalstorkImport * 100) / (long)objDashboard.SeletedEvent.NumlStock; } catch { percentStock = 0; }
                    ViewBag.TotalStockImport = _totalstorkImport;
                    string CSSClass = (objDashboard.SeletedEvent.NumlStock > _totalstorkImport) ? "text-danger" : "text-success";
                    ViewBag.CSSClass = CSSClass;
                    ViewBag.percentStockImport = percentStock;
                    ViewBag.CountImportBatch = objDashboard.ListImport.Count;

                    long numberCN = DataAccess.CountGroup(_eventID, 1);
                    long percentCN = 0;
                    try { percentCN = (numberCN * 100) / _totalInsert; } catch { percentCN = 0; }
                    ViewBag.percentCN = percentCN;
                    ViewBag.percentTC = 100 - percentCN;

                    int TotalInvi = DataAccess.CountStockholderInvite(_eventID);
                    ViewBag.TotalInvi = TotalInvi;
                    int TotalTN = DataAccess.CountType(_eventID,1);
                    ViewBag.TotalTN = TotalTN;
                    ViewBag.TotalQT = TotalInvi - TotalTN;

                    return View(objDashboard);
                }
                else
                {
                    return Redirect("/NotFound");
                }
            }
            else
            {
                return Redirect("/NotFound");
            }
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult EditEvent()
        {
            try
            {
                string violationChaRegex = "^[^`~!@#$%\\^{}'\"]+$";
                Regex re = new Regex(violationChaRegex);
                int _countError = 0;
                ValidModel validModel = new ValidModel();
                tblStockholderEvent objEvent = new tblStockholderEvent();

                int _eID = 0;
                if (string.IsNullOrEmpty(Request.Form["eID"]))
                {
                    validModel.listViewisValid.Add("ID không tìm thấy");
                    _countError++;
                }
                else if (!CommonHelper.HashDecodeId(Request.Form["eID"], out _eID))
                {
                    validModel.listViewisValid.Add("ID không tìm thấy");
                    _countError++;
                }
                else
                {
                    objEvent = DataAccess.FindEvent(_eID);
                }



                if (string.IsNullOrEmpty(Request.Form["NumlStock"]))
                {
                    validModel.listViewisValid.Add("[Số lượng cổ phiếu] là thông tin bắt buộc");
                    _countError++;
                }
                else
                {
                    if (!re.IsMatch(Request.Form["NumlStock"]))
                    {
                        validModel.listViewisValid.Add("[Số lượng cổ phiếu] không chứa ký tự đặc biệt");
                        _countError++;
                    }
                    else
                    {
                        if (!Regex.IsMatch(Request.Form["NumlStock"], @"^\d+$"))
                        {
                            validModel.listViewisValid.Add("[Số lượng cổ phiếu] chỉ nhận số");
                            _countError++;
                        }
                        else
                        {
                            objEvent.NumlStock = long.Parse(Request.Form["NumlStock"]);
                        }
                    }
                }

                if (string.IsNullOrEmpty(Request.Form["Event_Name"]))
                {
                    validModel.listViewisValid.Add("[Tên sự kiện] là thông tin bắt buộc");
                    _countError++;
                }
                else
                {
                    if (!re.IsMatch(Request.Form["Event_Name"]))
                    {
                        validModel.listViewisValid.Add("[Tên sự kiện] không chứa ký tự đặc biệt");
                        _countError++;
                    }
                    else
                    {
                        objEvent.Event_Name = Request.Form["Event_Name"];
                    }
                }

                if (string.IsNullOrEmpty(Request.Form["Event_Name_En"]))
                {
                    validModel.listViewisValid.Add("[Tên sự kiện Eng] là thông tin bắt buộc");
                    _countError++;
                }
                else
                {
                    if (!re.IsMatch(Request.Form["Event_Name_En"]))
                    {
                        validModel.listViewisValid.Add("[Tên sự kiện Eng] không chứa ký tự đặc biệt");
                        _countError++;
                    }
                    else
                    {
                        objEvent.Event_Name_En = Request.Form["Event_Name_En"];
                    }
                }


                if (string.IsNullOrEmpty(Request.Form["Event_Year"]))
                {
                    validModel.listViewisValid.Add("[Năm sự kiện] là thông tin bắt buộc");
                    _countError++;
                }
                else
                {
                    objEvent.Event_Year = Request.Form["Event_Year"];
                }


                if (string.IsNullOrEmpty(Request.Form["Location"]))
                {
                    validModel.listViewisValid.Add("[Địa chỉ tổ chức] là thông tin bắt buộc");
                    _countError++;
                }
                else
                {
                    if (!re.IsMatch(Request.Form["Location"]))
                    {
                        validModel.listViewisValid.Add("[Địa chỉ tổ chức] không chứa ký tự đặc biệt");
                        _countError++;
                    }
                    else
                    {
                        objEvent.Location = Request.Form["Location"];
                    }

                }

                if (string.IsNullOrEmpty(Request.Form["Location_En"]))
                {
                    validModel.listViewisValid.Add("[Địa chỉ tổ chức Eng] là thông tin bắt buộc");
                    _countError++;
                }
                else
                {
                    if (!re.IsMatch(Request.Form["Location_En"]))
                    {
                        validModel.listViewisValid.Add("[Địa chỉ tổ chức Eng] không chứa ký tự đặc biệt");
                        _countError++;
                    }
                    else
                    {
                        objEvent.Location_En = Request.Form["Location_En"];
                    }
                }


                if (string.IsNullOrEmpty(Request.Form["FromDate"]))
                {
                    validModel.listViewisValid.Add("[Ngày bắt đầu] là thông tin bắt buộc");
                    _countError++;
                }
                else
                {
                    objEvent.FromDate = DateTime.ParseExact(Request.Form["FromDate"], "dd/MM/yyyy", CultureInfo.InvariantCulture);
                }
                if (string.IsNullOrEmpty(Request.Form["ToDate"]))
                {
                    validModel.listViewisValid.Add("[Ngày kết thúc] là thông tin bắt buộc");
                    _countError++;
                }
                else
                {
                    objEvent.ToDate = DateTime.ParseExact(Request.Form["ToDate"], "dd/MM/yyyy", CultureInfo.InvariantCulture);
                }
                if (!string.IsNullOrEmpty(Request.Form["Note"]))
                {
                    objEvent.Note = Request.Form["Note"];
                }

                int value = DateTime.Compare(objEvent.FromDate, objEvent.ToDate);
                if (value > 0)
                {
                    validModel.listViewisValid.Add("[Ngày kết thúc] nhỏ hơn [Ngày bắt đầu]");
                    _countError++;
                }

                if (_countError > 0)
                {
                    return Json(new { status = 0, litmess = validModel });
                }
                else
                {
                    objEvent.Created_Acc = authenticationService.GetAuthenticatedUser().Acc_ID;
                    int statusUpdate = DataAccess.UpdateEvent(objEvent);
                    return Json(new { status = 1, rref = statusUpdate });
                }
            }
            catch (Exception ex)
            {
                return Json(new { status = -1, mes = ex.Message });
            }
        }

        [HttpPost]
        public async Task<JsonResult> Download(string eID)
        {
            int _eventID = 0;
            if (CommonHelper.HashDecodeId(eID, out _eventID))
            {
                List<tblStockholderInvite> cus = DataAccess.GetStockholderInvite(_eventID);

                using (var bus = RabbitHutch.CreateBus(ConstKey.RabbitConnect))
                {
                    foreach (tblStockholderInvite item in cus)
                    {
                        mQInvitation2 mqInvite = new mQInvitation2()
                        {
                            EventID = CommonHelper.HashEncodeId(item.Event_ID),
                            StockHolderID = CommonHelper.HashEncodeId(item.SHID),
                            sType = item.sType.Value
                        };
                        await bus.PublishAsync(mqInvite);
                    }
                }

                return Json(new { status = 1, mes = "Download order has been sent" });
            }
            return Json(new { status = 0, mes = "null params" });
        }

        [HttpPost]
        public async Task<JsonResult> DeleteBatch(int bID)
        {
            DataAccess.DeleteBatchImport(bID);
            //Xoa file vat ly
            return Json(new { status = 1, mes = "Batch đã được xóa" });
        }


        [HttpPost]
        public async Task<JsonResult> OnEventConfig(int type, int value, string eID)
        {
            int _eventID = 0;
            if (CommonHelper.HashDecodeId(eID, out _eventID))
            {
                int statusUpdate = DataAccess.EventChangeConfig(_eventID, type, value.ToBool());
                return Json(new { status = 1, mes = statusUpdate });
            }
            return Json(new { status = 0, mes = "null params" });
        }
    }
}