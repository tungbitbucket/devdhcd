﻿using DataService.DomainModel;
using DHCDFrontEnd.Extensions;
using EasyNetQ;
using System;
using System.Threading.Tasks;
using System.Web.Mvc;
using VG.Common;

namespace DHCDFrontEnd.Controllers
{
    [Authorize]
    public class EventBusController : ControllerExtension
    {
        [HttpPost]
        public async Task<JsonResult> EventMessPrinting()
        {
            try
            {
                if (!string.IsNullOrEmpty(Request.Form["eID"]) && (!string.IsNullOrEmpty(Request.Form["uID"])))
                {
                    using (var bus = RabbitHutch.CreateBus(ConstKey.RabbitConnect))
                    {
                        mQInvitation mqInvite = new mQInvitation()
                        {
                            EventID = Request.Form["eID"],
                            StockHolderID = Request.Form["uID"]
                        };
                        await bus.PublishAsync(mqInvite);
                    }
                    return Json(new { status = 1, mes = "print order has been sent" });
                }
                else
                {
                    return Json(new { status = 0, mes = "null params" });
                }
            }
            catch (Exception ex)
            {
                return Json(new { status = 0, mes = ex.Message });
            }
        }

        [HttpPost]
        public async Task<JsonResult> GenAllBarcode()
        {
            try
            {
                if (!string.IsNullOrEmpty(Request.Form["eID"]))
                {
                    int _eventID = 0;
                    CommonHelper.HashDecodeId(Request.Form["eID"], out _eventID);

                    using (var bus = RabbitHutch.CreateBus(ConstKey.RabbitConnect))
                    {
                        mQGenbarcode mqInvite = new mQGenbarcode()
                        {
                            EventID = _eventID
                        };
                        await bus.PublishAsync(mqInvite);
                    }
                    return Json(new { status = 1, mes = "print order has been sent" });
                }
                else
                {
                    return Json(new { status = 0, mes = "null params" });
                }
            }
            catch (Exception ex)
            {
                return Json(new { status = 0, mes = ex.Message });
            }
        }
    }
}