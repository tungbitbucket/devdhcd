﻿using DataService;
using DataService.ActionServices;
using DataService.DomainModel;
using DHCDFrontEnd.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VG.Common;

namespace DHCDFrontEnd.Controllers
{
    [Authorize]
    public class ReportController : ControllerExtension
    {
        private IAuthenticationService authenticationService;
        public ReportController()
        {
            authenticationService = new AuthenticationService(new HttpContextWrapper(System.Web.HttpContext.Current));
        }

        public ActionResult ReportMaster(string eID, string doc)
        {
            try
            {
                if (!string.IsNullOrEmpty(eID))
                {
                    int _eventID = 0;
                    if (CommonHelper.HashDecodeId(eID, out _eventID))
                    {
                        EReports eReports = new EReports();
                        eReports.SeletedEvent = DataAccess.GetEventByID(_eventID);
                        eReports.SeletedPL = DataAccess.GetPLByID(eReports.SeletedEvent.PL_ID);
                        eReports.ListEventDoc = DataAccess.GetAllDocOfEvent(_eventID, 2);

                        if (!string.IsNullOrEmpty(doc))
                        {
                            eReports.SeletedDocument = eReports.ListEventDoc.SingleOrDefault(o => o.EventDoc_ID == int.Parse(doc));
                        }
                        else
                        {
                            eReports.SeletedDocument = eReports.ListEventDoc.FirstOrDefault();

                        }
                        List<tblEventDocDynParam> listDocParams = DataAccess.GetDocEventParams(eReports.SeletedDocument.EventDoc_ID);
                        //Loc system params
                        List<ParamValue> lstSysParams = DataAccess.GetListSysParams(true);
                        foreach (tblEventDocDynParam param in listDocParams)
                        {
                            if (lstSysParams.Any(p => p.strParam == param.Param_name))
                            {
                                eReports.ListSysParams.Add(param);
                            }
                            else
                            {
                                eReports.ListCusParams.Add(param);
                            }
                        }

                        return View(eReports);
                    }
                    else
                    {
                        return Redirect("/NotFound");
                    }
                }
                else
                {
                    return Redirect("/NotFound");
                }
            }
            catch
            {
                return Redirect("/NotFound");
            }
        }


        [HttpPost]
        [CompressFilter(Order = 1)]
        public JsonResult checkPrinted()
        {
            try
            {
                if (!string.IsNullOrEmpty(Request.Form["eID"]) && (!string.IsNullOrEmpty(Request.Form["uID"])))
                {
                    if (CommonHelper.HashDecodeId(Request.Form["eID"].Trim(), out int _eventID) && CommonHelper.HashDecodeId(Request.Form["uID"].Trim(), out long _shID))
                    {
                        tblVotePrinted printed = new tblVotePrinted()
                        {
                            EventID = _eventID,
                            ShID = _shID,
                            PrintDate = DateTime.Now,
                        };
                        int result = DataAccess.InsertMarkPrinted(printed);
                        return Json(new { status = result, mes = "" });
                    }
                    else
                    {
                        return Json(new { status = -1, mes = "Mã số cổ đông không hợp lệ" });
                    }
                }
                else
                {
                    return Json(new { status = -1, mes = "Thông tin kiểm tra không đủ" });
                }
            }
            catch(Exception ex)
            {
                return Json(new { status = -1, mes = ex.Message });
            }
        }

        public int getLocationID()
        {
            if (!string.IsNullOrEmpty(Session["LocationID"].ToString()))
            {
                string locationID = Session["LocationID"].ToString();
                return Int32.Parse(locationID);
            }
            return 0;
        }

        [HttpPost]
        [CompressFilter(Order = 1)]
        public JsonResult checkElectionPrinted()
        {
            try
            {
                if (!string.IsNullOrEmpty(Request.Form["eID"]) && (!string.IsNullOrEmpty(Request.Form["uID"])) && (!string.IsNullOrEmpty(Request.Form["type"])))
                {
                    if (CommonHelper.HashDecodeId(Request.Form["eID"].Trim(), out int _eventID) && CommonHelper.HashDecodeId(Request.Form["uID"].Trim(), out long _shID))
                    {
                        tblCandidateVotePrinted printed = new tblCandidateVotePrinted()
                        {
                            EventID = _eventID,
                            ShID = _shID,
                            PrintDate = DateTime.Now,
                            CandidateType = byte.Parse(Request.Form["type"].Trim()),
                            LocationID = getLocationID()
                        };

                        int result = DataAccess.InserElectionPrinted(printed);
                        return Json(new { status = result, mes = "" });
                    }
                    else
                    {
                        return Json(new { status = -1, mes = "Mã số cổ đông không hợp lệ" });
                    }
                }
                else
                {
                    return Json(new { status = -1, mes = "Thông tin kiểm tra không đủ" });
                }
            }
            catch (Exception ex)
            {
                return Json(new { status = -1, mes = ex.Message });
            }
        }


        public ActionResult VoteTopic(string eID, string uID, string lang)
        {
            string url = "";
            if (lang == "vn")
            {
                url = string.Format("/Report/VoteTopicVN?eID={0}&uID={1}", eID, uID);
                
            }
            else
            {
                url = string.Format("/Report/VoteTopicEN?eID={0}&uID={1}", eID, uID);
            }
            return Redirect(url);
        }



        public ActionResult VoteTopicVN(string eID, string uID)
        {
            try
            {
                if (!string.IsNullOrEmpty(eID) && !string.IsNullOrEmpty(uID))
                {
                    int _eventID = 0;
                    CommonHelper.HashDecodeId(eID, out _eventID);
                    long _userID = 0;
                    CommonHelper.HashDecodeId(uID, out _userID);

                    VotingCardModel votingCardModel = new VotingCardModel();
                    tblStockholderEvent SelectedEvent = DataAccess.GetEventByID(_eventID);
                    var objPL = DataAccess.GetPLByID(SelectedEvent.PL_ID);
                    tblStockholderInvite stockholder = DataAccess.FindStockHolder(_eventID, _userID);

                    votingCardModel.Event_ID = SelectedEvent.Event_ID;
                    votingCardModel.EventHashID = CommonHelper.HashEncodeId(SelectedEvent.Event_ID);
                    votingCardModel.Event_Name = SelectedEvent.Event_Name.ToUpper();
                    votingCardModel.PL_ID = objPL.PL_ID;
                    votingCardModel.PLName = objPL.PL_Name.ToUpper();
                    votingCardModel.StockholderName = stockholder.FullName;
                    votingCardModel.StockHolderCode = CommonHelper.HashEncodeId(stockholder.SHID);
                    votingCardModel.StockHolderID = stockholder.SHID;
                    votingCardModel.StockHolderBarcodeCode64 = stockholder.BarCode;
                    votingCardModel.Quantity = stockholder.Quantity.Value;
                    votingCardModel.VotingTopic = DataAccess.GetListVotingCardTopicByEvent(_eventID, 1);


                    //kiem tra da tao Barcode Vote chua
                    int countBarcode = DataAccess.CheckExistBarcodeVote(_userID, _eventID);
                    int temp = votingCardModel.VotingTopic.Count * 4;
                    if (countBarcode != temp)
                    {
                        //Xoa het barcode da tao, tao lai bo barcode moi
                        List<tblBarcodeVote> genBarcodevote = new List<tblBarcodeVote>();
                        foreach (tblVotingCardTopic vote in votingCardModel.VotingTopic)
                        {
                            //0: dong y
                            //1: khong dong y
                            //2: ko co y kien
                            //3: khong hop le
                            for (int i = 0; i < 4; i++)
                            {
                                genBarcodevote.Add(new tblBarcodeVote()
                                {
                                    EventID = _eventID,
                                    ShID = _userID,
                                    VotingTopicID = vote.VotingTopicID,
                                    Result = i
                                });
                            }
                        }
                        DataAccess.GenBarcodeVote(genBarcodevote, _userID, _eventID);
                    }

                    List<tblBarcodeVote> GetBarcodeForVotingcard = DataAccess.GetBarcodeForVotingcard(_userID, _eventID);
                    List<VoteSubject> lstSubject = new List<VoteSubject>();

                    int _index = 1;
                    foreach (tblVotingCardTopic topic in votingCardModel.VotingTopic)
                    {
                        VoteSubject vote = new VoteSubject();
                        vote.SubjectNo = _index;
                        vote.Name = topic.TopicNameVN;
                        GetBarcodeForVotingcard.Where(b => b.VotingTopicID == topic.VotingTopicID && b.Result != 3).OrderBy(b => b.Result).ToList().ForEach(bc => vote.presentBarcodes.Add(new PresentBarcode()
                        {
                            BarcodeID = bc.barcodeID,
                            Barcode = CommonHelper.BarcodeToBase64(bc.barcodeID),
                            Title = CommonHelper.VoteOption(bc.Result)
                        }));

                        lstSubject.Add(vote);
                        _index++;
                    }
                    votingCardModel.VotingItems = lstSubject;
                    ViewBag.Agree = CommonHelper.BarcodeToBase64(_userID+":"+ _eventID + ":0");

                    return View(votingCardModel);
                }
                else
                {
                    return Redirect("/NotFound");
                }
            }
            catch
            {
                return Redirect("/NotFound");
            }
        }

        public ActionResult VoteTopicEN(string eID, string uID)
        {
            try
            {
                if (!string.IsNullOrEmpty(eID) && !string.IsNullOrEmpty(uID))
                {
                    int _eventID = 0;
                    CommonHelper.HashDecodeId(eID, out _eventID);
                    long _userID = 0;
                    CommonHelper.HashDecodeId(uID, out _userID);

                    VotingCardModel votingCardModel = new VotingCardModel();
                    tblStockholderEvent SelectedEvent = DataAccess.GetEventByID(_eventID);
                    var objPL = DataAccess.GetPLByID(SelectedEvent.PL_ID);
                    tblStockholderInvite stockholder = DataAccess.FindStockHolder(_eventID, _userID);

                    votingCardModel.Event_ID = SelectedEvent.Event_ID;
                    votingCardModel.EventHashID = CommonHelper.HashEncodeId(SelectedEvent.Event_ID);
                    votingCardModel.Event_Name = SelectedEvent.Event_Name_En.ToUpper();
                    votingCardModel.PL_ID = objPL.PL_ID;
                    votingCardModel.PLName = objPL.PL_Name_En.ToUpper();
                    votingCardModel.StockholderName = stockholder.FullName;
                    votingCardModel.StockHolderCode = CommonHelper.HashEncodeId(stockholder.SHID);
                    votingCardModel.StockHolderID = stockholder.SHID;
                    votingCardModel.StockHolderBarcodeCode64 = stockholder.BarCode;
                    votingCardModel.Quantity = stockholder.Quantity.Value;
                    votingCardModel.VotingTopic = DataAccess.GetListVotingCardTopicByEvent(_eventID, 1);


                    //kiem tra da tao Barcode Vote chua
                    int countBarcode = DataAccess.CheckExistBarcodeVote(_userID, _eventID);
                    int temp = votingCardModel.VotingTopic.Count * 4;
                    if (countBarcode != temp)
                    {
                        //Xoa het barcode da tao, tao lai bo barcode moi
                        List<tblBarcodeVote> genBarcodevote = new List<tblBarcodeVote>();
                        foreach (tblVotingCardTopic vote in votingCardModel.VotingTopic)
                        {
                            //0: dong y
                            //1: khong dong y
                            //2: ko co y kien
                            //3: khong hop le
                            for (int i = 0; i < 4; i++)
                            {
                                genBarcodevote.Add(new tblBarcodeVote()
                                {
                                    EventID = _eventID,
                                    ShID = _userID,
                                    VotingTopicID = vote.VotingTopicID,
                                    Result = i
                                });
                            }
                        }
                        DataAccess.GenBarcodeVote(genBarcodevote, _userID, _eventID);
                    }

                    List<tblBarcodeVote> GetBarcodeForVotingcard = DataAccess.GetBarcodeForVotingcard(_userID, _eventID);
                    List<VoteSubject> lstSubject = new List<VoteSubject>();

                    int _index = 1;
                    foreach (tblVotingCardTopic topic in votingCardModel.VotingTopic)
                    {
                        VoteSubject vote = new VoteSubject();
                        vote.SubjectNo = _index;
                        vote.Name = topic.TopicName_En;
                        GetBarcodeForVotingcard.Where(b => b.VotingTopicID == topic.VotingTopicID && b.Result != 3).OrderBy(b => b.Result).ToList().ForEach(bc => vote.presentBarcodes.Add(new PresentBarcode()
                        {
                            BarcodeID = bc.barcodeID,
                            Barcode = CommonHelper.BarcodeToBase64(bc.barcodeID),
                            Title = CommonHelper.VoteOptionEN(bc.Result)
                        }));

                        lstSubject.Add(vote);
                        _index++;
                    }
                    votingCardModel.VotingItems = lstSubject;
                    ViewBag.Agree = CommonHelper.BarcodeToBase64(_userID + ":" + _eventID + ":0");

                    return View(votingCardModel);
                }
                else
                {
                    return Redirect("/NotFound");
                }
            }
            catch
            {
                return Redirect("/NotFound");
            }
        }

        public ActionResult ElectionBKS(string eID, string uID, string lang)
        {
            string url = "";
            if (lang == "vn")
            {
                url = string.Format("/Report/ElectionBKSVN?eID={0}&uID={1}", eID, uID);

            }
            else
            {
                url = string.Format("/Report/ElectionBKSEN?eID={0}&uID={1}", eID, uID);
            }
            return Redirect(url);
        }

        public ActionResult ElectionBKSVN(string eID, string uID)
        {
            try
            {
                if (!string.IsNullOrEmpty(eID) && !string.IsNullOrEmpty(uID))
                {
                    int _eventID = 0;
                    CommonHelper.HashDecodeId(eID, out _eventID);
                    long _userID = 0;
                    CommonHelper.HashDecodeId(uID, out _userID);

                    ElectionModel electionModel = new ElectionModel();
                    tblStockholderEvent SelectedEvent = DataAccess.GetEventByID(_eventID);
                    var objPL = DataAccess.GetPLByID(SelectedEvent.PL_ID);
                    tblStockholderInvite stockholder = DataAccess.FindStockHolder(_eventID, _userID);

                    electionModel.Event_ID = SelectedEvent.Event_ID;
                    electionModel.EventHashID = CommonHelper.HashEncodeId(SelectedEvent.Event_ID);
                    electionModel.Event_Name = SelectedEvent.Event_Name.ToUpper();
                    electionModel.PL_ID = objPL.PL_ID;
                    electionModel.PLName = objPL.PL_Name.ToUpper();
                    electionModel.StockholderName = stockholder.FullName;
                    electionModel.StockHolderCode = CommonHelper.HashEncodeId(stockholder.SHID);
                    electionModel.StockHolderID = stockholder.SHID;
                    electionModel.StockHolderBarcodeCode64 = stockholder.BarCode;
                    electionModel.Quantity = stockholder.Quantity.Value;

                    electionModel.lstCandidates = DataAccess.GetListCandidateByEventandType(_eventID, 1);

                    electionModel.CountCandidates = electionModel.lstCandidates.Count;
                    electionModel.TotalQuantity = (electionModel.Quantity * electionModel.CountCandidates);

                    return View(electionModel);
                }
                else
                {
                    return Redirect("/NotFound");
                }
            }
            catch
            {
                return Redirect("/NotFound");
            }
        }

        public ActionResult ElectionBKSEN(string eID, string uID)
        {
            try
            {
                if (!string.IsNullOrEmpty(eID) && !string.IsNullOrEmpty(uID))
                {
                    int _eventID = 0;
                    CommonHelper.HashDecodeId(eID, out _eventID);
                    long _userID = 0;
                    CommonHelper.HashDecodeId(uID, out _userID);

                    ElectionModel electionModel = new ElectionModel();
                    tblStockholderEvent SelectedEvent = DataAccess.GetEventByID(_eventID);
                    var objPL = DataAccess.GetPLByID(SelectedEvent.PL_ID);
                    tblStockholderInvite stockholder = DataAccess.FindStockHolder(_eventID, _userID);

                    electionModel.Event_ID = SelectedEvent.Event_ID;
                    electionModel.EventHashID = CommonHelper.HashEncodeId(SelectedEvent.Event_ID);
                    electionModel.Event_Name = SelectedEvent.Event_Name_En.ToUpper();
                    electionModel.PL_ID = objPL.PL_ID;
                    electionModel.PLName = objPL.PL_Name_En.ToUpper();
                    electionModel.StockholderName = stockholder.FullName;
                    electionModel.StockHolderCode = CommonHelper.HashEncodeId(stockholder.SHID);
                    electionModel.StockHolderID = stockholder.SHID;
                    electionModel.StockHolderBarcodeCode64 = stockholder.BarCode;
                    electionModel.Quantity = stockholder.Quantity.Value;

                    electionModel.lstCandidates = DataAccess.GetListCandidateByEventandType(_eventID, 1);

                    electionModel.CountCandidates = electionModel.lstCandidates.Count;
                    electionModel.TotalQuantity = (electionModel.Quantity * electionModel.CountCandidates);

                    return View(electionModel);
                }
                else
                {
                    return Redirect("/NotFound");
                }
            }
            catch
            {
                return Redirect("/NotFound");
            }
        }

        public ActionResult ElectionBOD(string eID, string uID, string lang)
        {
            string url = "";
            if (lang == "vn")
            {
                url = string.Format("/Report/ElectionBODVN?eID={0}&uID={1}", eID, uID);

            }
            else
            {
                url = string.Format("/Report/ElectionBODEN?eID={0}&uID={1}", eID, uID);
            }
            return Redirect(url);
        }

        public ActionResult ElectionBODVN(string eID, string uID)
        {
            try
            {
                if (!string.IsNullOrEmpty(eID) && !string.IsNullOrEmpty(uID))
                {
                    int _eventID = 0;
                    CommonHelper.HashDecodeId(eID, out _eventID);
                    long _userID = 0;
                    CommonHelper.HashDecodeId(uID, out _userID);

                    ElectionModel electionModel = new ElectionModel();
                    tblStockholderEvent SelectedEvent = DataAccess.GetEventByID(_eventID);
                    var objPL = DataAccess.GetPLByID(SelectedEvent.PL_ID);
                    tblStockholderInvite stockholder = DataAccess.FindStockHolder(_eventID, _userID);

                    electionModel.Event_ID = SelectedEvent.Event_ID;
                    electionModel.EventHashID = CommonHelper.HashEncodeId(SelectedEvent.Event_ID);
                    electionModel.Event_Name = SelectedEvent.Event_Name.ToUpper();
                    electionModel.PL_ID = objPL.PL_ID;
                    electionModel.PLName = objPL.PL_Name.ToUpper();
                    electionModel.StockholderName = stockholder.FullName;
                    electionModel.StockHolderCode = CommonHelper.HashEncodeId(stockholder.SHID);
                    electionModel.StockHolderID = stockholder.SHID;
                    electionModel.StockHolderBarcodeCode64 = stockholder.BarCode;
                    electionModel.Quantity = stockholder.Quantity.Value;

                    electionModel.lstCandidates = DataAccess.GetListCandidateByEventandType(_eventID, 2);

                    electionModel.CountCandidates = electionModel.lstCandidates.Count;
                    electionModel.TotalQuantity = (electionModel.Quantity * electionModel.CountCandidates);

                    return View(electionModel);
                }
                else
                {
                    return Redirect("/NotFound");
                }
            }
            catch
            {
                return Redirect("/NotFound");
            }
        }

        public ActionResult ElectionBODEN(string eID, string uID)
        {
            try
            {
                if (!string.IsNullOrEmpty(eID) && !string.IsNullOrEmpty(uID))
                {
                    int _eventID = 0;
                    CommonHelper.HashDecodeId(eID, out _eventID);
                    long _userID = 0;
                    CommonHelper.HashDecodeId(uID, out _userID);

                    ElectionModel electionModel = new ElectionModel();
                    tblStockholderEvent SelectedEvent = DataAccess.GetEventByID(_eventID);
                    var objPL = DataAccess.GetPLByID(SelectedEvent.PL_ID);
                    tblStockholderInvite stockholder = DataAccess.FindStockHolder(_eventID, _userID);

                    electionModel.Event_ID = SelectedEvent.Event_ID;
                    electionModel.EventHashID = CommonHelper.HashEncodeId(SelectedEvent.Event_ID);
                    electionModel.Event_Name = SelectedEvent.Event_Name_En.ToUpper();
                    electionModel.PL_ID = objPL.PL_ID;
                    electionModel.PLName = objPL.PL_Name_En.ToUpper();
                    electionModel.StockholderName = stockholder.FullName;
                    electionModel.StockHolderCode = CommonHelper.HashEncodeId(stockholder.SHID);
                    electionModel.StockHolderID = stockholder.SHID;
                    electionModel.StockHolderBarcodeCode64 = stockholder.BarCode;
                    electionModel.Quantity = stockholder.Quantity.Value;

                    electionModel.lstCandidates = DataAccess.GetListCandidateByEventandType(_eventID, 2);

                    electionModel.CountCandidates = electionModel.lstCandidates.Count;
                    electionModel.TotalQuantity = (electionModel.Quantity * electionModel.CountCandidates);

                    return View(electionModel);
                }
                else
                {
                    return Redirect("/NotFound");
                }
            }
            catch
            {
                return Redirect("/NotFound");
            }
        }


        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public JsonResult UpdateDoc()
        {
            try
            {
                int docID = int.Parse(Request.Form["DocID"]);
                tblEventDoc docEvent = DataAccess.GetEventDoc(docID);

                int EventID = docEvent.EventDoc_ID;
                string docLang = Request.Form["SelectLang"];
                docEvent.DocName = CommonHelper.HtmlStrip(Request.Form["DocName"]);
                docEvent.DocTemplate = CommonHelper.repSC(Request.Form["DocTemplate"]);
                if (DataAccess.UpdateEventDoc(docEvent) == 1)
                {
                    List<ParamValue> lstParams = TemplateEngine.GetListDynamicParams(docEvent.DocTemplate);
                    if(lstParams.Count > 0)
                    {
                        List<tblEventDocDynParam> listDocParams = new List<tblEventDocDynParam>();
                        lstParams.ForEach(p => listDocParams.Add(new tblEventDocDynParam() {
                            EventDoc_ID = EventID,
                            Param_name = p.strParam,
                            Param_Value = p.strValue
                        }));

                        DataAccess.UpdateEventDocParams(listDocParams);
                    }
                    else
                    {
                        DataAccess.ClearEventDocParams(EventID);
                    }
                    return Json(new { status = 1, mes = "Okie" });
                }
                else
                {
                    return Json(new { status = -1, mes = "Lưu template thất bại" });
                }
            }
            catch(Exception ex)
            {
                return Json(new { status = -1, mes = ex.Message });
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public JsonResult SaveParamvalue(int p, string v)
        {
            try
            {
                int statusUpdate = DataAccess.UpdateEventParamValue(p, v);
                return Json(new { status = 1, rref = statusUpdate });
            }
            catch (Exception ex)
            {
                return Json(new { status = -1, mes = ex.Message });
            }
        }
    }
}