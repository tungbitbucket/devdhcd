﻿using DataService;
using System;
using System.Collections.Generic;
using System.Threading;
using VG.Common;

namespace ThreadTest
{
    class Program
    {
        static void Main(string[] args)
        {
            //for (int i = 1; i <= 26; i++)
            //{
            //    Thread oThreadone = new Thread(Program.GenPage);
            //    oThreadone.Start(i);
            //}
            Thread oThreadone = new Thread(Program.GenPage);
            oThreadone.Start(1);

            Console.Read();
        }

        public static void GenPage(object page)
        {
            int idPage = (int)page;

            tblStockholderEvent objEvent = DataAccess.GetEventByID(15);
            tblMasterPL objPL = DataAccess.GetPLByID(objEvent.PL_ID);

            string TemplateVNSource = InlineTemplateEngine.GetTemplate("InviteVN_VEFAC.html");
            string TemplateENSource = InlineTemplateEngine.GetTemplate("InviteVN_VEFAC.html");
            string _ressult = "";

            int webPageWidth = 1024;
            SelectPdf.PdfPageSize pageSize = (SelectPdf.PdfPageSize)Enum.Parse(typeof(SelectPdf.PdfPageSize), "A4", true);
            SelectPdf.PdfPageOrientation pdfOrientation = (SelectPdf.PdfPageOrientation)Enum.Parse(typeof(SelectPdf.PdfPageOrientation), "Portrait", true);
            SelectPdf.HtmlToPdf converter = new SelectPdf.HtmlToPdf();
            converter.Options.PdfPageSize = pageSize;
            converter.Options.PdfPageOrientation = pdfOrientation;
            converter.Options.WebPageWidth = webPageWidth;


            //List<tblStockholderInvite> listcus = DataAccess.GetStockholderInvite(9, idPage);
            List<tblStockholderInvite> listcus = DataAccess.GetStockholderInviteCustom(15, idPage);
            Console.WriteLine("----Start Gen page {0}----", idPage);

            foreach (tblStockholderInvite cus in listcus)
            {
                //string _fileName = "D:/VIC_QAS/VN/" + CommonHelper.UnicodeToKoDauAndGach(cus.FullName) + "_" + CommonHelper.HashEncodeId(cus.SHID) + "_Invi.pdf";
                //if (!File.Exists(_fileName))
                //{
                    List<ParamValue> fillData = new List<ParamValue>();
                    //if (cus.sType == 1)
                    {
                        fillData.Add(new ParamValue()
                        {
                            strParam = "{PL_NAME_UPPER}",
                            strValue = objPL.PL_Name.ToUpper()
                        });
                        fillData.Add(new ParamValue()
                        {
                            strParam = "{PL_NAME}",
                            strValue = objPL.PL_Name
                        });
                        fillData.Add(new ParamValue()
                        {
                            strParam = "{EVENT_NAME_UPPER}",
                            strValue = objEvent.Event_Name.ToUpper()
                        });
                        fillData.Add(new ParamValue()
                        {
                            strParam = "{EVENT_NAME}",
                            strValue = objEvent.Event_Name
                        });
                        fillData.Add(new ParamValue()
                        {
                            strParam = "{INVITE_FULLNAME}",
                            strValue = cus.FullName
                        });
                        fillData.Add(new ParamValue()
                        {
                            strParam = "{INVITE_ADDRESS}",
                            strValue = cus.Address
                        });
                        fillData.Add(new ParamValue()
                        {
                            strParam = "{INVITE_MOBILE}",
                            strValue = cus.Mobile
                        });
                        fillData.Add(new ParamValue()
                        {
                            strParam = "{INVITE_ID_CODE}",
                            strValue = CommonHelper.HashEncodeId(cus.SHID)
                        });
                        fillData.Add(new ParamValue()
                        {
                            strParam = "{INVITE_BARCODE}",
                            strValue = cus.BarCode
                        });
                        fillData.Add(new ParamValue()
                        {
                            strParam = "{EVENT_LOCATION}",
                            strValue = objEvent.Location
                        });
                        fillData.Add(new ParamValue()
                        {
                            strParam = "{EVENT_FROMDATE}",
                            strValue = VnDateConvert.ConvertDateTimeTempVN(objEvent.FromDate)
                        });

                        _ressult = TemplateEngine.FillTemplate(fillData, TemplateVNSource);
                    }
                    //else
                    //{
                    //    fillData.Add(new ParamValue()
                    //    {
                    //        strParam = "{PL_NAME_UPPER}",
                    //        strValue = objPL.PL_Name_En.ToUpper()
                    //    });
                    //    fillData.Add(new ParamValue()
                    //    {
                    //        strParam = "{PL_NAME}",
                    //        strValue = objPL.PL_Name_En
                    //    });
                    //    fillData.Add(new ParamValue()
                    //    {
                    //        strParam = "{EVENT_NAME_UPPER}",
                    //        strValue = objEvent.Event_Name_En.ToUpper()
                    //    });
                    //    fillData.Add(new ParamValue()
                    //    {
                    //        strParam = "{EVENT_NAME}",
                    //        strValue = objEvent.Event_Name_En
                    //    });
                    //    fillData.Add(new ParamValue()
                    //    {
                    //        strParam = "{INVITE_FULLNAME}",
                    //        strValue = cus.FullName
                    //    });
                    //    fillData.Add(new ParamValue()
                    //    {
                    //        strParam = "{INVITE_ADDRESS}",
                    //        strValue = cus.Address
                    //    });
                    //    fillData.Add(new ParamValue()
                    //    {
                    //        strParam = "{INVITE_MOBILE}",
                    //        strValue = cus.Mobile
                    //    });
                    //    fillData.Add(new ParamValue()
                    //    {
                    //        strParam = "{INVITE_ID_CODE}",
                    //        strValue = CommonHelper.HashEncodeId(cus.SHID)
                    //    });
                    //    fillData.Add(new ParamValue()
                    //    {
                    //        strParam = "{INVITE_BARCODE}",
                    //        strValue = cus.BarCode
                    //    });
                    //    fillData.Add(new ParamValue()
                    //    {
                    //        strParam = "{EVENT_LOCATION}",
                    //        strValue = objEvent.Location_En
                    //    });
                    //    fillData.Add(new ParamValue()
                    //    {
                    //        strParam = "{EVENT_FROMDATE}",
                    //        strValue = VnDateConvert.ConvertDateTimeTempEN(objEvent.FromDate)
                    //    });

                    //    _ressult = TemplateEngine.FillTemplate(fillData, TemplateENSource);
                    //}

                    SelectPdf.PdfDocument docuri = converter.ConvertHtmlString(_ressult);
                    //if (cus.sType == 1)
                    {
                        docuri.Save("D:/VEFAC-2/" + CommonHelper.UnicodeToKoDauAndGach(cus.FullName) + "_" + CommonHelper.HashEncodeId(cus.SHID) + "_Invi.pdf");
                    }
                    //else
                    //{
                    //    docuri.Save("D:/Data_ChinhThuc_SaiDong/SAIDONG_INVI_2019/EN/" + CommonHelper.UnicodeToKoDauAndGach(cus.FullName) + "_" + CommonHelper.HashEncodeId(cus.SHID) + "_Invi.pdf");
                    //}

                Console.WriteLine("--DONE for " + CommonHelper.UnicodeToKoDauAndGach(cus.FullName));
                //}
            }
        }
    }
}

public class InlineTemplateEngine
{
    public static string GetTemplate(string fileName)
    {
        string fileContents = string.Empty;
        string path = System.Reflection.Assembly.GetExecutingAssembly().Location;
        var directory = System.IO.Path.GetDirectoryName(path);
        fileContents = System.IO.File.ReadAllText(directory + "\\Content\\InvitationTemplates\\" + fileName);
        return fileContents;
    }
}
