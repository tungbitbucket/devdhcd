﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataService.DomainModel
{
    public class ElectionModel
    {
        public ElectionModel()
        {
            lstCandidates = new List<tblCandidate>();
        }
        public int PL_ID { get; set; }
        public int Event_ID { get; set; }
        public string EventHashID { get; set; }
        public string PLName { get; set; }
        public string Event_Name { get; set; }
        public string StockholderName { get; set; }
        public string StockHolderCode { get; set; }
        public long StockHolderID { get; set; }

        public string StockHolderBarcodeCode64 { get; set; }
        public long Quantity { get; set; }
        public int CountCandidates { get; set; }
        public long TotalQuantity { get; set; }
        public byte ElectionType { get; set; }
        public List<tblCandidate> lstCandidates { get; set; }
    }
}
