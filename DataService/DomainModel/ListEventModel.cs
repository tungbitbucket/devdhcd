﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataService.DomainModel
{
    public class ListEventModel
    {
        public ListEventModel()
        {
            listPL = new List<tblMasterPL>();
            listEventByPL = new List<tblStockholderEvent>();
        }
        public List<tblMasterPL> listPL { get; set; }
        public tblMasterPL SeletedPL { get; set; }
        public List<tblStockholderEvent> listEventByPL { get; set; }
    }
}
