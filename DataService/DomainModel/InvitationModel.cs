﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataService.DomainModel
{
    public class InvitationModel
    {
        public tblMasterPL SeletedPL { get; set; }
        public tblStockholderEvent SeletedEvent { get; set; }
        public tblStockholderInvite SelectedInvite { get; set; }
    }
}
