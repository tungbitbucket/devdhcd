﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataService.DomainModel
{
    public class VoteSubject
    {
        public VoteSubject()
        {
            presentBarcodes = new List<PresentBarcode>();
        }
        public int SubjectNo { get; set; }
        public string Name { get; set; }
        public List<PresentBarcode> presentBarcodes { get; set; }
    }

    public class PresentBarcode
    {
        public long BarcodeID { get; set; }
        public string Title { get; set; }
        public string Barcode { get; set; }
    }
}
