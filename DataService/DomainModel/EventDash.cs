﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataService.DomainModel
{
    public class EventDash
    {
        public EventDash()
        {
            ListImport = new List<tblSharreholdImportBatch>();
            ListEventReport = new List<tblEventReport>();
        }
        public tblMasterPL SeletedPL { get; set; }
        public tblStockholderEvent SeletedEvent { get; set; }
        public List<tblSharreholdImportBatch> ListImport { get; set; }
        public tblEventVotingCard EventVotingCard { get; set; }
        public List<tblEventReport> ListEventReport { get; set; }
        public bool HasBKS { get; set; }
        public bool HasHDQT { get; set; }
        public bool HasVote { get; set; }

    }
}
