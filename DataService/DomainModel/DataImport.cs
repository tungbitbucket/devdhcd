﻿using System.Data;

namespace DataService.DomainModel
{
    public class DataImport
    {
        public DataTable dtResult { get; set; }
        public string originalFileName { get; set; }
        public string excelPath { get; set; }
    }
}
