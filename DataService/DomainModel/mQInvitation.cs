﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataService.DomainModel
{
    public class mQInvitation
    {
        public string EventID { get; set; }
        public string StockHolderID { get; set; }
    }

    public class mQGenbarcode
    {
        public int EventID { get; set; }
    }

    public class mQVoteCounting
    {
        public long barcodeID { get; set; }
    }
    public class mQVoteCountingAll
    {
        public long ShID { get; set; }
        public int TopicID { get; set; }
        public int Result { get; set; }
    }
}
