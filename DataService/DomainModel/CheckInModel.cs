﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataService.DomainModel
{
    public class CheckInModel
    {
        public CheckInModel()
        {
            HasVotingCard = false;
        }
        public int PL_ID { get; set; }
        public int Event_ID { get; set; }
        public string EventHashID { get; set; }
        public string PLName { get; set; }
        public string Event_Name { get; set; }
        public bool HasVotingCard { get; set; }
        public bool HasBSK { get; set; }
        public bool HasHDQT { get; set; }
    }
}
