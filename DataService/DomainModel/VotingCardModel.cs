﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataService.DomainModel
{
    public class VotingCardModel
    {
        public VotingCardModel()
        {
            VotingTopic = new List<tblVotingCardTopic>();
            VotingItems = new List<VoteSubject>();
        }
        public int PL_ID { get; set; }
        public int Event_ID { get; set; }
        public string EventHashID { get; set; }
        public string PLName { get; set; }
        public string Event_Name { get; set; }
        public string StockholderName { get; set; }
        public string StockHolderCode { get; set; }
        public long StockHolderID { get; set; }

        public string StockHolderBarcodeCode64 { get; set; }
        public long Quantity { get; set; }

        public List<tblVotingCardTopic> VotingTopic { get; set; }
        public List<VoteSubject> VotingItems { get; set; }
    }
}
