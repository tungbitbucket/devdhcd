﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataService.DomainModel
{
    public class Identity
    {
        public Identity()
        {
            Acc_Role = new List<Role>();
        }
        public int Acc_ID { get; set; }
        public string Display_Name { get; set; }
        public List<Role> Acc_Role { get; set; }
    }

    public class Role
    {
        public int Role_ID { get; set; }
        public string RoleFunName { get; set; }
        public string Role_Name { get; set; }
    }

}
