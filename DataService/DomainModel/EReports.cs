﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataService.DomainModel
{
    public class EReports
    {
        public EReports()
        {
            ListSysParams = new List<tblEventDocDynParam>();
            ListCusParams = new List<tblEventDocDynParam>();
            ListEventDoc = new List<tblEventDoc>();
        }
        public tblMasterPL SeletedPL { get; set; }
        public tblStockholderEvent SeletedEvent { get; set; }
        public List<tblSharreholdImportBatch> ListImport { get; set; }
        public List<tblEventDoc> ListEventDoc { get; set; }
        public tblEventDoc SeletedDocument { get; set; }
        public List<tblEventDocDynParam> ListSysParams { get; set; }
        public List<tblEventDocDynParam> ListCusParams { get; set; }
    }
}
