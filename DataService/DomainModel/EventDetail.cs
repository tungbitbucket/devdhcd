﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataService.DomainModel
{
    public class EventDetail
    {
        public EventDetail()
        {
            ListImport = new List<tblSharreholdImportBatch>();
        }
        public tblMasterPL SeletedPL { get; set; }
        public tblStockholderEvent SeletedEvent { get; set; }
        public List<tblSharreholdImportBatch> ListImport { get; set; }
        public PagedList.IPagedList<tblStockholderInvite> ListStockholder { get; set; }
    }
}
