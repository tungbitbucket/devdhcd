﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataService.DomainModel
{
    public class ImportLog
    {
        public int RowIndex { get; set; }
        public string Value { get; set; }
        public string Mess { get; set; }
    }
}
