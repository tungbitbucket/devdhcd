﻿using DataService.DomainModel;
using FormsAuthenticationExtensions;
using System;
using System.Collections.Specialized;
using System.Web;
using System.Web.Security;
using VG.Common;

namespace DataService.ActionServices
{
    public partial interface IAuthenticationService
    {
        void SignIn(Identity user, bool createPersistentCookie);
        void SignOut();
        Identity Login(string userName, string password, bool remember);
        bool CheckUserSignIn();
        Identity GetAuthenticatedUser();
    }

    public partial class AuthenticationService : IAuthenticationService
    {
        private readonly HttpContextBase _httpContext;

        public AuthenticationService(HttpContextBase httpContext)
        {
            this._httpContext = httpContext;
        }

        public bool CheckUserSignIn()
        {
            bool val1 = (System.Web.HttpContext.Current.User != null) && System.Web.HttpContext.Current.User.Identity.IsAuthenticated;
            return val1;
        }

        public Identity GetAuthenticatedUser()
        {
            var ticketData = ((FormsIdentity)HttpContext.Current.User.Identity).Ticket.GetStructuredUserData();
            var rq = new Identity
            {
                Acc_ID = ticketData["Acc_ID"].ToInt32OrDefault(),
                Display_Name = ticketData["Display_Name"]
            };
            return rq;

            //If you want something even simple, you can also just pass a string in:
            //new FormsAuthentication().SetAuthCookie(user.UserId, true, "arbitrary string here");
            //and read it back via:
            //var userData = ((FormsIdentity)User.Identity).Ticket.UserData;
        }

        public Identity Login(string userName, string password, bool remember)
        {
            try
            {
                var objUser = DataAccess.SysUserLogin(userName, password);
                if (objUser != null)
                {
                    var rq = new Identity
                    {
                        Acc_ID = objUser.Acc_ID,
                        Display_Name = objUser.Acc_DisplayName
                    };
                    SignIn(rq, remember);

                    return rq;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public void SignIn(Identity user, bool createPersistentCookie)
        {
            try
            {
                var ticketData = new NameValueCollection
                {
                    { "Acc_ID", user.Acc_ID.ToString() },
                    { "Display_Name", user.Display_Name }
                };

                new FormsAuthentication().SetAuthCookie(user.Display_Name, createPersistentCookie, ticketData, DateTime.Now.AddMonths(1));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SignOut()
        {
            FormsAuthentication.SignOut();
        }
    }
}
