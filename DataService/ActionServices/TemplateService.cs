﻿using System;
using System.Collections.Generic;
using System.Linq;
using VG.Common;

namespace DataService.ActionServices
{
    public class TemplateService
    {
        public static string TemplateDataFill(int docID, int LocationID, string LocationAddress)
        {
            tblEventDoc doc = DataAccess.GetEventDoc(docID);
            List<tblEventDocDynParam> docParams = DataAccess.GetDocEventParams(doc.EventDoc_ID);
            tblStockholderEvent objEvent = DataAccess.GetEventByID(doc.Event_ID);
            tblMasterPL objPL = DataAccess.GetPLByID(objEvent.PL_ID);
            tblEventVotingCard tbleventVote = null;
            List<ParamValue> lstSysParams = DataAccess.GetListSysParams(true);
            List<ParamValue> fillData = new List<ParamValue>();


            foreach (tblEventDocDynParam param in docParams)
            {
                if (lstSysParams.Any(p => p.strParam == param.Param_name))
                {
                    switch (param.Param_name)
                    {
                        case "{SYS_EVENT_NAME_UPPER}":
                            {
                                fillData.Add(new ParamValue()
                                {
                                    strParam = "{SYS_EVENT_NAME_UPPER}",
                                    strValue = (doc.Doc_Lang == "vn") ? objEvent.Event_Name.ToUpper() : objEvent.Event_Name_En.ToUpper()
                                });
                            }
                            break;
                        case "{SYS_EVENT_NAME}":
                            {
                                fillData.Add(new ParamValue()
                                {
                                    strParam = "{SYS_EVENT_NAME}",
                                    strValue = (doc.Doc_Lang == "vn") ? objEvent.Event_Name : objEvent.Event_Name_En
                                });
                            }
                            break;
                        case "{SYS_PL_NAME_UPPER}":
                            {
                                fillData.Add(new ParamValue()
                                {
                                    strParam = "{SYS_PL_NAME_UPPER}",
                                    strValue = (doc.Doc_Lang == "vn") ? objPL.PL_Name.ToUpper() : objPL.PL_Name_En.ToUpper()
                                });
                            }
                            break;
                        case "{SYS_PL_NAME}":
                            {
                                fillData.Add(new ParamValue()
                                {
                                    strParam = "{SYS_PL_NAME}",
                                    strValue = (doc.Doc_Lang == "vn") ? objPL.PL_Name : objPL.PL_Name
                                });
                            }
                            break;
                        case "{SYS_EVENT_LOCATION}":
                            {
                                fillData.Add(new ParamValue()
                                {
                                    strParam = "{SYS_EVENT_LOCATION}",
                                    strValue = (doc.Doc_Lang == "vn") ? objEvent.Location : objEvent.Location_En
                                });
                            }
                            break;
                        case "{SYS_EVENT_FROMDATE}":
                            {
                                fillData.Add(new ParamValue()
                                {
                                    strParam = "{SYS_EVENT_FROMDATE}",
                                    strValue = (doc.Doc_Lang == "vn") ? VnDateConvert.ConvertDateTimeTempVN(objEvent.FromDate) : VnDateConvert.ConvertDateTimeTempEN(objEvent.FromDate)
                                });
                            }
                            break;
                        case "{SYS_TOTAL_STOCKHOLDER_INVITE}":
                            {
                                fillData.Add(new ParamValue()
                                {
                                    strParam = "{SYS_TOTAL_STOCKHOLDER_INVITE}",
                                    strValue = (doc.Doc_Lang == "vn") ? CommonHelper.GetDecimalFormart(DataAccess.CountTotalInviteAttend(objEvent.Event_ID),"vn") : CommonHelper.GetDecimalFormart(DataAccess.CountTotalInviteAttend(objEvent.Event_ID), "en")
                                });
                            }
                            break;
                        case "{SYS_TOTAL_STOCKHOLDER_ATTEND}":
                            {
                                fillData.Add(new ParamValue()
                                {
                                    strParam = "{SYS_TOTAL_STOCKHOLDER_ATTEND}",
                                    strValue = (doc.Doc_Lang == "vn") ? CommonHelper.GetDecimalFormart(DataAccess.CountTotalInviteAttend(objEvent.Event_ID, 1),"vn") : CommonHelper.GetDecimalFormart(DataAccess.CountTotalInviteAttend(objEvent.Event_ID, 1), "en")
                                });
                            }
                            break;
                        case "{SYS_TOTAL_STOCKHOLDER_ATTEND_CAU}":
                            {
                                fillData.Add(new ParamValue()
                                {
                                    strParam = "{SYS_TOTAL_STOCKHOLDER_ATTEND_CAU}",
                                    strValue = (doc.Doc_Lang == "vn") ? CommonHelper.GetDecimalFormart(DataAccess.CountTotalInviteAttendCau(objEvent.Event_ID, 1, LocationID), "vn") : CommonHelper.GetDecimalFormart(DataAccess.CountTotalInviteAttendCau(objEvent.Event_ID, 1, LocationID), "en")
                                });
                            }
                            break;
                        case "{SYS_COUNT_STOCK_ATTEND}":
                            {
                                fillData.Add(new ParamValue()
                                {
                                    strParam = "{SYS_COUNT_STOCK_ATTEND}",
                                    strValue = (doc.Doc_Lang == "vn") ? CommonHelper.GetDecimalFormart(DataAccess.SumStockByStatus(objEvent.Event_ID, 1),"vn") : CommonHelper.GetDecimalFormart(DataAccess.SumStockByStatus(objEvent.Event_ID, 1), "en")
                                });
                            }
                            break;
                        case "{SYS_COUNT_STOCK_ATTEND_CAU}":
                            {
                                fillData.Add(new ParamValue()
                                {
                                    strParam = "{SYS_COUNT_STOCK_ATTEND_CAU}",
                                    strValue = (doc.Doc_Lang == "vn") ? CommonHelper.GetDecimalFormart(DataAccess.SumStockByStatusCau(objEvent.Event_ID, 1, LocationID), "vn") : CommonHelper.GetDecimalFormart(DataAccess.SumStockByStatusCau(objEvent.Event_ID, 1, LocationID), "en")
                                });
                            }
                            break;
                        case "{SYS_TOTAL_STOCKHOLDER_AUTH}":
                            {
                                fillData.Add(new ParamValue()
                                {
                                    strParam = "{SYS_TOTAL_STOCKHOLDER_AUTH}",
                                    strValue = (doc.Doc_Lang == "vn") ? CommonHelper.GetDecimalFormart(DataAccess.CountTotalInviteAttend(objEvent.Event_ID, 2),"vn"): CommonHelper.GetDecimalFormart(DataAccess.CountTotalInviteAttend(objEvent.Event_ID, 2), "en")
                                });
                            }
                            break;

                        case "{SYS_TOTAL_STOCKHOLDER_AUTH_CAU}":
                            {
                                fillData.Add(new ParamValue()
                                {
                                    strParam = "{SYS_TOTAL_STOCKHOLDER_AUTH_CAU}",
                                    strValue = (doc.Doc_Lang == "vn") ? CommonHelper.GetDecimalFormart(DataAccess.CountTotalInviteAttendCau(objEvent.Event_ID, 2, LocationID), "vn") : CommonHelper.GetDecimalFormart(DataAccess.CountTotalInviteAttendCau(objEvent.Event_ID, 2, LocationID), "en")
                                });
                            }
                            break;
                        case "{SYS_TOTAL_STOCK_ATTEND}":
                            {
                                fillData.Add(new ParamValue()
                                {
                                    strParam = "{SYS_TOTAL_STOCK_ATTEND}",
                                    strValue = (doc.Doc_Lang == "vn") ? CommonHelper.GetDecimalFormart(DataAccess.SumStockAttend(objEvent.Event_ID),"vn") : CommonHelper.GetDecimalFormart(DataAccess.SumStockAttend(objEvent.Event_ID), "en")
                                });
                            }
                            break;
                        case "{SYS_COUNT_STOCK_AUTH_CAU}":
                            {
                                fillData.Add(new ParamValue()
                                {
                                    strParam = "{SYS_COUNT_STOCK_AUTH_CAU}",
                                    strValue = (doc.Doc_Lang == "vn") ? CommonHelper.GetDecimalFormart(DataAccess.SumStockByStatusCau(objEvent.Event_ID, 2, LocationID),"vn") : CommonHelper.GetDecimalFormart(DataAccess.SumStockByStatusCau(objEvent.Event_ID, 2, LocationID), "en")
                                });
                            }
                            break;
                        case "{SYS_TOTAL_STOCKPERCENT_ATTEND}":
                            {
                                double _TotalStockAttend = DataAccess.SumStockAttend(objEvent.Event_ID);
                                double _TotalStockCompany = DataAccess.SumStockCompany(objEvent.Event_ID);
                                fillData.Add(new ParamValue()
                                {
                                    strParam = "{SYS_TOTAL_STOCKPERCENT_ATTEND}",
                                    strValue = (doc.Doc_Lang == "vn") ? CommonHelper.GetPercent(_TotalStockAttend, _TotalStockCompany).GetNumberDisplayVN() : CommonHelper.GetPercent(_TotalStockAttend, _TotalStockCompany).GetNumberDisplayUS()
                                });
                            }
                            break;
                        case "{TODAY_SHORT}":
                            {
                                fillData.Add(new ParamValue()
                                {
                                    strParam = "{TODAY_SHORT}",
                                    strValue = (doc.Doc_Lang == "vn") ? VnDateConvert.ConvertDateTime04(DateTime.Now) : DateTime.Now.ToString("dd MMMM yyyy")
                                });
                            }
                            break;
                        case "{TODAY_LONG}":
                            {
                                fillData.Add(new ParamValue()
                                {
                                    strParam = "{TODAY_LONG}",
                                    strValue = (doc.Doc_Lang == "vn") ? VnDateConvert.ConvertDateTime10(DateTime.Now) : VnDateConvert.ConvertDateTimeTempEN2(DateTime.Now)
                                });
                            }
                            break;
                        case "{SYS_TOTAL_VOTE_PUBLISH}":
                            {
                                if (tbleventVote == null)
                                {
                                    tbleventVote = DataAccess.GetEventVotingCard(doc.Event_ID);
                                }
                                fillData.Add(new ParamValue()
                                {
                                    strParam = "{SYS_TOTAL_VOTE_PUBLISH}",
                                    strValue = (doc.Doc_Lang == "vn") ? CommonHelper.GetDecimalFormart(DataAccess.TotalVotePublish(tbleventVote.EventID),"vn"): CommonHelper.GetDecimalFormart(DataAccess.TotalVotePublish(tbleventVote.EventID), "en")
                                });
                            }
                            break;
                        case "{SYS_TOTAL_STOCK_PUBLISH}":
                            {
                                if (tbleventVote == null)
                                {
                                    tbleventVote = DataAccess.GetEventVotingCard(doc.Event_ID);
                                }
                                fillData.Add(new ParamValue()
                                {
                                    strParam = "{SYS_TOTAL_STOCK_PUBLISH}",
                                    strValue = (doc.Doc_Lang == "vn") ? CommonHelper.GetDecimalFormart(DataAccess.SYS_TOTAL_STOCK_PUBLISH(tbleventVote.EventID),"vn") : CommonHelper.GetDecimalFormart(DataAccess.SYS_TOTAL_STOCK_PUBLISH(tbleventVote.EventID), "en")
                                });
                            }
                            break;
                        case "{SYS_VOTE_STOCK_PERCENT}":
                            {
                                if (tbleventVote == null)
                                {
                                    tbleventVote = DataAccess.GetEventVotingCard(doc.Event_ID);
                                }
                                double _TotalStockAttend = DataAccess.SumStockAttend(objEvent.Event_ID);
                                double _TotalStockPublish = DataAccess.SYS_TOTAL_STOCK_PUBLISH(tbleventVote.EventID);
                                fillData.Add(new ParamValue()
                                {
                                    strParam = "{SYS_VOTE_STOCK_PERCENT}",
                                    strValue = (doc.Doc_Lang == "vn") ? CommonHelper.GetPercent(_TotalStockPublish, _TotalStockAttend).GetNumberDisplayVN() : CommonHelper.GetPercent(_TotalStockPublish, _TotalStockAttend).GetNumberDisplayUS()
                                });
                            }
                            break;
                        case "{SYS_VOTE_RESULT_COUNT}":
                            {
                                if (tbleventVote == null)
                                {
                                    tbleventVote = DataAccess.GetEventVotingCard(doc.Event_ID);
                                }
                                fillData.Add(new ParamValue()
                                {
                                    //Tổng số cổ phần trên số phiếu thu về: Tính gộp cả số phiếu không hợp lệ 
                                    strParam = "{SYS_VOTE_RESULT_COUNT}",
                                    strValue = (doc.Doc_Lang == "vn") ? CommonHelper.GetDecimalFormart(DataAccess.TotalVoteReturn(tbleventVote.EventID), "vn") : CommonHelper.GetDecimalFormart(DataAccess.TotalVoteReturn(tbleventVote.EventID), "en")
                                });
                            }
                            break;
                        case "{SYS_SUM_STOCK_RESULT}":
                            {
                                if (tbleventVote == null)
                                {
                                    tbleventVote = DataAccess.GetEventVotingCard(doc.Event_ID);
                                }
                                fillData.Add(new ParamValue()
                                {
                                    strParam = "{SYS_SUM_STOCK_RESULT}",
                                    strValue = (doc.Doc_Lang == "vn") ? CommonHelper.GetDecimalFormart(DataAccess.SYS_SUM_STOCK_RESULT(tbleventVote.EventID), "vn") : CommonHelper.GetDecimalFormart(DataAccess.SYS_SUM_STOCK_RESULT(tbleventVote.EventID), "en")
                                });
                            }
                            break;
                        case "{SYS_SUM_STOCK_RESULT__PERCENT}":
                            {
                                if (tbleventVote == null)
                                {
                                    tbleventVote = DataAccess.GetEventVotingCard(doc.Event_ID);
                                }
                                double _TotalStockAttend = DataAccess.SumStockAttend(objEvent.Event_ID);
                                double _TotalStockPublish = DataAccess.SYS_SUM_STOCK_RESULT(tbleventVote.EventID);
                                fillData.Add(new ParamValue()
                                {
                                    strParam = "{SYS_SUM_STOCK_RESULT__PERCENT}",
                                    strValue = (doc.Doc_Lang == "vn") ? CommonHelper.GetPercent(_TotalStockPublish, _TotalStockAttend).GetNumberDisplayVN() : CommonHelper.GetPercent(_TotalStockPublish, _TotalStockAttend).GetNumberDisplayUS()
                                });
                            }
                            break;
                        case "{SYS_SO_THE_HOP_LE}":
                            {
                                if (tbleventVote == null)
                                {
                                    tbleventVote = DataAccess.GetEventVotingCard(doc.Event_ID);
                                }
                                fillData.Add(new ParamValue()
                                {
                                    //Tổng số cổ phần trên số phiếu thu về: chi tinh ca the hop le 
                                    strParam = "{SYS_SO_THE_HOP_LE}",
                                    strValue = (doc.Doc_Lang == "vn") ? CommonHelper.GetDecimalFormart(DataAccess.TotalVoteReturn(tbleventVote.EventID, false), "vn") : CommonHelper.GetDecimalFormart(DataAccess.TotalVoteReturn(tbleventVote.EventID, false), "en")
                                });
                            }
                            break;
                        case "{SYS_SO_THE_KO_HOP_LE}":
                            {
                                if (tbleventVote == null)
                                {
                                    tbleventVote = DataAccess.GetEventVotingCard(doc.Event_ID);
                                }
                                fillData.Add(new ParamValue()
                                {
                                    //Tổng số cổ phần trên số phiếu thu về: chi tinh ca the hop le 
                                    strParam = "{SYS_SO_THE_KO_HOP_LE}",
                                    strValue = (doc.Doc_Lang == "vn") ? CommonHelper.GetDecimalFormart(DataAccess.TotalVoteReturn(tbleventVote.EventID, true), "vn") : CommonHelper.GetDecimalFormart(DataAccess.TotalVoteReturn(tbleventVote.EventID, true), "en")
                                });
                            }
                            break;
                        case "{SYS_SO_CO_PHAN_THE_HOP_LE}":
                            {
                                if (tbleventVote == null)
                                {
                                    tbleventVote = DataAccess.GetEventVotingCard(doc.Event_ID);
                                }
                                fillData.Add(new ParamValue()
                                {
                                    strParam = "{SYS_SO_CO_PHAN_THE_HOP_LE}",
                                    strValue = (doc.Doc_Lang == "vn") ? CommonHelper.GetDecimalFormart(DataAccess.SYS_SUM_STOCK_RESULT_BASE_ON_INVALID(tbleventVote.EventID, false), "vn") : CommonHelper.GetDecimalFormart(DataAccess.SYS_SUM_STOCK_RESULT_BASE_ON_INVALID(tbleventVote.EventID, false), "en")
                                });
                            }
                            break;
                        case "{SYS_SO_CO_PHAN_THE_KO_HOP_LE}":
                            {
                                if (tbleventVote == null)
                                {
                                    tbleventVote = DataAccess.GetEventVotingCard(doc.Event_ID);
                                }
                                fillData.Add(new ParamValue()
                                {
                                    strParam = "{SYS_SO_CO_PHAN_THE_KO_HOP_LE}",
                                    strValue = (doc.Doc_Lang == "vn") ? CommonHelper.GetDecimalFormart(DataAccess.SYS_SUM_STOCK_RESULT_BASE_ON_INVALID(tbleventVote.EventID, true), "vn") : CommonHelper.GetDecimalFormart(DataAccess.SYS_SUM_STOCK_RESULT_BASE_ON_INVALID(tbleventVote.EventID, true), "en")
                                });
                            }
                            break;
                        case "{SYS_SO_CO_PHAN_THE_HOP_LE_PERCENT}":
                            {
                                if (tbleventVote == null)
                                {
                                    tbleventVote = DataAccess.GetEventVotingCard(doc.Event_ID);
                                }
                                double _TotalStockAttend = DataAccess.SumStockAttend(objEvent.Event_ID);
                                double _TotalStockValid = DataAccess.SYS_SUM_STOCK_RESULT_BASE_ON_INVALID(tbleventVote.EventID, false);
                                fillData.Add(new ParamValue()
                                {
                                    strParam = "{SYS_SO_CO_PHAN_THE_HOP_LE_PERCENT}",
                                    strValue = (doc.Doc_Lang == "vn") ? CommonHelper.GetPercent(_TotalStockValid, _TotalStockAttend).GetNumberDisplayVN() : CommonHelper.GetPercent(_TotalStockValid, _TotalStockAttend).GetNumberDisplayUS()
                                });
                            }
                            break;
                        case "{SYS_SO_CO_PHAN_THE_KO_HOP_LE_PERCENT}":
                            {
                                if (tbleventVote == null)
                                {
                                    tbleventVote = DataAccess.GetEventVotingCard(doc.Event_ID);
                                }
                                double _TotalStockAttend = DataAccess.SumStockAttend(objEvent.Event_ID);
                                double _TotalStockValid = DataAccess.SYS_SUM_STOCK_RESULT_BASE_ON_INVALID(tbleventVote.EventID, true);
                                fillData.Add(new ParamValue()
                                {
                                    strParam = "{SYS_SO_CO_PHAN_THE_KO_HOP_LE_PERCENT}",
                                    strValue = (doc.Doc_Lang == "vn") ? CommonHelper.GetPercent(_TotalStockValid, _TotalStockAttend).GetNumberDisplayVN() : CommonHelper.GetPercent(_TotalStockValid, _TotalStockAttend).GetNumberDisplayUS()
                                });
                            }
                            break;
                        case "{SYS_LOOP_TOPIC}":
                            {
                                string ResultHTML = "";
                                if (tbleventVote == null)
                                {
                                    tbleventVote = DataAccess.GetEventVotingCard(doc.Event_ID);
                                }
                                double _SoCoPhanThuVe = DataAccess.SYS_SUM_STOCK_RESULT(tbleventVote.EventID);
                                //double _SoCoPhanThuVe = DataAccess.SYS_SUM_STOCK_RESULT_BASE_ON_INVALID(tbleventVote.EventID, false);

                                tblEventDoc itemTemplate = (doc.Doc_Lang == "vn") ? DataAccess.GetEventDoc(30) : DataAccess.GetEventDoc(49);
                                List<ParamValue> fillDataItem = TemplateEngine.GetListDynamicParams(itemTemplate.DocTemplate);
                                //lay danh sach cau hoi
                                List<tblVotingCardTopic> lstTopic = DataAccess.GetListVotingCardTopicByEvent(doc.Event_ID, 1);
                                foreach(var TopicItem in lstTopic)
                                {
                                    foreach (ParamValue itemparam in fillDataItem)
                                    {
                                        //Tong so co phan rải vào từng cau hỏi, bao gom ca các cac ko hop le
                                        long TotalStockBaseOnTopic = DataAccess.TotalSumStockResultBaseOnTopicID(doc.Event_ID, TopicItem.VotingTopicID);
                                        long stockAgree = DataAccess.SumStockResultBaseOnTopicID(doc.Event_ID, TopicItem.VotingTopicID, 0);
                                        long stockReject = DataAccess.SumStockResultBaseOnTopicID(doc.Event_ID, TopicItem.VotingTopicID, 1);
                                        long stockOther = DataAccess.SumStockResultBaseOnTopicID(doc.Event_ID, TopicItem.VotingTopicID, 2);
                                        long stockInvalid = DataAccess.SumStockResultBaseOnTopicID(doc.Event_ID, TopicItem.VotingTopicID, 3);

                                        switch (itemparam.strParam)
                                        {
                                            case "{SYS_VOTE_NAME}":
                                                {
                                                    itemparam.strValue = (doc.Doc_Lang == "vn") ? TopicItem.TopicNameVN : TopicItem.TopicName_En;
                                                }
                                                break;
                                            case "{SYS_AGREE}":
                                                {
                                                    itemparam.strValue = (doc.Doc_Lang == "vn") ? CommonHelper.GetDecimalFormart(stockAgree, "vn") : CommonHelper.GetDecimalFormart(stockAgree, "en");
                                                }
                                                break;
                                            case "{SYS_AGREE_PERCENT}":
                                                {
                                                    //itemparam.strValue = CommonHelper.GetPercent((double)stockAgree,(double) TotalStockBaseOnTopic).GetNumberDisplayVN();
                                                    itemparam.strValue = (doc.Doc_Lang == "vn") ? CommonHelper.GetPercent((double)stockAgree, (double)_SoCoPhanThuVe).GetNumberDisplayVN() : CommonHelper.GetPercent((double)stockAgree, (double)_SoCoPhanThuVe).GetNumberDisplayUS();
                                                }
                                                break;
                                            case "{SYS_REJECT}":
                                                {
                                                    itemparam.strValue = (doc.Doc_Lang == "vn") ? CommonHelper.GetDecimalFormart(stockReject, "vn") : CommonHelper.GetDecimalFormart(stockReject, "en");
                                                }
                                                break;
                                            case "{SYS_REJECT_PERCENT}":
                                                {
                                                    //itemparam.strValue = CommonHelper.GetPercent((double)stockReject, (double)TotalStockBaseOnTopic).GetNumberDisplayVN();
                                                    itemparam.strValue = (doc.Doc_Lang == "vn") ? CommonHelper.GetPercent((double)stockReject, (double)_SoCoPhanThuVe).GetNumberDisplayVN() : CommonHelper.GetPercent((double)stockReject, (double)_SoCoPhanThuVe).GetNumberDisplayUS();
                                                }
                                                break;
                                            case "{SYS_OTHER}":
                                                {
                                                    itemparam.strValue = (doc.Doc_Lang == "vn") ? CommonHelper.GetDecimalFormart(stockOther, "vn") : CommonHelper.GetDecimalFormart(stockOther, "en");
                                                }
                                                break;
                                            case "{SYS_OTHER_PERCENT}":
                                                {
                                                    //itemparam.strValue = CommonHelper.GetPercent((double)stockOther, (double)TotalStockBaseOnTopic).GetNumberDisplayVN();
                                                    itemparam.strValue = (doc.Doc_Lang == "vn") ? CommonHelper.GetPercent((double)stockOther, (double)_SoCoPhanThuVe).GetNumberDisplayVN() : CommonHelper.GetPercent((double)stockOther, (double)_SoCoPhanThuVe).GetNumberDisplayUS();
                                                }
                                                break;
                                            case "{SYS_INVALID}":
                                                {
                                                    itemparam.strValue = (doc.Doc_Lang == "vn") ? CommonHelper.GetDecimalFormart(stockInvalid, "vn") : CommonHelper.GetDecimalFormart(stockInvalid, "en");
                                                }
                                                break;
                                            case "{SYS_INVALID_PERCENT}":
                                                {
                                                    itemparam.strValue = (doc.Doc_Lang == "vn") ? CommonHelper.GetPercent((double)stockInvalid, (double)_SoCoPhanThuVe).GetNumberDisplayVN() : CommonHelper.GetPercent((double)stockInvalid, (double)_SoCoPhanThuVe).GetNumberDisplayUS();
                                                }
                                                break;
                                            case "{SYS_TOTAL_STOCK_SPEND}":
                                                {
                                                    itemparam.strValue = (doc.Doc_Lang == "vn") ? CommonHelper.GetDecimalFormart(TotalStockBaseOnTopic, "vn") : CommonHelper.GetDecimalFormart(TotalStockBaseOnTopic, "en");
                                                }
                                                break;
                                            case "{SYS_SUM_STOCK_RESULT_PERCENT}":
                                                {
                                                    itemparam.strValue = (doc.Doc_Lang == "vn") ? CommonHelper.GetPercent((double)TotalStockBaseOnTopic, (double)_SoCoPhanThuVe).GetNumberDisplayVN() : CommonHelper.GetPercent((double)TotalStockBaseOnTopic, (double)_SoCoPhanThuVe).GetNumberDisplayUS();
                                                }
                                                break;
                                            case "{SYS_SUM_STOCK_AGREE_PER_TOTAL}":
                                                {
                                                    double _TotalStockAttend = DataAccess.SumStockAttend(doc.Event_ID);
                                                    //itemparam.strValue = (doc.Doc_Lang == "vn") ? CommonHelper.GetPercent((double)stockAgree, (double)_TotalStockAttend).GetNumberDisplayVN() : CommonHelper.GetPercent((double)stockAgree, (double)_TotalStockAttend).GetNumberDisplayUS();
                                                    itemparam.strValue = (doc.Doc_Lang == "vn") ? CommonHelper.GetPercent((double)stockAgree, (double)_SoCoPhanThuVe).GetNumberDisplayVN() : CommonHelper.GetPercent((double)stockAgree, (double)_SoCoPhanThuVe).GetNumberDisplayUS();
                                                }
                                                break;
                                        }
                                    }

                                    ResultHTML += TemplateEngine.FillTemplate(fillDataItem, itemTemplate.DocTemplate);
                                }


                                fillData.Add(new ParamValue()
                                {
                                    strParam = "{SYS_LOOP_TOPIC}",
                                    strValue = ResultHTML
                                });
                            }
                            break;

                        case "{SYS_LOCATION}":
                            {

                                fillData.Add(new ParamValue()
                                {
                                    strParam = "{SYS_LOCATION}",
                                    strValue = " - "+LocationAddress.ToUpper()
                                });
                            }
                            break;

                        case "{SYS_LOOP_TOPIC_CAU}":
                            {
                                string ResultHTML = "";
                                if (tbleventVote == null)
                                {
                                    tbleventVote = DataAccess.GetEventVotingCard(doc.Event_ID);
                                }
                                double _SoCoPhanThuVe = DataAccess.SYS_SUM_STOCK_RESULT(tbleventVote.EventID);
                                //double _SoCoPhanThuVe = DataAccess.SYS_SUM_STOCK_RESULT_BASE_ON_INVALID(tbleventVote.EventID, false);

                                tblEventDoc itemTemplate = (doc.Doc_Lang == "vn") ? DataAccess.GetEventDoc(30) : DataAccess.GetEventDoc(49);
                                List<ParamValue> fillDataItem = TemplateEngine.GetListDynamicParams(itemTemplate.DocTemplate);
                                //lay danh sach cau hoi
                                List<tblVotingCardTopic> lstTopic = DataAccess.GetListVotingCardTopicByEvent(doc.Event_ID, 1);
                                foreach (var TopicItem in lstTopic)
                                {
                                    foreach (ParamValue itemparam in fillDataItem)
                                    {
                                        //Tong so co phan rải vào từng cau hỏi, bao gom ca các cac ko hop le
                                        long TotalStockBaseOnTopic = DataAccess.TotalSumStockResultBaseOnTopicIDCau(doc.Event_ID, TopicItem.VotingTopicID, LocationID);
                                        long stockAgree = DataAccess.SumStockResultBaseOnTopicIDCau(doc.Event_ID, TopicItem.VotingTopicID, 0, LocationID);
                                        long stockReject = DataAccess.SumStockResultBaseOnTopicIDCau(doc.Event_ID, TopicItem.VotingTopicID, 1, LocationID);
                                        long stockOther = DataAccess.SumStockResultBaseOnTopicIDCau(doc.Event_ID, TopicItem.VotingTopicID, 2, LocationID);
                                        long stockInvalid = DataAccess.SumStockResultBaseOnTopicIDCau(doc.Event_ID, TopicItem.VotingTopicID, 3, LocationID);

                                        switch (itemparam.strParam)
                                        {
                                            case "{SYS_VOTE_NAME}":
                                                {
                                                    itemparam.strValue = (doc.Doc_Lang == "vn") ? TopicItem.TopicNameVN : TopicItem.TopicName_En;
                                                }
                                                break;
                                            case "{SYS_AGREE}":
                                                {
                                                    itemparam.strValue = (doc.Doc_Lang == "vn") ? CommonHelper.GetDecimalFormart(stockAgree, "vn") : CommonHelper.GetDecimalFormart(stockAgree, "en");
                                                }
                                                break;
                                            case "{SYS_AGREE_PERCENT}":
                                                {
                                                    //itemparam.strValue = CommonHelper.GetPercent((double)stockAgree,(double) TotalStockBaseOnTopic).GetNumberDisplayVN();
                                                    itemparam.strValue = (doc.Doc_Lang == "vn") ? CommonHelper.GetPercent((double)stockAgree, (double)_SoCoPhanThuVe).GetNumberDisplayVN() : CommonHelper.GetPercent((double)stockAgree, (double)_SoCoPhanThuVe).GetNumberDisplayUS();
                                                }
                                                break;
                                            case "{SYS_REJECT}":
                                                {
                                                    itemparam.strValue = (doc.Doc_Lang == "vn") ? CommonHelper.GetDecimalFormart(stockReject, "vn") : CommonHelper.GetDecimalFormart(stockReject, "en");
                                                }
                                                break;
                                            case "{SYS_REJECT_PERCENT}":
                                                {
                                                    //itemparam.strValue = CommonHelper.GetPercent((double)stockReject, (double)TotalStockBaseOnTopic).GetNumberDisplayVN();
                                                    itemparam.strValue = (doc.Doc_Lang == "vn") ? CommonHelper.GetPercent((double)stockReject, (double)_SoCoPhanThuVe).GetNumberDisplayVN() : CommonHelper.GetPercent((double)stockReject, (double)_SoCoPhanThuVe).GetNumberDisplayUS();
                                                }
                                                break;
                                            case "{SYS_OTHER}":
                                                {
                                                    itemparam.strValue = (doc.Doc_Lang == "vn") ? CommonHelper.GetDecimalFormart(stockOther, "vn") : CommonHelper.GetDecimalFormart(stockOther, "en");
                                                }
                                                break;
                                            case "{SYS_OTHER_PERCENT}":
                                                {
                                                    //itemparam.strValue = CommonHelper.GetPercent((double)stockOther, (double)TotalStockBaseOnTopic).GetNumberDisplayVN();
                                                    itemparam.strValue = (doc.Doc_Lang == "vn") ? CommonHelper.GetPercent((double)stockOther, (double)_SoCoPhanThuVe).GetNumberDisplayVN() : CommonHelper.GetPercent((double)stockOther, (double)_SoCoPhanThuVe).GetNumberDisplayUS();
                                                }
                                                break;
                                            case "{SYS_INVALID}":
                                                {
                                                    itemparam.strValue = (doc.Doc_Lang == "vn") ? CommonHelper.GetDecimalFormart(stockInvalid, "vn") : CommonHelper.GetDecimalFormart(stockInvalid, "en");
                                                }
                                                break;
                                            case "{SYS_INVALID_PERCENT}":
                                                {
                                                    itemparam.strValue = (doc.Doc_Lang == "vn") ? CommonHelper.GetPercent((double)stockInvalid, (double)_SoCoPhanThuVe).GetNumberDisplayVN() : CommonHelper.GetPercent((double)stockInvalid, (double)_SoCoPhanThuVe).GetNumberDisplayUS();
                                                }
                                                break;
                                            case "{SYS_TOTAL_STOCK_SPEND}":
                                                {
                                                    itemparam.strValue = (doc.Doc_Lang == "vn") ? CommonHelper.GetDecimalFormart(TotalStockBaseOnTopic, "vn") : CommonHelper.GetDecimalFormart(TotalStockBaseOnTopic, "en");
                                                }
                                                break;
                                            case "{SYS_SUM_STOCK_RESULT_PERCENT}":
                                                {
                                                    itemparam.strValue = (doc.Doc_Lang == "vn") ? CommonHelper.GetPercent((double)TotalStockBaseOnTopic, (double)_SoCoPhanThuVe).GetNumberDisplayVN() : CommonHelper.GetPercent((double)TotalStockBaseOnTopic, (double)_SoCoPhanThuVe).GetNumberDisplayUS();
                                                }
                                                break;
                                            case "{SYS_SUM_STOCK_AGREE_PER_TOTAL}":
                                                {
                                                    double _TotalStockAttend = DataAccess.SumStockAttend(doc.Event_ID);
                                                    //itemparam.strValue = (doc.Doc_Lang == "vn") ? CommonHelper.GetPercent((double)stockAgree, (double)_TotalStockAttend).GetNumberDisplayVN() : CommonHelper.GetPercent((double)stockAgree, (double)_TotalStockAttend).GetNumberDisplayUS();
                                                    itemparam.strValue = (doc.Doc_Lang == "vn") ? CommonHelper.GetPercent((double)stockAgree, (double)_SoCoPhanThuVe).GetNumberDisplayVN() : CommonHelper.GetPercent((double)stockAgree, (double)_SoCoPhanThuVe).GetNumberDisplayUS();
                                                }
                                                break;
                                        }
                                    }

                                    ResultHTML += TemplateEngine.FillTemplate(fillDataItem, itemTemplate.DocTemplate);
                                }


                                fillData.Add(new ParamValue()
                                {
                                    strParam = "{SYS_LOOP_TOPIC_CAU}",
                                    strValue = ResultHTML
                                });
                            }
                            break;
                        case "{SYS_LOOP_TOPIC_MINI}":
                            {
                                string ResultHTML = "";
                                if (tbleventVote == null)
                                {
                                    tbleventVote = DataAccess.GetEventVotingCard(doc.Event_ID);
                                }
                                double _SoCoPhanThuVe = DataAccess.SYS_SUM_STOCK_RESULT(tbleventVote.EventID);

                                tblEventDoc itemTemplate = (doc.Doc_Lang == "vn") ? DataAccess.GetEventDoc(3203) : DataAccess.GetEventDoc(3204);
                                List<ParamValue> fillDataItem = TemplateEngine.GetListDynamicParams(itemTemplate.DocTemplate);
                                //lay danh sach cau hoi
                                List<tblVotingCardTopic> lstTopic = DataAccess.GetListVotingCardTopicByEvent(doc.Event_ID, 1);

                                foreach (var TopicItem in lstTopic)
                                {
                                    foreach (ParamValue itemparam in fillDataItem)
                                    {
                                        long stockAgree = DataAccess.SumStockResultBaseOnTopicID(doc.Event_ID, TopicItem.VotingTopicID, 0);
                                        switch (itemparam.strParam)
                                        {
                                            case "{SYS_VOTE_NAME}":
                                                {
                                                    itemparam.strValue = (doc.Doc_Lang == "vn") ? TopicItem.TopicNameVN : TopicItem.TopicName_En;
                                                }
                                                break;
                                            case "{SYS_AGREE}":
                                                {
                                                    itemparam.strValue = (doc.Doc_Lang == "vn") ? CommonHelper.GetDecimalFormart(stockAgree, "vn") : CommonHelper.GetDecimalFormart(stockAgree, "en");
                                                }
                                                break;
                                            case "{SYS_SUM_STOCK_AGREE_PER_TOTAL}":
                                                {
                                                    double _TotalStockAttend = DataAccess.SumStockAttend(doc.Event_ID);
                                                    itemparam.strValue = (doc.Doc_Lang == "vn") ? CommonHelper.GetPercent((double)stockAgree, (double)_SoCoPhanThuVe).GetNumberDisplayVN() : CommonHelper.GetPercent((double)stockAgree, (double)_SoCoPhanThuVe).GetNumberDisplayUS();
                                                }
                                                break;
                                            case "{SYS_TOPIC_DESC}":
                                                {
                                                    itemparam.strValue = (doc.Doc_Lang == "vn") ? TopicItem.TopicContectVN : TopicItem.TopicContectEN;
                                                }
                                                break;
                                        }
                                    }
                                    ResultHTML += TemplateEngine.FillTemplate(fillDataItem, itemTemplate.DocTemplate);
                                }

                                fillData.Add(new ParamValue()
                                {
                                    strParam = "{SYS_LOOP_TOPIC_MINI}",
                                    strValue = ResultHTML
                                });
                            }
                            break;
                        // BAU HDQT TYPE CANDIDATE = 2
                        case "{SYS_HDQT_SO_THE_PHAT_RA}":
                            {
                                fillData.Add(new ParamValue()
                                {
                                    strParam = "{SYS_HDQT_SO_THE_PHAT_RA}",
                                    strValue = (doc.Doc_Lang == "vn") ? CommonHelper.GetDecimalFormart(DataAccess.SoTheBauCuPhatRa(objEvent.Event_ID, 2), "vn") : CommonHelper.GetDecimalFormart(DataAccess.SoTheBauCuPhatRa(objEvent.Event_ID,2), "en")
                                });
                            }
                            break;
                        case "{SYS_HDQT_SO_THE_THU_VE}":
                            {
                                fillData.Add(new ParamValue()
                                {
                                    strParam = "{SYS_HDQT_SO_THE_THU_VE}",
                                    strValue = (doc.Doc_Lang == "vn") ? CommonHelper.GetDecimalFormart(DataAccess.SoTheBauCuThuVe(objEvent.Event_ID, 2), "vn") : CommonHelper.GetDecimalFormart(DataAccess.SoTheBauCuThuVe(objEvent.Event_ID, 2), "en")
                                });
                            }
                            break;
                        case "{SYS_HDQT_SO_THE_HOP_LE}":
                            {
                                fillData.Add(new ParamValue()
                                {
                                    strParam = "{SYS_HDQT_SO_THE_HOP_LE}",
                                    strValue = (doc.Doc_Lang == "vn") ? CommonHelper.GetDecimalFormart(DataAccess.SoTheBauCuThuVeHopLe(objEvent.Event_ID, 2, false), "vn") : CommonHelper.GetDecimalFormart(DataAccess.SoTheBauCuThuVeHopLe(objEvent.Event_ID, 2, false), "en")
                                });
                            }
                            break;
                        case "{SYS_HDQT_SO_THE_KO_HOP_LE}":
                            {
                                fillData.Add(new ParamValue()
                                {
                                    strParam = "{SYS_HDQT_SO_THE_KO_HOP_LE}",
                                    strValue = (doc.Doc_Lang == "vn") ? CommonHelper.GetDecimalFormart(DataAccess.SoTheBauCuThuVeHopLe(objEvent.Event_ID, 2, true), "vn") : CommonHelper.GetDecimalFormart(DataAccess.SoTheBauCuThuVeHopLe(objEvent.Event_ID, 2, true), "en")
                                });
                            }
                            break;
                        case "{SYS_HDQT_LOOP_CANDIDATE}":
                            {
                                string ResultHTML = "";
                                tblEventDoc itemTemplate = (doc.Doc_Lang == "vn") ? DataAccess.GetEventDoc(3211) : DataAccess.GetEventDoc(3212);

                                //double TongSoPhieuBauPhatRa = DataAccess.TongSoPhieuBauPhatRa(objEvent.Event_ID, 2);

                                List<ParamValue> fillDataItem = TemplateEngine.GetListDynamicParams(itemTemplate.DocTemplate);
                                //danh sách ưng cu vien
                                List<tblCandidate> lstCandidate = DataAccess.GetListCandidateByEventandType(objEvent.Event_ID, 2);
                                foreach (var candidate in lstCandidate)
                                {
                                    foreach (ParamValue itemparam in fillDataItem)
                                    {
                                        long sophieubau = DataAccess.SoPhieuBau(objEvent.Event_ID, 2, candidate.CandidateID);

                                        switch (itemparam.strParam)
                                        {
                                            case "{SYS_DS_CANDIDATE_ORDER}":
                                                {
                                                    itemparam.strValue = candidate.GetOrder.ToString();
                                                }
                                                break;
                                            case "{SYS_DS_CANDIDATE_NAME}":
                                                {
                                                    itemparam.strValue = candidate.FullName;
                                                }
                                                break;
                                            case "{SYS_HDQT_CANDIDATE_GET_VOTE}":
                                                {
                                                    itemparam.strValue = (doc.Doc_Lang == "vn") ? CommonHelper.GetDecimalFormart(sophieubau, "vn") : CommonHelper.GetDecimalFormart(sophieubau, "en");
                                                }
                                                break;
                                            case "{SYS_HSQT_ELECTION_PERCENT}":
                                                {
                                                    double sopheubauphatra = DataAccess.TongSoPhieuBauPhatRa(doc.Event_ID,2) / lstCandidate.Count;
                                                    itemparam.strValue = (doc.Doc_Lang == "vn") ? CommonHelper.GetPercent((double)sophieubau, (double)sopheubauphatra).GetNumberDisplayVN() : CommonHelper.GetPercent((double)sophieubau, (double)sopheubauphatra).GetNumberDisplayUS();
                                                }
                                                break;
                                        }
                                    }
                                    ResultHTML += TemplateEngine.FillTemplate(fillDataItem, itemTemplate.DocTemplate);
                                }

                                fillData.Add(new ParamValue()
                                {
                                    strParam = "{SYS_HDQT_LOOP_CANDIDATE}",
                                    strValue = ResultHTML
                                });
                            }
                            break;


                        // Báo cáo HDQT tại điểm cầu
                        case "{SYS_HDQT_CAU_SO_THE_PHAT_RA}":
                            {
                                fillData.Add(new ParamValue()
                                {
                                    strParam = "{SYS_HDQT_CAU_SO_THE_PHAT_RA}",
                                    strValue = (doc.Doc_Lang == "vn") ? CommonHelper.GetDecimalFormart(DataAccess.SoTheBauCuCauPhatRa(objEvent.Event_ID, 2, LocationID), "vn") : CommonHelper.GetDecimalFormart(DataAccess.SoTheBauCuCauPhatRa(objEvent.Event_ID, 2, LocationID), "en")
                                });
                            }
                            break;
                        case "{SYS_HDQT_CAU_SO_THE_THU_VE}":
                            {
                                fillData.Add(new ParamValue()
                                {
                                    strParam = "{SYS_HDQT_CAU_SO_THE_THU_VE}",
                                    strValue = (doc.Doc_Lang == "vn") ? CommonHelper.GetDecimalFormart(DataAccess.SoTheBauCuCauThuVe(objEvent.Event_ID, 2, LocationID), "vn") : CommonHelper.GetDecimalFormart(DataAccess.SoTheBauCuCauThuVe(objEvent.Event_ID, 2, LocationID), "en")
                                });
                            }
                            break;
                        case "{SYS_HDQT_CAU_SO_THE_HOP_LE}":
                            {
                                fillData.Add(new ParamValue()
                                {
                                    strParam = "{SYS_HDQT_CAU_SO_THE_HOP_LE}",
                                    strValue = (doc.Doc_Lang == "vn") ? CommonHelper.GetDecimalFormart(DataAccess.SoTheBauCuCauThuVeHopLe(objEvent.Event_ID, 2, false, LocationID), "vn") : CommonHelper.GetDecimalFormart(DataAccess.SoTheBauCuCauThuVeHopLe(objEvent.Event_ID, 2, false, LocationID), "en")
                                });
                            }
                            break;
                        case "{SYS_HDQT_CAU_SO_THE_KO_HOP_LE}":
                            {
                                fillData.Add(new ParamValue()
                                {
                                    strParam = "{SYS_HDQT_CAU_SO_THE_KO_HOP_LE}",
                                    strValue = (doc.Doc_Lang == "vn") ? CommonHelper.GetDecimalFormart(DataAccess.SoTheBauCuCauThuVeHopLe(objEvent.Event_ID, 2, true, LocationID), "vn") : CommonHelper.GetDecimalFormart(DataAccess.SoTheBauCuCauThuVeHopLe(objEvent.Event_ID, 2, true, LocationID), "en")
                                });
                            }
                            break;
                        case "{SYS_HDQT_CAU_LOOP_CANDIDATE}":
                            {
                                string ResultHTML = "";
                                tblEventDoc itemTemplate = (doc.Doc_Lang == "vn") ? DataAccess.GetEventDoc(3211) : DataAccess.GetEventDoc(3212);

                                //double TongSoPhieuBauPhatRa = DataAccess.TongSoPhieuBauPhatRa(objEvent.Event_ID, 2);

                                List<ParamValue> fillDataItem = TemplateEngine.GetListDynamicParams(itemTemplate.DocTemplate);
                                //danh sách ưng cu vien
                                List<tblCandidate> lstCandidate = DataAccess.GetListCandidateByEventandType(objEvent.Event_ID, 2);
                                foreach (var candidate in lstCandidate)
                                {
                                    foreach (ParamValue itemparam in fillDataItem)
                                    {
                                        long sophieubau = DataAccess.SoPhieuBauCau(objEvent.Event_ID, 2, candidate.CandidateID, LocationID);

                                        switch (itemparam.strParam)
                                        {
                                            case "{SYS_DS_CANDIDATE_ORDER}":
                                                {
                                                    itemparam.strValue = candidate.GetOrder.ToString();
                                                }
                                                break;
                                            case "{SYS_DS_CANDIDATE_NAME}":
                                                {
                                                    itemparam.strValue = candidate.FullName;
                                                }
                                                break;
                                            case "{SYS_HDQT_CANDIDATE_GET_VOTE}":
                                                {
                                                    itemparam.strValue = (doc.Doc_Lang == "vn") ? CommonHelper.GetDecimalFormart(sophieubau, "vn") : CommonHelper.GetDecimalFormart(sophieubau, "en");
                                                }
                                                break;
                                            case "{SYS_HSQT_ELECTION_PERCENT}":
                                                {
                                                    double sopheubauphatra = DataAccess.TongSoPhieuBauCauPhatRa(doc.Event_ID, 2, LocationID) / lstCandidate.Count;
                                                    itemparam.strValue = (doc.Doc_Lang == "vn") ? CommonHelper.GetPercent((double)sophieubau, (double)sopheubauphatra).GetNumberDisplayVN() : CommonHelper.GetPercent((double)sophieubau, (double)sopheubauphatra).GetNumberDisplayUS();
                                                }
                                                break;
                                        }
                                    }
                                    ResultHTML += TemplateEngine.FillTemplate(fillDataItem, itemTemplate.DocTemplate);
                                }

                                fillData.Add(new ParamValue()
                                {
                                    strParam = "{SYS_HDQT_CAU_LOOP_CANDIDATE}",
                                    strValue = ResultHTML
                                });
                            }
                            break;

                        // BAU BKS TYPE CANDIDATE: 1
                        case "{SYS_BKS_SO_THE_PHAT_RA}":
                            {
                                fillData.Add(new ParamValue()
                                {
                                    strParam = "{SYS_BKS_SO_THE_PHAT_RA}",
                                    strValue = (doc.Doc_Lang == "vn") ? CommonHelper.GetDecimalFormart(DataAccess.SoTheBauCuPhatRa(objEvent.Event_ID, 1), "vn") : CommonHelper.GetDecimalFormart(DataAccess.SoTheBauCuPhatRa(objEvent.Event_ID, 1), "en")
                                });
                            }
                            break;
                        case "{SYS_BKS_SO_THE_THU_VE}":
                            {
                                fillData.Add(new ParamValue()
                                {
                                    strParam = "{SYS_BKS_SO_THE_THU_VE}",
                                    strValue = (doc.Doc_Lang == "vn") ? CommonHelper.GetDecimalFormart(DataAccess.SoTheBauCuThuVe(objEvent.Event_ID, 1), "vn") : CommonHelper.GetDecimalFormart(DataAccess.SoTheBauCuThuVe(objEvent.Event_ID, 1), "en")
                                });
                            }
                            break;
                        case "{SYS_BKS_SO_THE_HOP_LE}":
                            {
                                fillData.Add(new ParamValue()
                                {
                                    strParam = "{SYS_BKS_SO_THE_HOP_LE}",
                                    strValue = (doc.Doc_Lang == "vn") ? CommonHelper.GetDecimalFormart(DataAccess.SoTheBauCuThuVeHopLe(objEvent.Event_ID, 1, false), "vn") : CommonHelper.GetDecimalFormart(DataAccess.SoTheBauCuThuVeHopLe(objEvent.Event_ID, 1, false), "en")
                                });
                            }
                            break;
                        case "{SYS_BKS_SO_THE_KO_HOP_LE}":
                            {
                                fillData.Add(new ParamValue()
                                {
                                    strParam = "{SYS_BKS_SO_THE_KO_HOP_LE}",
                                    strValue = (doc.Doc_Lang == "vn") ? CommonHelper.GetDecimalFormart(DataAccess.SoTheBauCuThuVeHopLe(objEvent.Event_ID, 1, true), "vn") : CommonHelper.GetDecimalFormart(DataAccess.SoTheBauCuThuVeHopLe(objEvent.Event_ID, 1, true), "en")
                                });
                            }
                            break;

                        case "{SYS_BKS_LOOP_CANDIDATE}":
                            {
                                string ResultHTML = "";
                                tblEventDoc itemTemplate = (doc.Doc_Lang == "vn") ? DataAccess.GetEventDoc(3211) : DataAccess.GetEventDoc(3212);

                                //double TongSoPhieuBauPhatRa = DataAccess.TongSoPhieuBauPhatRa(objEvent.Event_ID, 2);

                                List<ParamValue> fillDataItem = TemplateEngine.GetListDynamicParams(itemTemplate.DocTemplate);
                                //danh sách ưng cu vien
                                List<tblCandidate> lstCandidate = DataAccess.GetListCandidateByEventandType(objEvent.Event_ID, 1);
                                foreach (var candidate in lstCandidate)
                                {
                                    foreach (ParamValue itemparam in fillDataItem)
                                    {
                                        long sophieubau = DataAccess.SoPhieuBau(objEvent.Event_ID, 1, candidate.CandidateID);

                                        switch (itemparam.strParam)
                                        {
                                            case "{SYS_DS_CANDIDATE_ORDER}":
                                                {
                                                    itemparam.strValue = candidate.GetOrder.ToString();
                                                }
                                                break;
                                            case "{SYS_DS_CANDIDATE_NAME}":
                                                {
                                                    itemparam.strValue = candidate.FullName;
                                                }
                                                break;
                                            case "{SYS_HDQT_CANDIDATE_GET_VOTE}":
                                                {
                                                    itemparam.strValue = (doc.Doc_Lang == "vn") ? CommonHelper.GetDecimalFormart(sophieubau, "vn") : CommonHelper.GetDecimalFormart(sophieubau, "en");
                                                }
                                                break;
                                            case "{SYS_HSQT_ELECTION_PERCENT}":
                                                {
                                                    double sopheubauphatra = DataAccess.TongSoPhieuBauPhatRa(doc.Event_ID, 1) / lstCandidate.Count;
                                                    itemparam.strValue = (doc.Doc_Lang == "vn") ? CommonHelper.GetPercent((double)sophieubau, (double)sopheubauphatra).GetNumberDisplayVN() : CommonHelper.GetPercent((double)sophieubau, (double)sopheubauphatra).GetNumberDisplayUS();
                                                }
                                                break;
                                        }
                                    }
                                    ResultHTML += TemplateEngine.FillTemplate(fillDataItem, itemTemplate.DocTemplate);
                                }

                                fillData.Add(new ParamValue()
                                {
                                    strParam = "{SYS_BKS_LOOP_CANDIDATE}",
                                    strValue = ResultHTML
                                });
                            }
                            break;

                        // Báo cáo BKS tại điểm cầu
                        case "{SYS_BKS_CAU_SO_THE_PHAT_RA}":
                            {
                                fillData.Add(new ParamValue()
                                {
                                    strParam = "{SYS_BKS_CAU_SO_THE_PHAT_RA}",
                                    strValue = (doc.Doc_Lang == "vn") ? CommonHelper.GetDecimalFormart(DataAccess.SoTheBauCuCauPhatRa(objEvent.Event_ID, 1, LocationID), "vn") : CommonHelper.GetDecimalFormart(DataAccess.SoTheBauCuCauPhatRa(objEvent.Event_ID, 1, LocationID), "en")
                                });
                            }
                            break;
                        case "{SYS_BKS_CAU_SO_THE_THU_VE}":
                            {
                                fillData.Add(new ParamValue()
                                {
                                    strParam = "{SYS_BKS_CAU_SO_THE_THU_VE}",
                                    strValue = (doc.Doc_Lang == "vn") ? CommonHelper.GetDecimalFormart(DataAccess.SoTheBauCuCauThuVe(objEvent.Event_ID, 1, LocationID), "vn") : CommonHelper.GetDecimalFormart(DataAccess.SoTheBauCuCauThuVe(objEvent.Event_ID, 1, LocationID), "en")
                                });
                            }
                            break;
                        case "{SYS_BKS_CAU_SO_THE_HOP_LE}":
                            {
                                fillData.Add(new ParamValue()
                                {
                                    strParam = "{SYS_BKS_CAU_SO_THE_HOP_LE}",
                                    strValue = (doc.Doc_Lang == "vn") ? CommonHelper.GetDecimalFormart(DataAccess.SoTheBauCuCauThuVeHopLe(objEvent.Event_ID, 1, false, LocationID), "vn") : CommonHelper.GetDecimalFormart(DataAccess.SoTheBauCuCauThuVeHopLe(objEvent.Event_ID, 1, false, LocationID), "en")
                                });
                            }
                            break;
                        case "{SYS_BKS_CAU_SO_THE_KO_HOP_LE}":
                            {
                                fillData.Add(new ParamValue()
                                {
                                    strParam = "{SYS_BKS_CAU_SO_THE_KO_HOP_LE}",
                                    strValue = (doc.Doc_Lang == "vn") ? CommonHelper.GetDecimalFormart(DataAccess.SoTheBauCuCauThuVeHopLe(objEvent.Event_ID, 1, true, LocationID), "vn") : CommonHelper.GetDecimalFormart(DataAccess.SoTheBauCuCauThuVeHopLe(objEvent.Event_ID, 1, true, LocationID), "en")
                                });
                            }
                            break;
                        case "{SYS_BKS_CAU_LOOP_CANDIDATE}":
                            {
                                string ResultHTML = "";
                                tblEventDoc itemTemplate = (doc.Doc_Lang == "vn") ? DataAccess.GetEventDoc(3211) : DataAccess.GetEventDoc(3212);

                                //double TongSoPhieuBauPhatRa = DataAccess.TongSoPhieuBauPhatRa(objEvent.Event_ID, 2);

                                List<ParamValue> fillDataItem = TemplateEngine.GetListDynamicParams(itemTemplate.DocTemplate);
                                //danh sách ưng cu vien
                                List<tblCandidate> lstCandidate = DataAccess.GetListCandidateByEventandType(objEvent.Event_ID, 1);
                                foreach (var candidate in lstCandidate)
                                {
                                    foreach (ParamValue itemparam in fillDataItem)
                                    {
                                        long sophieubauCau = DataAccess.SoPhieuBauCau(objEvent.Event_ID, 1, candidate.CandidateID, LocationID);

                                        switch (itemparam.strParam)
                                        {
                                            case "{SYS_DS_CANDIDATE_ORDER}":
                                                {
                                                    itemparam.strValue = candidate.GetOrder.ToString();
                                                }
                                                break;
                                            case "{SYS_DS_CANDIDATE_NAME}":
                                                {
                                                    itemparam.strValue = candidate.FullName;
                                                }
                                                break;
                                            case "{SYS_HDQT_CANDIDATE_GET_VOTE}":
                                                {
                                                    itemparam.strValue = (doc.Doc_Lang == "vn") ? CommonHelper.GetDecimalFormart(sophieubauCau, "vn") : CommonHelper.GetDecimalFormart(sophieubauCau, "en");
                                                }
                                                break;
                                            case "{SYS_HSQT_ELECTION_PERCENT}":
                                                {
                                                    double sophieubaucauphatra = DataAccess.TongSoPhieuBauCauPhatRa(doc.Event_ID, 1, LocationID) / lstCandidate.Count;
                                                    itemparam.strValue = (doc.Doc_Lang == "vn") ? CommonHelper.GetPercent((double)sophieubauCau, (double)sophieubaucauphatra).GetNumberDisplayVN() : CommonHelper.GetPercent((double)sophieubauCau, (double)sophieubaucauphatra).GetNumberDisplayUS();
                                                }
                                                break;
                                        }
                                    }
                                    ResultHTML += TemplateEngine.FillTemplate(fillDataItem, itemTemplate.DocTemplate);
                                }

                                fillData.Add(new ParamValue()
                                {
                                    strParam = "{SYS_BKS_CAU_LOOP_CANDIDATE}",
                                    strValue = ResultHTML
                                });
                            }
                            break;
                            // End báo cáo BKS tại điểm cầu
                    }
                }
                else
                {
                    fillData.Add(new ParamValue()
                    {
                        strParam = param.Param_name,
                        strValue = param.Param_Value
                    });
                }
            }

            string _ressult = TemplateEngine.FillTemplate(fillData, doc.DocTemplate);
            return _ressult;
        }
    }
}
