using System;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;

namespace DataService
{
    public partial class DataAccess
    {
        /// <summary>
        /// Checkin khach moi den su kien
        /// </summary>
        /// <param name="eID"></param>
        /// <param name="uID"></param>
        /// <param name="status"></param>
        /// <returns>1: checkin thanh cong</returns>
        /// <returns>2: khach de checkin truoc day, update lai</returns>
        /// <returns>0: loi</returns>
        [Obsolete]
        public static int StockHolderAttend(int eID, long uID, byte status, int _locationID)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    try
                    {
                        tblStockholderAttend objAttend = null;
                        //Kiểm tra Customer đã checkin hay chưa
                        objAttend = StockholderIsCheckin(eID, uID);
                        if (objAttend == null)
                        {
                            objAttend = new tblStockholderAttend()
                            {
                                Event_ID = eID,
                                SHID = uID,
                                Status = status,
                                AttendDate = DateTime.Now,
                                LocationID = _locationID,
                            };

                            db.Entry(objAttend).State = EntityState.Added;
                            return db.SaveChanges();
                        }
                        else
                        {
                            objAttend.Status = status;
                            db.Entry(objAttend).State = EntityState.Modified;
                            return db.SaveChanges();
                        }
                    }
                    catch (Exception ex)
                    {
                        return 0;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static tblStockholderAttend StockholderIsCheckin(int eID, long uID)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    var attend = db.tblStockholderAttends.Where(o => o.Event_ID == eID && o.SHID == uID).FirstOrDefault();
                    return attend;
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }


        public static int CountTotalInviteAttend(int _eID)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    int count = db.tblStockholderAttends.Count(o => o.Event_ID == _eID);

                    return count;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int CountTotalInviteAttend(int _eID, byte _status)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    //var todaysDate = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day);
                    //int count = db.tblStockholderAttends.Count(o => o.Event_ID == _eID && o.Status == _status && EntityFunctions.TruncateTime(o.AttendDate) == EntityFunctions.TruncateTime(todaysDate.Date));
                    int count = db.tblStockholderAttends.Count(o => o.Event_ID == _eID && o.Status == _status);

                    return count;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int CountTotalInviteAttendCau(int _eID, byte _status, int LocationID)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    //var todaysDate = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day);
                    //int count = db.tblStockholderAttends.Count(o => o.Event_ID == _eID && o.Status == _status && EntityFunctions.TruncateTime(o.AttendDate) == EntityFunctions.TruncateTime(todaysDate.Date));
                    int count = db.tblStockholderAttends.Count(o => o.Event_ID == _eID && o.Status == _status && o.LocationID == LocationID);

                    return count;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
