﻿using DataService.DomainModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Validation;
using System.Linq;
using VG.Common;

namespace DataService
{
    public partial class DataAccess
    {
        public static List<tblVotingCardTopic> GetListVotingCardTopic(int votingCard, byte? status)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    if (status.HasValue)
                    {
                        return db.tblVotingCardTopics.Where(o => o.VotingCardID == votingCard && o.Status == status).OrderBy(o => o.TopicOrder).ToList();
                    }
                    else
                    {
                        return db.tblVotingCardTopics.Where(o => o.VotingCardID == votingCard).OrderBy(o => o.TopicOrder).ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static List<tblVotingCardTopic> GetListVotingCardTopicByEvent(int eventID, byte? status)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    if (status.HasValue)
                    {
                        return db.tblVotingCardTopics.Where(o => o.EventID == eventID && o.Status == status).OrderBy(o => o.TopicOrder).ToList();
                    }
                    else
                    {
                        return db.tblVotingCardTopics.Where(o => o.EventID == eventID).OrderBy(o => o.TopicOrder).ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int CountTopicByEvent(int eventID)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    return db.tblVotingCardTopics.Count(o => o.EventID == eventID && o.Status == 1);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int CountResultBaseOnTopicID(int eventID, int topicID, int result)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    return db.tblVoteResults.Count(o => o.EventID == eventID && o.VoteTopicId == topicID && o.Result == result);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static long SumStockResultBaseOnTopicID(int eventID, int topicID, int result)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    var lst = db.tblVoteResults.Where(o => o.EventID == eventID && o.VoteTopicId == topicID && o.Result == result);
                    long SumReturn = 0;
                    foreach(var item in lst)
                    {
                        SumReturn += item.Quantity;
                    }

                    return SumReturn;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static long SumStockResultBaseOnTopicIDCau(int eventID, int topicID, int result, int Location)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    var lst = db.tblVoteResults.Where(o => o.EventID == eventID && o.VoteTopicId == topicID && o.Result == result && o.LocationID == Location);
                    long SumReturn = 0;
                    foreach (var item in lst)
                    {
                        SumReturn += item.Quantity;
                    }

                    return SumReturn;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static long TotalSumStockResultBaseOnTopicID(int eventID, int topicID)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    var lst = db.tblVoteResults.Where(o => o.EventID == eventID && o.VoteTopicId == topicID);
                    long SumReturn = 0;
                    foreach (var item in lst)
                    {
                        SumReturn += item.Quantity;
                    }
                    return SumReturn;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static long TotalSumStockResultBaseOnTopicIDCau(int eventID, int topicID, int LocationID)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    var lst = db.tblVoteResults.Where(o => o.EventID == eventID && o.VoteTopicId == topicID && o.LocationID == LocationID);
                    long SumReturn = 0;
                    foreach (var item in lst)
                    {
                        SumReturn += item.Quantity;
                    }
                    return SumReturn;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
