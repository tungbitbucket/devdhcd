﻿using DataService.DomainModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Validation;
using System.Linq;
using System.Transactions;
using VG.Common;

namespace DataService
{
    public partial class DataAccess
    {
        public static int InsertBatchImport(tblSharreholdImportBatch objBatch)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    try
                    {
                        db.Configuration.AutoDetectChangesEnabled = false;
                        db.Configuration.ValidateOnSaveEnabled = false;
                        db.Entry(objBatch).State = EntityState.Added;
                        return db.SaveChanges();
                    }
                    finally
                    {
                        db.Configuration.AutoDetectChangesEnabled = true;
                        db.Configuration.ValidateOnSaveEnabled = false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int UpdateBatchImport(tblSharreholdImportBatch objBatch)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    try
                    {
                        db.Configuration.AutoDetectChangesEnabled = false;
                        db.Configuration.ValidateOnSaveEnabled = false;

                        db.Entry(objBatch).State = EntityState.Modified;
                        return db.SaveChanges();
                    }
                    finally
                    {
                        db.Configuration.AutoDetectChangesEnabled = true;
                        db.Configuration.ValidateOnSaveEnabled = false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<tblMasterPL> ListPL()
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    return db.tblMasterPLs.ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static List<tblMasterPL> ListPL(int _status)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    return db.tblMasterPLs.Where(o => o.PL_Status == _status).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int AddMasterPL(tblMasterPL objPL)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    try
                    {
                        db.Configuration.AutoDetectChangesEnabled = false;
                        db.Configuration.ValidateOnSaveEnabled = false;

                        db.Entry(objPL).State = EntityState.Added;
                        db.SaveChanges();
                        return objPL.PL_ID;
                    }
                    finally
                    {
                        db.Configuration.AutoDetectChangesEnabled = true;
                        db.Configuration.ValidateOnSaveEnabled = false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int UpdateMasterPL(tblMasterPL objPL)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    try
                    {
                        db.Configuration.AutoDetectChangesEnabled = false;
                        db.Configuration.ValidateOnSaveEnabled = false;

                        db.Entry(objPL).State = EntityState.Modified;
                        return db.SaveChanges();
                    }
                    finally
                    {
                        db.Configuration.AutoDetectChangesEnabled = true;
                        db.Configuration.ValidateOnSaveEnabled = false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static tblMasterPL GetPLByID(int _id)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    return db.tblMasterPLs.Find(_id);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<tblSharreholdImportBatch> ListBatchByEvent(int _id)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    return db.tblSharreholdImportBatches.Where(o => o.Event_ID == _id).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static List<tblDocument> GetListDefaultDoc(int? status)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    if (status.HasValue)
                    {
                        return db.tblDocuments.Where(o => o.Doc_Status == status.Value).ToList();
                    }
                    else
                    {
                        return db.tblDocuments.ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<ParamValue> GetListSysParams(bool? status)
        {
            try
            {
                List<ParamValue> listReturn = new List<ParamValue>();
                List<tblSystemParam> listResult = new List<tblSystemParam>();

                using (var db = new VIN_DHCDEntities())
                {
                    if (status.HasValue)
                    {
                        listResult = db.tblSystemParams.Where(o => o.ParamStatus == status.Value).ToList();
                    }
                    else
                    {
                        listResult = db.tblSystemParams.ToList();
                    }
                }
                listResult.ForEach(s => listReturn.Add(new ParamValue() { strParam = s.ParamName }));
                return listReturn;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void CopyDocToEvent(int eID)
        {
            try
            {
                List<tblDocument> lisDefault = GetListDefaultDoc(1);
                List<tblEventDoc> eventDocs = new List<tblEventDoc>();
                foreach (var item in lisDefault)
                {
                    eventDocs.Add(new tblEventDoc()
                    {
                        Event_ID = eID,
                        DocTemplate = item.Doc_Desc,
                        DocName = item.Doc_Name,
                        Doc_ID_Master = item.ID_Doc,
                        Doc_Type = item.Doc_Type,
                        Doc_Lang = item.Doc_Lang
                    });
                }
                using (var db = new VIN_DHCDEntities())
                {
                    db.tblEventDocs.AddRange(eventDocs);
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void InsertEventDocParams(List<tblEventDocDynParam> listParams)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    try
                    {
                        db.Configuration.AutoDetectChangesEnabled = false;
                        db.Configuration.ValidateOnSaveEnabled = false;

                        db.tblEventDocDynParams.AddRange(listParams);
                        db.SaveChanges();
                    }
                    finally
                    {
                        db.Configuration.AutoDetectChangesEnabled = true;
                        db.Configuration.ValidateOnSaveEnabled = true;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static void GenBarcodeVote(List<tblBarcodeVote> lisrBarcode, long ShID, int eventID)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    try
                    {
                        db.Configuration.AutoDetectChangesEnabled = false;
                        db.Configuration.ValidateOnSaveEnabled = false;


                        db.tblBarcodeVotes.RemoveRange(db.tblBarcodeVotes.Where(x => x.ShID == ShID && x.EventID == eventID));
                        db.SaveChanges();

                        db.tblBarcodeVotes.AddRange(lisrBarcode);
                        db.SaveChanges();
                    }
                    finally
                    {
                        db.Configuration.AutoDetectChangesEnabled = true;
                        db.Configuration.ValidateOnSaveEnabled = true;
                    }

                }
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        System.Console.WriteLine("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                    }
                }
                throw dbEx;
            }
        }

        public static List<tblBarcodeVote> GetBarcodeForVotingcard(long ShID, int eventID)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    return db.tblBarcodeVotes.Where(o => o.ShID == ShID && o.EventID == eventID).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static tblBarcodeVote GetByBarcode(long barcode)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    return db.tblBarcodeVotes.Find(barcode);
                }
            }
            catch
            {
                return null;
            }
        }

        public static void InsertVoteResult(tblVoteResult vote)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    try
                    {
                        //db.Configuration.AutoDetectChangesEnabled = false;
                        //db.Configuration.ValidateOnSaveEnabled = false;

                        try
                        {
                            var VoteResult = db.tblVoteResults.Where(o => o.ShID == vote.ShID && o.VoteTopicId == vote.VoteTopicId).FirstOrDefault();
                            VoteResult.Result = vote.Result;
                            VoteResult.BarcodeID = vote.BarcodeID;
                            db.Entry(VoteResult).State = EntityState.Modified;
                        }
                        catch
                        {
                            vote.Quantity = FindStockHolder(vote.ShID).Quantity.Value;
                            db.Entry(vote).State = EntityState.Added;
                        }
                        db.SaveChanges();
                    }
                    finally
                    {
                        //db.Configuration.AutoDetectChangesEnabled = true;
                        //db.Configuration.ValidateOnSaveEnabled = false;
                    }
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public static List<string> GetLstVotedBarcode(long ShID, int EventID)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    List<string> result = new List<string>();
                    var VoteResult = db.tblVoteResults.Where(o => o.ShID == ShID && o.EventID == EventID).ToList();
                    foreach (var cusResult in VoteResult)
                    {
                        result.Add(cusResult.BarcodeID.ToString());
                    }

                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<tblVoteResult> GetLstVotedBarcodeObj(long ShID, int EventID)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    return db.tblVoteResults.Where(o => o.ShID == ShID && o.EventID == EventID).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void RemoveFalseCheck(long ShID, int EventID)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    var listFalse = db.tblVoteResults.Where(o => o.ShID == ShID && o.EventID == EventID).ToList();
                    db.tblVoteResults.RemoveRange(listFalse);
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }



        //public static int InsertMarkPrinted(tblVotePrinted printed)
        //{
        //    using (var db = new VIN_DHCDEntities())
        //    {
        //        int count = db.tblVotePrinteds.Count(o => o.ShID == printed.ShID && o.EventID == printed.EventID);
        //        if (count == 0)
        //        {
        //            db.Entry(printed).State = EntityState.Added;
        //            return db.SaveChanges();
        //        }
        //        else
        //        {
        //            return 0;
        //        }
        //    }
        //}

        public static int InsertMarkPrinted(tblVotePrinted printed)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    int _returnvalue = 0;
                    using (var transaction = db.Database.BeginTransaction())
                    {
                        var VotePrinted = db.tblVotePrinteds.Where(o => o.ShID == printed.ShID && o.EventID == printed.EventID).FirstOrDefault();
                        
                        if (VotePrinted == null)
                        {
                            printed.Count = 1;
                            db.Entry(printed).State = EntityState.Added;
                            _returnvalue = db.SaveChanges();
                        }
                        else
                        {
                            VotePrinted.Count = VotePrinted.Count + 1; // In phiếu cho 1 người lần tiếp theo.
                            db.Entry(VotePrinted).State = EntityState.Modified;
                            _returnvalue = db.SaveChanges();
                        }
                        transaction.Commit();
                    }
                    return _returnvalue;
                }
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        System.Console.WriteLine("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                    }
                }
                throw dbEx;
            }
        }


        public static int InserElectionPrinted(tblCandidateVotePrinted printed)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    int _returnvalue = 0;
                    using (var transaction = db.Database.BeginTransaction())
                    {
                        int count = db.tblCandidateVotePrinteds.Count(o => o.ShID == printed.ShID && o.EventID == printed.EventID && o.CandidateType == printed.CandidateType);
                        if (count == 0)
                        {
                            db.Entry(printed).State = EntityState.Added;
                            _returnvalue = db.SaveChanges();
                        }
                        transaction.Commit();
                    }
                    return _returnvalue;
                }
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        System.Console.WriteLine("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                    }
                }
                throw dbEx;
            }
        }


        public static void InsertInvalid(tblInvalidVote invalidVote)
        {
            using (var db = new VIN_DHCDEntities())
            {
                using (var transaction = db.Database.BeginTransaction())
                {
                    int count = db.tblInvalidVotes.Count(o => o.ShID == invalidVote.ShID && o.EventID == invalidVote.EventID);
                    if (count == 0)
                    {
                        db.Entry(invalidVote).State = EntityState.Added;
                        db.SaveChanges();
                    }
                    transaction.Commit();
                }   
            }
        }

        public static void UpdateEventDocParams(List<tblEventDocDynParam> listParams)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    int _eventDocID = listParams[0].EventDoc_ID;

                    //Lấy lại danh sách params cu
                    List<tblEventDocDynParam> lstOldParams = GetDocEventParams(_eventDocID);
                    foreach (tblEventDocDynParam oldParam in lstOldParams)
                    {
                        foreach (tblEventDocDynParam newParam in listParams)
                        {
                            if (newParam.Param_name == oldParam.Param_name)
                            {
                                newParam.Param_Value = oldParam.Param_Value;
                            }
                        }
                    }

                    ClearEventDocParams(_eventDocID);

                    db.tblEventDocDynParams.AddRange(listParams);
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void ClearEventDocParams(int _eventDocID)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    db.tblEventDocDynParams.RemoveRange(db.tblEventDocDynParams.Where(x => x.EventDoc_ID == _eventDocID));
                    db.SaveChanges();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        public static int UpdateEventParamValue(int _paramID, string _paramValue)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    tblEventDocDynParam param = db.tblEventDocDynParams.Find(_paramID);
                    param.Param_Value = _paramValue;
                    db.Entry(param).State = EntityState.Modified;
                    return db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static List<tblEventDocDynParam> GetDocEventParams(int eventDoc)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    return db.tblEventDocDynParams.Where(o => o.EventDoc_ID == eventDoc).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<tblEventDoc> GetAllDocOfEvent(int eID)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    return db.tblEventDocs.Where(o => o.Event_ID == eID).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<tblEventDoc> GetAllDocOfEvent(int eID, byte docType)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    return db.tblEventDocs.Where(o => o.Event_ID == eID && o.Doc_Type == docType).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static tblEventDoc GetEventDoc(int eventDoc)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    return db.tblEventDocs.Find(eventDoc);
                }
            }
            catch
            {
                return null;
            }
        }

        public static int UpdateEventDoc(tblEventDoc eventDoc)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    db.Entry(eventDoc).State = EntityState.Modified;
                    return db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        public static int TotalVotePublish(int EventID)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    int count = db.tblVotePrinteds.Count(o => o.EventID == EventID);
                    return count;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int TotalVoteReturn(int EventID, bool GetInvalid)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    int count = db.tblInvalidVotes.Count(o => o.EventID == EventID && o.IsInvalidVote == GetInvalid);
                    return count;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int TotalVoteReturn(int EventID)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    int count = db.tblInvalidVotes.Count(o => o.EventID == EventID);
                    return count;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int CheckExistBarcodeVote(long _shID, int _eventID)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    int count = db.tblBarcodeVotes.Count(o => o.ShID == _shID && o.EventID == _eventID);
                    return count;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static long SumStockCompany(int _eIDs)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    long result = (long)db.SUM_TOTAL_STOCK(_eIDs).FirstOrDefault();
                    return result;
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public static long SYS_TOTAL_STOCK_PUBLISH(int TopicID)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    long result = (long)db.SYS_TOTAL_STOCK_PUBLISH(TopicID).FirstOrDefault();
                    return result;
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public static long SYS_SUM_STOCK_RESULT(int EventID)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    long result = (long)db.SYS_SUM_STOCK_RESULT(EventID).FirstOrDefault();
                    return result;
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public static long SumStockByStatus(int _eIDs, byte _Status)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    long result = (long)db.SUM_STOCK_By_STATUS(_eIDs, _Status).FirstOrDefault();
                    return result;
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public static long SumStockByStatusCau(int _eIDs, byte _Status, int LocationID)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    long result = (long)db.SUM_STOCK_By_STATUS_CAU(_eIDs, _Status, LocationID).FirstOrDefault();
                    return result;
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public static long SumStockAttend(int _eIDs)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    long result = (long)db.TOTAL_STOCK_ATTEND(_eIDs).FirstOrDefault();
                    return result;
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public static long SYS_SUM_STOCK_RESULT_BASE_ON_INVALID(int _eIDs, bool _IsInvalid)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    long result = (long)db.SYS_SUM_STOCK_RESULT_BASE_ON_INVALID(_eIDs, _IsInvalid).FirstOrDefault();
                    return result;
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
    }
}
