﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataService
{
    public partial class DataAccess
    {
        public static List<tblCandidate> GetListCandidateByEventandType(int eventID, byte type)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    return db.tblCandidates.Where(o => o.EventID == eventID && o.CandidateType == type).OrderBy(o => o.GetOrder).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int CountCandidate(int eventID, byte type)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    return db.tblCandidates.Count(o => o.EventID == eventID && o.CandidateType == type);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static int CountCandidateVoteResult(int eID, long uID, byte type)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    int count = db.tblCandidateIDVoteResults.Count(o => o.EventID == eID && o.ShID== uID && o.voteType== type);
                    return count;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void ClearCandidateVoteResult(int eID, long uID, byte type)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    db.tblCandidateInvalidVotes.RemoveRange(db.tblCandidateInvalidVotes.Where(x => x.EventID == eID && x.CandidateType == type && x.ShID==uID));
                    db.tblCandidateIDVoteResults.RemoveRange(db.tblCandidateIDVoteResults.Where(x => x.EventID == eID && x.voteType == type && x.ShID == uID));
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void InsertCandidateVoteResult(List<tblCandidateIDVoteResult> listVote)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    try
                    {
                        db.Configuration.AutoDetectChangesEnabled = false;
                        db.Configuration.ValidateOnSaveEnabled = false;

                        db.tblCandidateIDVoteResults.AddRange(listVote);

                        db.SaveChanges();
                    }
                    finally
                    {
                        db.Configuration.AutoDetectChangesEnabled = true;
                        db.Configuration.ValidateOnSaveEnabled = true;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int InsertCandicateInvalidVote(tblCandidateInvalidVote obj)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    db.Entry(obj).State = EntityState.Added;
                    return db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int SoTheBauCuPhatRa(int eventID, byte type)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    return db.tblCandidateVotePrinteds.Count(o => o.EventID == eventID && o.CandidateType == type);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int SoTheBauCuCauPhatRa(int eventID, byte type, int LocationID)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    return db.tblCandidateVotePrinteds.Count(o => o.EventID == eventID && o.CandidateType == type && o.LocationID == LocationID);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int SoTheBauCuThuVe(int EventID, byte type)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    int count = db.tblCandidateInvalidVotes.Count(o => o.EventID == EventID && o.CandidateType== type);
                    return count;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int SoTheBauCuCauThuVe(int EventID, byte type, int LocationID)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    int count = db.tblCandidateInvalidVotes.Count(o => o.EventID == EventID && o.CandidateType == type && o.LocationID == LocationID);
                    return count;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int SoTheBauCuThuVeHopLe(int EventID, byte type, bool valid)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    int count = db.tblCandidateInvalidVotes.Count(o => o.EventID == EventID && o.CandidateType == type && o.IsInvalidVote== valid);
                    return count;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static int SoTheBauCuCauThuVeHopLe(int EventID, byte type, bool valid, int LocationID)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    int count = db.tblCandidateInvalidVotes.Count(o => o.EventID == EventID && o.CandidateType == type && o.IsInvalidVote == valid && o.LocationID == LocationID);
                    return count;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static long SoPhieuBau(int EventID, byte type, int Candidate)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    try
                    {
                        long result = (long)db.SYS_SUM_ELECTION_FOR_CANDIDATE(EventID, type, Candidate).FirstOrDefault();
                        return result;
                    }
                    catch
                    {
                        return 0;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static long SoPhieuBauCau(int EventID, byte type, int Candidate, int LocationID)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    try
                    {
                        long result = (long)db.SYS_SUM_ELECTION_FOR_CANDIDATE_CAU(EventID, type, Candidate, LocationID).FirstOrDefault();
                        return result;
                    }
                    catch
                    {
                        return 0;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Số tổng tính % dựa trên tổng số phiều bầu phát ra
        public static long TongSoPhieuBauPhatRa(int EventID, byte type)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    try
                    {
                        long result = (long)db.SYS_SUM_TOTALSTOCK_ON_ELECTION_PRINTED(EventID, type).FirstOrDefault();
                        return result;
                    }
                    catch
                    {
                        return 0;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static long TongSoPhieuBauCauPhatRa(int EventID, byte type, int LocationID)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    try
                    {
                        long result = (long)db.SYS_SUM_TOTALSTOCK_ON_ELECTION_PRINTED_CAU(EventID, type, LocationID).FirstOrDefault();
                        return result;
                    }
                    catch
                    {
                        return 0;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
