﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataService
{
    public partial class DataAccess
    {
        public static long SumStockCompany(int _eIDs)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    long result = (long)db.SUM_TOTAL_STOCK(_eIDs).FirstOrDefault();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static long SYS_TOTAL_STOCK_PUBLISH(int TopicID)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    long result = (long)db.SYS_TOTAL_STOCK_PUBLISH(TopicID).FirstOrDefault();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static long SumStockByStatus(int _eIDs, byte _Status)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    long result = (long)db.SUM_STOCK_By_STATUS(_eIDs, _Status).FirstOrDefault();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static long SumStockAttend(int _eIDs)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    long result = (long)db.TOTAL_STOCK_ATTEND(_eIDs).FirstOrDefault();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
