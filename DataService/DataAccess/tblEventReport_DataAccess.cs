﻿using DataService.DomainModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using VG.Common;

namespace DataService
{
    public partial class DataAccess
    {
        public static List<tblEventReport> GetReportByEvent(int eventID)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    return db.tblEventReports.Where(o => o.EventID == eventID).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
