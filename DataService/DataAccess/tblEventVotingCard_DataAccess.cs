﻿using DataService.DomainModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using VG.Common;

namespace DataService
{
    public partial class DataAccess
    {
        public static List<tblEventVotingCard> ListVotingCard(int _eventID)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    return db.tblEventVotingCards.Where(o => o.EventID == _eventID).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static tblEventVotingCard GetEventVotingCard(int _eventID)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    return db.tblEventVotingCards.Where(o => o.EventID == _eventID).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int UpdateEventVotingCard(tblEventVotingCard obj)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    db.Entry(obj).State = EntityState.Modified;
                    return db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int InsertEventVotingCard(tblEventVotingCard obj)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    db.Entry(obj).State = EntityState.Added;
                    return db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
    
}
