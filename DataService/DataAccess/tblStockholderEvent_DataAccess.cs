﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;

namespace DataService
{
    public partial class DataAccess
    {
        /// <summary>
        /// Tạo mới đại hội và copy các template mặc định từ Document MasterData
        /// </summary>
        /// <param name="objEvent"></param>
        /// <returns></returns>
        public static int AddEvent(tblStockholderEvent objEvent)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    try
                    {
                        db.Configuration.AutoDetectChangesEnabled = false;
                        db.Configuration.ValidateOnSaveEnabled = false;

                        db.Entry(objEvent).State = EntityState.Added;
                        db.SaveChanges();

                        CopyDocToEvent(objEvent.Event_ID);

                        return objEvent.Event_ID;
                    }
                    finally
                    {
                        db.Configuration.AutoDetectChangesEnabled = true;
                        db.Configuration.ValidateOnSaveEnabled = false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int UpdateStatusEvent(int EventID, byte status)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    tblStockholderEvent objEvent = db.tblStockholderEvents.Find(EventID);
                    if (objEvent != null)
                    {
                        objEvent.Status = status;
                        db.Entry(objEvent).State = EntityState.Modified;
                        return db.SaveChanges();
                    }
                    return 0;
                }
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        System.Console.WriteLine("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                    }
                }
                throw dbEx;
            }
        }

        public static int EventChangeConfig(int EventID, int type, bool value)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    tblStockholderEvent objEvent = db.tblStockholderEvents.Find(EventID);
                    if (objEvent != null)
                    {
                        switch (type)
                        {
                            case 1:
                                {
                                    //Biểu quyết
                                    objEvent.HasVote = value;
                                    if (!value)
                                    {
                                        //Xóa các Data liên quan đến Biểu quyết
                                        db.tblBarcodeVotes.RemoveRange(db.tblBarcodeVotes.Where(x => x.EventID == EventID));
                                        db.tblVoteResults.RemoveRange(db.tblVoteResults.Where(x => x.EventID == EventID));
                                        db.tblVotePrinteds.RemoveRange(db.tblVotePrinteds.Where(x => x.EventID == EventID));
                                        db.tblVotingCardTopics.RemoveRange(db.tblVotingCardTopics.Where(x => x.EventID == EventID));
                                        db.tblEventVotingCards.RemoveRange(db.tblEventVotingCards.Where(x => x.EventID== EventID));
                                    }
                                    else
                                    {
                                        //Init các Data liên quan
                                        var objVoting = new tblEventVotingCard() {
                                            EventID= EventID,
                                            CreatedDate = DateTime.Now,
                                            ReportTemplateVN = 12,
                                            ReportTemplateEN = 13
                                        };

                                        db.Entry(objVoting).State = EntityState.Added;
                                    }
                                }
                                break;
                            case 2:
                                {
                                    //Ban Kiểm Soát
                                    objEvent.HasBKS = value;
                                    if (!value)
                                    {
                                        db.tblCandidateInvalidVotes.RemoveRange(db.tblCandidateInvalidVotes.Where(x => x.EventID == EventID && x.CandidateType == 1));
                                        db.tblCandidateVotePrinteds.RemoveRange(db.tblCandidateVotePrinteds.Where(x => x.EventID == EventID && x.CandidateType == 1));
                                        db.tblCandidateIDVoteResults.RemoveRange(db.tblCandidateIDVoteResults.Where(x => x.EventID == EventID && x.voteType==1));
                                        db.tblCandidates.RemoveRange(db.tblCandidates.Where(x => x.EventID == EventID & x.CandidateType == 1));
                                    }
                                }
                                break;
                            case 3:
                                {
                                    //Hội đồng quản trị
                                    objEvent.HasHDQT = value;
                                    if (!value)
                                    {
                                        db.tblCandidateInvalidVotes.RemoveRange(db.tblCandidateInvalidVotes.Where(x => x.EventID == EventID && x.CandidateType == 2));
                                        db.tblCandidateVotePrinteds.RemoveRange(db.tblCandidateVotePrinteds.Where(x => x.EventID == EventID && x.CandidateType == 2));
                                        db.tblCandidateIDVoteResults.RemoveRange(db.tblCandidateIDVoteResults.Where(x => x.EventID == EventID && x.voteType == 2));
                                        db.tblCandidates.RemoveRange(db.tblCandidates.Where(x => x.EventID == EventID & x.CandidateType == 2));
                                    }
                                }
                                break;
                        }
                        db.Entry(objEvent).State = EntityState.Modified;
                        return db.SaveChanges();
                    }
                    return 0;
                }
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        System.Console.WriteLine("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                    }
                }
                throw dbEx;
            }
        }


        public static tblStockholderEvent FindEvent(int EventID)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    return db.tblStockholderEvents.Find(EventID);
                }
            }
            catch
            {
                return null;
            }
        }

        public static int UpdateEvent(tblStockholderEvent objEvent)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    db.Entry(objEvent).State = EntityState.Modified;
                    return db.SaveChanges();
                }
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        System.Console.WriteLine("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                    }
                }
                throw dbEx;
            }
        }

        public static List<tblStockholderEvent> GetEventByPL(int _plID, string _order)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    var list = db.tblStockholderEvents.Where(o => o.PL_ID == _plID && o.Status != 0);
                    switch (_order)
                    {
                        case "from_date":
                            return list.OrderByDescending(o => o.FromDate).ToList();
                        case "created_date":
                            return list.OrderByDescending(o => o.Created_Date).ToList();
                        default:
                            return list.OrderByDescending(o => o.Created_Date).ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static tblStockholderEvent GetEventByID(int _id)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    return db.tblStockholderEvents.Find(_id);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int DeleteMasterPL(int _pl_ID)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    //count Event
                    int _count = db.tblStockholderEvents.Count(t => t.PL_ID == _pl_ID);
                    if (_count == 0)
                    {
                        tblMasterPL objPL = new tblMasterPL() { PL_ID = _pl_ID };
                        db.tblMasterPLs.Attach(objPL);
                        db.tblMasterPLs.Remove(objPL);
                        return db.SaveChanges();
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
