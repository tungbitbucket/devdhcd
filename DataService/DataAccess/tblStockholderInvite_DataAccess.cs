﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Transactions;

namespace DataService
{
    public partial class DataAccess
    {
        public static tblStockholderInvite FindStockHolder(long sHID)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    return db.tblStockholderInvites.Find(sHID);
                }
            }
            catch
            {
                return null;
            }
        }

        public static void UpdateStockholderInvite(tblStockholderInvite stockholder)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    db.Entry(stockholder).State = EntityState.Modified;
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static tblStockholderInvite FindStockHolder(int eID, long uID)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    return db.tblStockholderInvites.Where(o => o.Event_ID == eID && o.SHID == uID).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void DeleteBatchImport(int batchID)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    try
                    {
                        db.Configuration.AutoDetectChangesEnabled = false;
                        db.Configuration.ValidateOnSaveEnabled = false;

                        tblSharreholdImportBatch objPL = new tblSharreholdImportBatch() { Batch_ID = batchID };
                        db.tblSharreholdImportBatches.Attach(objPL);
                        db.tblSharreholdImportBatches.Remove(objPL);

                        db.tblStockholderInvites.RemoveRange(db.tblStockholderInvites.Where(x => x.Batch_ID == batchID));
                        db.SaveChanges();

                    }
                    finally
                    {
                        db.Configuration.AutoDetectChangesEnabled = true;
                        db.Configuration.ValidateOnSaveEnabled = false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int CountGroup(int _eID, int _sGroup)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    int count = db.tblStockholderInvites.Count(o => o.Event_ID == _eID && o.sGroup == _sGroup);
                    return count;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static int CountType(int _eID, int _sType)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    int count = db.tblStockholderInvites.Count(o => o.Event_ID == _eID && o.sType == _sType);
                    return count;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int CountStockholderInvite(int _eID)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    int count = db.tblStockholderInvites.Count(o => o.Event_ID == _eID);
                    return count;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void GenBarcodeStockHolderInviteBulk(List<tblStockholderInvite> listSH)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    try
                    {
                        db.Configuration.AutoDetectChangesEnabled = false;
                        db.Configuration.ValidateOnSaveEnabled = false;

                        foreach (tblStockholderInvite item in listSH)
                        {
                            db.Entry(item).State = EntityState.Modified;
                        }
                        db.SaveChanges();
                    }
                    finally
                    {
                        db.Configuration.AutoDetectChangesEnabled = true;
                        db.Configuration.ValidateOnSaveEnabled = false;
                    }
                }
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        System.Console.WriteLine("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                    }
                }
                throw dbEx;
            }
        }

        public static int CountTotalInvite(int _eID)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    int count = db.tblStockholderInvites.Count(o => o.Event_ID == _eID);
                    return count;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<tblStockholderInvite> GetStockholderInvite(int _eID)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    return db.tblStockholderInvites.Where(o => o.Event_ID == _eID).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static tblStockholderInvite GetStockholderInviteByID(long _shID)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    return db.tblStockholderInvites.Where(o => o.SHID == _shID).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<tblStockholderInvite> GetStockholderInviteStype(int _eID, int _sType)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    return db.tblStockholderInvites.Where(o => o.Event_ID == _eID && o.sType == _sType).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<tblStockholderInvite> GetStockholderInviteStype(int _eID)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    return db.tblStockholderInvites.Where(o => o.Event_ID == _eID & o.IsInternal == false & o.sType == 2 & o.sGroup == 1).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<tblStockholderInvite> GetStockholderInviteNoiBo(int _eID)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    return db.tblStockholderInvites.Where(o => o.Event_ID == _eID && o.IsInternal == true).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<tblStockholderInvite> GetStockholderInviteNoneBarcode(int _eID)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    return db.tblStockholderInvites.Where(o => o.Event_ID == _eID && o.BarCode == null).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int GetNumNotYetGenBarcode(int _eID)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    int count = db.tblStockholderInvites.Count(o => o.Event_ID == _eID && o.BarCode == null);
                    return count;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static IPagedList<tblStockholderInvite> GetStockholderInvite(int _eID, string _search, int? page)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    if (_search != null)
                    {
                        page = 1;
                    }
                    var stockholder = db.tblStockholderInvites.Where(o => o.Event_ID == _eID);

                    if (!String.IsNullOrEmpty(_search))
                    {
                        stockholder = stockholder.Where(s => s.CardID.Contains(_search) || s.FullName.Contains(_search) || s.Mobile.Contains(_search) || s.SHID.ToString() == _search);
                    }

                    stockholder = stockholder.OrderBy(o => o.FullName);

                    int pageSize = 25;
                    int pageNumber = (page ?? 1);
                    return stockholder.ToPagedList(pageNumber, pageSize);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<tblStockholderInvite> GetStockholderInvite(int _eID, int page)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    var stockholder = db.tblStockholderInvites.Where(o => o.Event_ID == _eID);
                    stockholder = stockholder.OrderByDescending(o => o.CreatedDate);

                    int pageSize = 300;
                    int pageNumber = page;
                    return stockholder.ToPagedList(pageNumber, pageSize).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<tblStockholderInvite> GetStockholderInvite2(int _eID, int page)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    var stockholder = db.tblStockholderInvites.Where(o => o.Event_ID == _eID & o.IsInternal == false);
                    stockholder = stockholder.OrderByDescending(o => o.CreatedDate);

                    int pageSize = 500;
                    int pageNumber = page;
                    return stockholder.ToPagedList(pageNumber, pageSize).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<tblStockholderInvite> GetStockholderInviteCustom(int _eID, int page)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    //var stockholder = db.tblStockholderInvites.Where(o => o.Event_ID == _eID && ids.Contains(o.SHID));
                    var stockholder = db.tblStockholderInvites.Where(o => o.Event_ID == _eID);
                    stockholder = stockholder.OrderByDescending(o => o.CreatedDate);
                    //stockholder = stockholder.OrderByDescending(o => o.Quantity);

                    int pageSize = 100000;
                    int pageNumber = page;
                    return stockholder.ToPagedList(pageNumber, pageSize).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<tblStockholderAttend> GetStockholderAttendCustom(int _eID, int page)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    var stockholder = db.tblStockholderAttends.Where(o => o.Event_ID == _eID);
                    stockholder = stockholder.OrderByDescending(o => o.AttendDate);

                    int pageSize = 1000000;
                    int pageNumber = page;
                    return stockholder.ToPagedList(pageNumber, pageSize).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static VIN_DHCDEntities AddToContext(VIN_DHCDEntities context, tblStockholderInvite entity, int count, int commitCount, bool recreateContext)
        {
            context.Set<tblStockholderInvite>().Add(entity);

            if (count % commitCount == 0)
            {
                context.SaveChanges();
                if (recreateContext)
                {
                    context.Dispose();
                    context = new VIN_DHCDEntities();
                    context.Configuration.AutoDetectChangesEnabled = false;
                }
            }
            return context;
        }

        public static List<tblStockholderInvite> ListStockholderByEvent(int _id)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    return db.tblStockholderInvites.Where(o => o.Event_ID == _id).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void ImportStockHolderInvite(List<tblStockholderInvite> listSH)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    try
                    {
                        db.Configuration.AutoDetectChangesEnabled = false;
                        db.Configuration.ValidateOnSaveEnabled = false;

                        db.tblStockholderInvites.AddRange(listSH);
                        db.SaveChanges();
                    }
                    finally
                    {
                        db.Configuration.AutoDetectChangesEnabled = true;
                        db.Configuration.ValidateOnSaveEnabled = true;
                    }

                }
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        System.Console.WriteLine("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                    }
                }
                throw dbEx;
            }
        }

        public static void ImportStockHolderInvite2(List<tblStockholderInvite> listSH)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    VIN_DHCDEntities context = null;
                    try
                    {
                        context = new VIN_DHCDEntities();
                        context.Configuration.AutoDetectChangesEnabled = false;

                        int count = 0;
                        foreach (var entityToInsert in listSH)
                        {
                            ++count;
                            context = AddToContext(context, entityToInsert, count, 100, true);
                        }

                        context.SaveChanges();
                    }
                    finally
                    {
                        if (context != null)
                            context.Dispose();
                    }

                    scope.Complete();
                }
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        System.Console.WriteLine("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                    }
                }
                throw dbEx;
            }
        }
    }
}
