﻿using DataService.DomainModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using VG.Common;
namespace DataService
{
    public partial class DataAccess
    {
        public static tblSysAccount SysUserLogin(string _username, string _pass)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    string encrypass = EncryBase.EncryptMD5(_pass);
                    try
                    {
                        var objUser = db.tblSysAccounts.FirstOrDefault(u => u.Acc_UserName == _username && u.Acc_Pass == encrypass);
                        return objUser;
                    }
                    catch (Exception e)
                    {
                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static tblLocation GetLocationByAccUser(string username)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    tblSysAccount account = db.tblSysAccounts.Where(o => o.Acc_UserName == username).FirstOrDefault();
                    return db.tblLocations.Where(o => o.LocationID == account.Acc_LocationID).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static tblLocation GetLocationByLocationID(int LocationID)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    return db.tblLocations.Where(o => o.LocationID == LocationID).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<tblRole> GetTblRole()
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    return db.tblRoles.ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool UpdateRole(tblRole objRole)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    try
                    {
                        db.Configuration.AutoDetectChangesEnabled = false;
                        db.Configuration.ValidateOnSaveEnabled = false;

                        db.Entry(objRole).State = EntityState.Modified;
                        return db.SaveChanges().ToBool();
                    }
                    finally
                    {
                        db.Configuration.AutoDetectChangesEnabled = true;
                        db.Configuration.ValidateOnSaveEnabled = false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool InsertRoleMapping(int _accID, int _roleID)
        {
            try
            {
                var objRoleMapping = new tblRoleMapping()
                {
                    Acc_ID = _accID,
                    Role_ID = _roleID
                };
                return InsertRoleMapping(objRoleMapping);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool InsertRoleMapping(tblRoleMapping objRoleMapping)
        {
            try
            {
                using (var db = new VIN_DHCDEntities())
                {
                    try
                    {
                        db.Configuration.AutoDetectChangesEnabled = false;
                        db.Configuration.ValidateOnSaveEnabled = false;

                        db.Entry(objRoleMapping).State = EntityState.Added;
                        return db.SaveChanges().ToBool();
                    }
                    finally
                    {
                        db.Configuration.AutoDetectChangesEnabled = true;
                        db.Configuration.ValidateOnSaveEnabled = false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<Role> GetListRoleOfUser(int _accID)
        {
            try
            {
                List<Role> listRole = new List<Role>();
                using (var db = new VIN_DHCDEntities())
                {
                    try
                    {
                        db.Configuration.AutoDetectChangesEnabled = false;
                        var listRoleMapping = db.tblRoleMappings.Where(o => o.Acc_ID == _accID);
                        foreach (tblRoleMapping x in listRoleMapping)
                        {
                            var tblRole = db.tblRoles.Find(x.Role_ID);
                            listRole.Add(new Role()
                            {
                                Role_ID = tblRole.Role_ID,
                                RoleFunName = tblRole.Role_FncName,
                                Role_Name = tblRole.Role_Desc
                            });
                        }
                    }
                    finally
                    {
                        db.Configuration.AutoDetectChangesEnabled = true;
                    }

                }
                return listRole;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
