﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VG.Common
{
    public class ConstKey
    {
        public static string HashKey = ConfigurationManager.AppSettings["HashKey"] ?? "9o32jogqbr";
        public static string LocalDir = ConfigurationManager.AppSettings["LocalDir"] ?? "";
        public static string printInvitationGet = ConfigurationManager.AppSettings["printInvitationGet"] ?? "";
        public static string printInvitation2Get = ConfigurationManager.AppSettings["printInvitation2Get"] ?? "";
        public static string printInvitationPost = ConfigurationManager.AppSettings["printInvitationPost"] ?? "";
        public static string RabbitConnect = ConfigurationManager.AppSettings["RabbitConnect"] ?? "host=127.0.0.1:5672;username=guest;password=guest;publisherConfirms=true";
        public static string ServiceCollections = ConfigurationManager.AppSettings["ServiceCollections"] ?? "";

        #region Redis-Replication-Config
        public static string RedisProvider = ConfigurationManager.AppSettings["RedisProvider"];
        public static string ServerWriteIp = ConfigurationManager.AppSettings["RedisHostMaster"];
        public static string ServerReadIp = ConfigurationManager.AppSettings["RedisHostSlaver"];
        public static int RedisDB = Convert.ToInt32(ConfigurationManager.AppSettings["RedisDB"] ?? "0");
        public static long ExpiresTime = string.IsNullOrEmpty(ConfigurationManager.AppSettings["MinExpires"]) ? 604800000 : Convert.ToInt64(ConfigurationManager.AppSettings["MinExpires"]);
        public const int KeepAlive = 10;
        #endregion

        #region Redis-Cluster-Config
        public static string ClusterServerIp = ConfigurationManager.AppSettings["RedisClusterHostServer"];
        public static int ClusterNodeCount = ConfigurationManager.AppSettings["ClusterNodeCount"] == null ? 0 : Int32.Parse(ConfigurationManager.AppSettings["ClusterNodeCount"]);
        #endregion
    }
}
