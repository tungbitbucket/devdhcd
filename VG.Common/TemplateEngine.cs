﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace VG.Common
{
    public class TemplateEngine
    {
        /// <summary>
        /// Đọc các Params động được định nghĩa riêng cho template này. Yêu cầu định nghĩa dạng {param_name}
        /// </summary>
        /// <param name="strSource"></param>
        /// <returns></returns>
        public static List<ParamValue> GetListDynamicParams(string strSource)
        {
            List<ParamValue> fillData = new List<ParamValue>();

            Regex r1 = new Regex(@"{\w*\}");
            MatchCollection match = r1.Matches(strSource);
            for (int i = 0; i < match.Count; i++)
            {
                if (!fillData.Any(p => p.strParam == match[i].Value))
                {
                    fillData.Add(new ParamValue()
                    {
                        strParam = match[i].Value,
                        strValue = string.Empty
                    });
                }
            }
            return fillData;
        }

        public static string FillTemplate(List<ParamValue> fillData, string strSource)
        {
            foreach (ParamValue data in fillData)
            {
                strSource = strSource.Replace(data.strParam, data.strValue);
            }
            return strSource;
        }
    }

    public class ParamValue
    {
        public string strParam { get; set; }
        public string strValue { get; set; }
    }
}
