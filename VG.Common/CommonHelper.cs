﻿using HashidsNet;
using System;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text.RegularExpressions;
using ZXing;
using ZXing.QrCode;

namespace VG.Common
{
    public class CommonHelper
    {
        #region HashidsNet
        public static string HashEncodeId(int pId)
        {
            string value = string.Empty;
            var hashids = new Hashids(ConstKey.HashKey);
            value = hashids.Encode(pId);
            return value;
        }
        public static bool HashDecodeId(string hashValue, out int pId)
        {
            pId = 0;
            var hashids = new Hashids(ConstKey.HashKey);
            try
            {
                var obj = hashids.Decode(hashValue);
                if (obj != null)
                {
                    var temp = hashids.Decode(hashValue)[0];
                    return int.TryParse(temp.ToString(), out pId);
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }
        public static string HashEncodeId(long pId)
        {
            string value = string.Empty;
            var hashids = new Hashids(ConstKey.HashKey);
            value = hashids.EncodeLong(pId);
            return value;
        }
        public static bool HashDecodeId(string hashValue, out long pId)
        {
            pId = 0;
            var hashids = new Hashids(ConstKey.HashKey);
            try
            {
                var obj = hashids.DecodeLong(hashValue);
                if (obj != null)
                {
                    var temp = hashids.Decode(hashValue)[0];
                    return long.TryParse(temp.ToString(), out pId);
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }
        #endregion

        #region Unicode
        private const string uniChars = "àáảãạâầấẩẫậăằắẳẵặèéẻẽẹêềếểễệđìíỉĩịòóỏõọôồốổỗộơờớởỡợùúủũụưừứửữựỳýỷỹỵÀÁẢÃẠÂẦẤẨẪẬĂẰẮẲẴẶÈÉẺẼẸÊỀẾỂỄỆĐÌÍỈĨỊÒÓỎÕỌÔỒỐỔỖỘƠỜỚỞỠỢÙÚỦŨỤƯỪỨỬỮỰỲÝỶỸỴÂĂĐÔƠƯ";
        private const string KoDauChars = "aaaaaaaaaaaaaaaaaeeeeeeeeeeediiiiiooooooooooooooooouuuuuuuuuuuyyyyyaaaaaaaaaaaaaaaaaeeeeeeeeeeediiiooooooooooooooooooouuuuuuuuuuuyyyyyaadoou";

        public static string UnicodeToKoDau(string s)
        {
            string retVal = String.Empty;
            int pos;
            for (int i = 0; i < s.Length; i++)
            {
                pos = uniChars.IndexOf(s[i].ToString());
                if (pos >= 0)
                    retVal += KoDauChars[pos];
                else
                    retVal += s[i];
            }
            return retVal;
        }
        public static string UnicodeToKoDauAndGach(string s)
        {
            if (string.IsNullOrEmpty(s))
                return "";
            string retVal = String.Empty;
            int pos;

            for (int i = 0; i < s.Length; i++)
            {
                pos = uniChars.IndexOf(s[i].ToString());
                if (pos >= 0)
                    retVal += KoDauChars[pos];
                else
                    retVal += s[i];
            }
            String temp = retVal;
            for (int i = 0; i < retVal.Length; i++)
            {
                pos = Convert.ToInt32(retVal[i]);
                if (!((pos >= 97 && pos <= 122) || (pos >= 65 && pos <= 90) || (pos >= 48 && pos <= 57) || pos == 32))
                    temp = temp.Replace(retVal[i].ToString(), "");
            }
            temp = temp.Replace(" ", "_");
            while (temp.EndsWith("_"))
                temp = temp.Substring(0, temp.Length - 1);

            while (temp.IndexOf("__") >= 0)
                temp = temp.Replace("__", "_");

            retVal = temp;

            return retVal.ToLower();
        }

        public static string repSC(string str)
        {
            try
            {
                if (!string.IsNullOrEmpty(str.Trim()))
                {
                    string t = Regex.Replace(str, @"<script\b[^<]*(?:(?!<\/script>)<[^<]*)*<\/script>", string.Empty);
                    return t.Trim();
                }
            }
            catch (Exception) { }
            return "";
        }

        public static string HtmlStrip(string text)
        {
            if (string.IsNullOrEmpty(text))
                return "";
            text = text.Trim();
            if (text != "")
            {
                text = Regex.Replace(text, @"<(.|\n)*?>", string.Empty).Replace('"', '\'').Replace("onclick", "").Replace("javascript:", "javascript").TrimStart(':').TrimEnd(':').Replace("^", "").Replace("(", "").Replace(")", "").Replace("/", "");
            }
            return text;
        }

        public static bool CheckExpire(string strDate)
        {
            DateTime date = DateTime.ParseExact(strDate, "MM.dd.yyyy", null);
            if (date >= DateTime.Today)
            {
                return true;
            }
            return false;
        }

        public static string CreateFileName()
        {
            var dtnow = DateTime.Now;
            return dtnow.Day.ToString() + dtnow.Month.ToString() + dtnow.Year.ToString() + dtnow.Hour.ToString() + dtnow.Minute.ToString() + dtnow.Second.ToString() + dtnow.Millisecond.ToString();
        }

        public static string RemoveZero(string number)
        {
            if (string.IsNullOrEmpty(number) || number == "0" || number == "0.0") return number;
            number = number.Trim();
            if (number[number.Length - 1] != '0') return number;
            number = number.Substring(0, number.Length - 1);
            return RemoveZero(number);
        }

        public static string TrimZeroStart(string number)
        {
            if (string.IsNullOrEmpty(number) || number == "0" || number == "0.0") return number;
            number = number.TrimStart(new Char[] { '0' });
            return number;
        }

        public static string AddZero(string number, int numberzero)
        {
            for (var i = 0; i < numberzero; i++)
            {
                number += "0";
            }
            return number;
        }
        #endregion

        public static bool SaveTempFile(DataTable dt, string url)
        {
            try
            {
                FileStream fs = new FileStream(url, FileMode.Create);
                BinaryFormatter bf = new BinaryFormatter();
                bf.Serialize(fs, dt);
                fs.Close();

                return true;
            }
            catch
            {
                return false;
            }

        }

        public static DataTable DeserializzeData(string url)
        {
            try
            {
                FileStream fs = new FileStream(url, FileMode.Open);
                BinaryFormatter bf = new BinaryFormatter();
                DataTable dt = (DataTable)bf.Deserialize(fs);
                fs.Close();

                return dt;
            }
            catch
            {
                return null;
            }
        }

        public static DateTime ParseRequestDate(string value)
        {

            // Scenario #1
            if (long.TryParse(value, out long dtLong))
                return dtLong.FromUnixTime();
            // Scenario #2
            string[] formatStrings = { "d/M/yyyy", "d-M-yyyy", "d-MMM-yy", "d-MMMM-yyyy", "M/d/yyyy", "M-d-yyyy", };
            if (DateTime.TryParseExact(value, formatStrings, CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime dateValue))
                return dateValue;

            throw new Exception("Don't know how to parse...");
        }

        public static string VoteOption(int rp)
        {
            switch (rp)
            {
                case 0:
                    return "Tán thành";
                case 1:
                    return "Không tán thành";
                case 2:
                    return "Không có ý kiến";
                default:
                    return "Không hợp lệ";
            }
        }

        public static string VoteOptionEN(int rp)
        {
            switch (rp)
            {
                case 0:
                    return "Agree";
                case 1:
                    return "Disagree";
                case 2:
                    return "Abstain";
                default:
                    return "invalid";
            }
        }

        public static string BarcodeToBase64(string sid)
        {
            var writer = new BarcodeWriter
            {
                Format = BarcodeFormat.CODE_128,
                Options = new ZXing.Common.EncodingOptions { Height = 40, Margin = 0, PureBarcode = true }
            };
            var bitmap = writer.Write(sid);
            Graphics g = Graphics.FromImage(bitmap);

            MemoryStream ms = new MemoryStream();
            bitmap.Save(ms, ImageFormat.Png);
            byte[] byteImage = ms.ToArray();
            string SigBase64 = Convert.ToBase64String(byteImage);
            return SigBase64;
        }

        public static string BarcodeToBase64(long sid)
        {
            string hash = HashEncodeId(sid);
            return BarcodeToBase64(hash);
        }

        public static string QrCodeToBase64(string sid)
        {
            var options = new QrCodeEncodingOptions
            {
                DisableECI = true,
                CharacterSet = "UTF-8",
                Width = 200,
                Height = 200,
                Margin = 2
            };

            var writer = new BarcodeWriter
            {
                Format = BarcodeFormat.QR_CODE,
                Options = options
            };
            var bitmap = writer.Write(sid);
            Graphics g = Graphics.FromImage(bitmap);

            MemoryStream ms = new MemoryStream();
            bitmap.Save(ms, ImageFormat.Png);
            byte[] byteImage = ms.ToArray();
            string SigBase64 = Convert.ToBase64String(byteImage);
            return SigBase64;
        }

        public static string GetCurrencyFormart(long value, string cul)
        {
            if (value == 0)
            {
                return "0";
            }
            else
            {
                if (cul == "vn")
                {
                    return value.ToString("C", CultureInfo.CreateSpecificCulture("vi-VN"));
                }
                else
                {
                    return value.ToString("C", CultureInfo.CreateSpecificCulture("en-US"));
                }
            }
        }

        public static string GetDecimalFormart(long value, string cul)
        {
            if (value == 0)
            {
                return "0";
            }
            else
            {
                if (cul == "vn")
                {
                    return value.ToString("N0", CultureInfo.CreateSpecificCulture("vi-VN"));
                }
                else
                {
                    return value.ToString("N0", CultureInfo.CreateSpecificCulture("en-US"));
                }
            }
        }

        public static double GetPercent(double valid, double total)
        {
            double percent = (double)(valid * 100) / total;
            return percent;
        }
    }


    public class Singleton<T> where T : class, new()
    {
        private Singleton() { }
        private static readonly Lazy<T> instance = new Lazy<T>(() => new T());
        public static T Instance { get { return instance.Value; } }
    }
}
