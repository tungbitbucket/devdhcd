﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VG.Common
{
    public static partial class MyExtensionMethods
    {
        #region PROVATE FUNCTION
        private static string Compress(string str_JSonValue)
        {
            String str_CompressedJson = String.Empty;
            using (MemoryStream memory = new MemoryStream())
            {
                using (GZipStream gzip = new GZipStream(memory, CompressionMode.Compress, true))
                {
                    byte[] raw = Encoding.UTF8.GetBytes(str_JSonValue);
                    gzip.Write(raw, 0, raw.Length);
                    gzip.Close();
                }
                byte[] zipped = memory.ToArray();
                str_CompressedJson = Convert.ToBase64String(zipped);
            }
            return str_CompressedJson;
        }
        private static string Decompress(string gzip)
        {
            String str_DecompressJson = String.Empty;
            byte[] raw = Encoding.UTF8.GetBytes(gzip);
            using (GZipStream stream = new GZipStream(new MemoryStream(raw), CompressionMode.Decompress))
            {
                const int size = 4096;
                byte[] buffer = new byte[size];
                using (MemoryStream memory = new MemoryStream())
                {
                    int count = 0;
                    do
                    {
                        count = stream.Read(buffer, 0, size);
                        if (count > 0)
                        {
                            memory.Write(buffer, 0, count);
                        }
                    }
                    while (count > 0);
                    byte[] unzip = memory.ToArray();
                    str_DecompressJson = Convert.ToBase64String(unzip);
                }
                stream.Close();
            }
            return str_DecompressJson;
        }
        private static byte[] Compress(byte[] raw)
        {
            using (MemoryStream memory = new MemoryStream())
            {
                using (GZipStream gzip = new GZipStream(memory, CompressionMode.Compress, true))
                {
                    gzip.Write(raw, 0, raw.Length);
                }
                return memory.ToArray();
            }
        }
        private static byte[] Decompress(byte[] gzip)
        {
            using (GZipStream stream = new GZipStream(new MemoryStream(gzip), CompressionMode.Decompress))
            {
                const int size = 4096;
                byte[] buffer = new byte[size];
                using (MemoryStream memory = new MemoryStream())
                {
                    int count = 0;
                    do
                    {
                        count = stream.Read(buffer, 0, size);
                        if (count > 0)
                        {
                            memory.Write(buffer, 0, count);
                        }
                    }
                    while (count > 0);
                    return memory.ToArray();
                }
            }
        }
        #endregion

        public static string ToJson<T>(this T obj, bool isCompress)
        {
            string strSerialize = JsonConvert.SerializeObject(obj);
            if (isCompress)
                strSerialize = Compress(strSerialize);
            return strSerialize;
        }
        public static T ToObject<T>(this string strJson, bool isDecompress)
        {
            if (isDecompress)
                strJson = Decompress(strJson);
            return JsonConvert.DeserializeObject<T>(strJson);
        }
        public static bool ToBool(this int value)
        {
            if (value == 1)
                return true;
            else
                return false;
        }
        public static bool BooleanNullToFalse(dynamic value)
        {
            return value ?? false;
        }

        public static bool ToBool(this string value)
        {
            if (value.ToLower() == "on")
                return true;
            else
                return false;
        }
        public static string ToUTF8(this byte[] value)
        {
            return Encoding.UTF8.GetString(value);
        }
        public static byte[] GetByte(this string value)
        {
            return Encoding.UTF8.GetBytes(value);
        }
        public static int ToInt32OrDefault(this string value, int defaultValue = 0)
        {
            int result;
            return int.TryParse(value, out result) ? result : defaultValue;
        }
        public static double ToDouble(this string value)
        {
            return double.Parse(value);
        }
        public static string Trim4(this string value, int len)
        {
            if (string.IsNullOrEmpty(value)) return "";
            if (value.Length > len) return value.Substring(len);
            return value;
        }
        public static string ToString4(this DateTime value, string formart)
        {
            try
            {
                return value.ToString(formart);
            }
            catch
            {
                return "";
            }
        }

        public static string ToStyleClass(this int value)
        {
            if (value == 1)
                return "text-success";
            else
                return "text-muted";
        }

        public static string GetDescription(this Enum value)
        {
            if (value.GetType().GetField(value.ToString()) != null)
            {
                DescriptionAttribute attribute = value.GetType()
                    .GetField(value.ToString())
                    .GetCustomAttributes(typeof(DescriptionAttribute), false)
                    .SingleOrDefault() as DescriptionAttribute;
                return attribute == null ? value.ToString() : attribute.Description;
            }
            else
            {
                return string.Empty;
            }

        }
        public static DateTime FromUnixTime(this long unixTime)
        {
            var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            return epoch.AddSeconds(unixTime);
        }

        public static bool IsInRange(this DateTime dateToCheck)
        {
            DateTime startDate = new DateTime(1753, 1, 1);
            DateTime endDate = new DateTime(9999, 12, 31);
            return dateToCheck >= startDate && dateToCheck < endDate;
        }

        public static string GetNumberDisplayVN(this double d)
        {
            if (d != 100)
            {
                return String.Format(CultureInfo.CreateSpecificCulture("vi-VN"), "{0:0.00}", d);
            }
            else
            {
                return Math.Round(d, 2).ToString(CultureInfo.CreateSpecificCulture("vi-VN"));
            }
        }
        public static string GetNumberDisplayUS(this double d)
        {
            if (d != 100)
            {
                return String.Format(CultureInfo.CreateSpecificCulture("en-US"), "{0:0.00}", d);
            }
            else
            {
                return Math.Round(d, 2).ToString(CultureInfo.CreateSpecificCulture("en-US"));
            }
        }
        public static string addZero(this int value)
        {
            return value.ToString("D8");
        }
    }
}
