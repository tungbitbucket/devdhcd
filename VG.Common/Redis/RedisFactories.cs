﻿using VG.Common.Redis.Implementation;
using VG.Common.Redis.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VG.Common.Redis
{
    public class RedisFactories
    {
        static public IRedisClient GetRedisClient(string Provider)
        {
            IRedisClient RedisSelector = null;
            switch (Provider.ToLower())
            {
                case "replication-write":
                    RedisSelector = ReplicationRedis.Current;
                    break;
                case "replication-read":
                    RedisSelector = ReplicationRedis.Current;
                    break;
                case "cluster":
                    RedisSelector = ClusterRedis.Current;
                    break;
                default:
                    RedisSelector = ReplicationRedis.Current;
                    break;
            }
            return RedisSelector;
        }
    }
}
