﻿using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VG.Common.Redis.Interface
{
    public interface IRedisClient
    {
        IDatabase CurrentConnection();
    }
}
