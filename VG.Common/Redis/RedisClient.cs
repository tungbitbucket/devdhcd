﻿using Newtonsoft.Json;
using ProtoBuf;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VG.Common.Redis
{
    public class RedisClient
    {
        #region Json
        public static string JsonSerialize(Object item, bool isCompress)
        {
            string strSerialize = JsonConvert.SerializeObject(item);
            if (isCompress)
                strSerialize = Compress(strSerialize);
            return strSerialize;
        }
        public static T JsonDeserialize<T>(string strJson, bool isDecompress)
        {
            if (isDecompress)
                strJson = Decompress(strJson);
            return JsonConvert.DeserializeObject<T>(strJson);
        }
        public static string Compress(string str_JSonValue)
        {
            String str_CompressedJson = String.Empty;
            using (MemoryStream memory = new MemoryStream())
            {
                using (GZipStream gzip = new GZipStream(memory, CompressionMode.Compress, true))
                {
                    byte[] raw = Encoding.UTF8.GetBytes(str_JSonValue);
                    gzip.Write(raw, 0, raw.Length);
                    gzip.Close();
                }
                byte[] zipped = memory.ToArray();
                str_CompressedJson = Convert.ToBase64String(zipped);
            }
            return str_CompressedJson;
        }
        public static string Decompress(string gzip)
        {
            String str_DecompressJson = String.Empty;
            byte[] raw = Encoding.UTF8.GetBytes(gzip);
            using (GZipStream stream = new GZipStream(new MemoryStream(raw), CompressionMode.Decompress))
            {
                const int size = 4096;
                byte[] buffer = new byte[size];
                using (MemoryStream memory = new MemoryStream())
                {
                    int count = 0;
                    do
                    {
                        count = stream.Read(buffer, 0, size);
                        if (count > 0)
                        {
                            memory.Write(buffer, 0, count);
                        }
                    }
                    while (count > 0);
                    byte[] unzip = memory.ToArray();
                    str_DecompressJson = Convert.ToBase64String(unzip);
                }
                stream.Close();
            }
            return str_DecompressJson;
        }
        #endregion

        #region Probuf
        public static byte[] ProtoBufSerialize(Object item)
        {
            if (item != null)
            {
                try
                {
                    var ms = new MemoryStream();
                    Serializer.Serialize(ms, item);
                    var rt = ms.ToArray();
                    return rt;
                }
                catch (Exception ex)
                {
                    throw new Exception("Unable to serialize object", ex);
                }
            }
            else
            {
                throw new Exception("Object serialize is null");
            }

        }
        public static byte[] ProtoBufSerialize(Object item, bool isCompress)
        {
            if (item != null)
            {
                try
                {
                    var ms = new MemoryStream();
                    Serializer.Serialize(ms, item);
                    var rt = ms.ToArray();
                    if (isCompress)
                    {
                        rt = Compress(rt);
                    }
                    return rt;
                }
                catch (Exception ex)
                {
                    throw new Exception("Unable to serialize object", ex);
                }
            }
            else
            {
                throw new Exception("Object serialize is null");
            }
        }
        public static T ProtoBufDeserialize<T>(byte[] byteArray)
        {
            if (byteArray != null && byteArray.Length > 0)
            {
                try
                {
                    var ms = new MemoryStream(byteArray);
                    return Serializer.Deserialize<T>(ms);
                }
                catch (Exception ex)
                {
                    throw new Exception("ProtoBufDeserialize Error", ex);
                }
            }
            else
            {
                throw new Exception("Object Deserialize is null or empty");
            }
        }
        public static T ProtoBufDeserialize<T>(byte[] byteArray, bool isDecompress)
        {
            if (byteArray != null && byteArray.Length > 0)
            {
                try
                {
                    if (isDecompress)
                    {
                        byteArray = Decompress(byteArray);
                    }
                    var ms = new MemoryStream(byteArray);
                    return Serializer.Deserialize<T>(ms);
                }
                catch (Exception ex)
                {
                    throw new Exception("ProtoBufDeserialize Compress Error", ex);
                }
            }
            else
            {
                throw new Exception("Object Deserialize is null or empty");
            }
        }
        public static byte[] Compress(byte[] raw)
        {
            using (MemoryStream memory = new MemoryStream())
            {
                using (GZipStream gzip = new GZipStream(memory, CompressionMode.Compress, true))
                {
                    gzip.Write(raw, 0, raw.Length);
                }
                return memory.ToArray();
            }
        }
        public static byte[] Decompress(byte[] gzip)
        {
            using (GZipStream stream = new GZipStream(new MemoryStream(gzip), CompressionMode.Decompress))
            {
                const int size = 4096;
                byte[] buffer = new byte[size];
                using (MemoryStream memory = new MemoryStream())
                {
                    int count = 0;
                    do
                    {
                        count = stream.Read(buffer, 0, size);
                        if (count > 0)
                        {
                            memory.Write(buffer, 0, count);
                        }
                    }
                    while (count > 0);
                    return memory.ToArray();
                }
            }
        }
        #endregion

        #region Get
        public static byte[] StringGet(string key)
        {
            var obj = RedisFactories.GetRedisClient(ConstKey.RedisProvider).CurrentConnection().StringGet(key);
            if (!obj.IsNullOrEmpty)
            {
                return (byte[])obj;
            }
            return null;
        }
        public static string StringGetString(string key)
        {
            try
            {
                return (string)RedisFactories.GetRedisClient(ConstKey.RedisProvider).CurrentConnection().StringGet(key);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static bool KeysExists(string key)
        {
            try
            {
                return RedisFactories.GetRedisClient(ConstKey.RedisProvider).CurrentConnection().KeyExists(key);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Set
        public static bool KeyRemove(string key)
        {
            bool flag = false;
            try
            {
                var redisClient = RedisFactories.GetRedisClient(ConstKey.RedisProvider).CurrentConnection();
                var trans = redisClient.CreateTransaction();
                trans.KeyDeleteAsync(key);
                flag = trans.Execute();
                return flag;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static bool KeysExpire(string key, int expirySeconds)
        {
            bool flag = false;
            try
            {
                flag = RedisFactories.GetRedisClient(ConstKey.RedisProvider).CurrentConnection().KeyExpire(key, DateTime.Now.AddSeconds(expirySeconds));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return flag;
        }
        public static bool StringSet(string key, string value)
        {
            bool flag = false;
            try
            {
                var redisClient = RedisFactories.GetRedisClient(ConstKey.RedisProvider).CurrentConnection();
                var trans = redisClient.CreateTransaction();
                trans.StringSetAsync(key, value);
                flag = trans.Execute();
                return flag;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static bool StringSet(string key, string value, long expirySeconds)
        {

            try
            {
                bool flag = false;
                TimeSpan time = TimeSpan.FromSeconds(expirySeconds);
                var redisClient = RedisFactories.GetRedisClient(ConstKey.RedisProvider).CurrentConnection();
                var trans = redisClient.CreateTransaction();
                trans.StringSetAsync(key, value, time);
                flag = trans.Execute();
                return flag;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public static bool StringSet(string key, byte[] data)
        {
            try
            {
                bool flag = false;
                var redisClient = RedisFactories.GetRedisClient(ConstKey.RedisProvider).CurrentConnection();
                var trans = redisClient.CreateTransaction();
                trans.StringSetAsync(key, data);
                flag = trans.Execute();
                return flag;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// Set value to redis
        /// </summary>
        /// <param name="key">Key</param>
        /// <param name="data">Byte[]</param>
        /// <param name="expirySeconds">Seconds</param>
        /// <returns></returns>
        public static bool StringSet(string key, byte[] data, long expirySeconds)
        {
            try
            {
                bool flag = false;
                TimeSpan time = TimeSpan.FromSeconds(expirySeconds);
                var redisClient = RedisFactories.GetRedisClient(ConstKey.RedisProvider).CurrentConnection();
                var trans = redisClient.CreateTransaction();
                trans.StringSetAsync(key, data, time);
                flag = trans.Execute();
                return flag;
            }
            catch (Exception ex)
            {
                throw ex;

            }
        }
        #endregion

        public static void StoreHash<T>(string key, List<T> objs, string propertyName)
        {
            var redisClient = RedisFactories.GetRedisClient(ConstKey.RedisProvider).CurrentConnection();
            List<HashEntry> arrHashValue = new List<HashEntry>();
            foreach (T p in objs)
            {
                if (p == null) continue;
                var pro = p.GetType().GetProperties().FirstOrDefault(x => x.Name == propertyName);
                string keyHash = pro.GetValue(p).ToString();

                arrHashValue.Add(new HashEntry(keyHash, JsonSerialize(p, false)));
            }
            redisClient.HashSet(key, arrHashValue.ToArray());
        }

        public static List<T> GetListInHash<T>(string key)
        {
            var redisClient = RedisFactories.GetRedisClient(ConstKey.RedisProvider).CurrentConnection();
            RedisValue[] redisValue = redisClient.HashValues(key);
            if (redisValue.Count() == 0)
                return null;
            List<T> ReturnList = new List<T>();
            foreach (RedisValue x in redisValue)
            {
                ReturnList.Add(JsonDeserialize<T>(x.ToString(), false));
            }
            return ReturnList;
        }

        public static async Task<bool> InsertObjectIntoHash<T>(string key, T obj, string Id)
        {
            var redisClient = RedisFactories.GetRedisClient(ConstKey.RedisProvider).CurrentConnection();
            RedisValue IdHash = new RedisValue();
            IdHash = Id;
            RedisValue HashValue = new RedisValue();
            HashValue = JsonSerialize(obj, false);
            return await redisClient.HashSetAsync(key, IdHash, HashValue);
        }

        public static async Task<bool> DeleleObjectInHash(string key, string Id)
        {
            var redisClient = RedisFactories.GetRedisClient(ConstKey.RedisProvider).CurrentConnection();
            RedisValue IdHash = new RedisValue();
            IdHash = Id;
            return await redisClient.HashDeleteAsync(key, IdHash);
        }
    }
}
