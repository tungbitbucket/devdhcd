﻿using StackExchange.Redis;
using System;
using System.Net;
using System.Threading.Tasks;
using VG.Common.Redis.Interface;
using VG.Common;

namespace VG.Common.Redis.Implementation
{
    sealed class ClusterRedis : IRedisClient
    {
        private const int IoTimeOut = 50000;
        private const int SyncTimeout = 50000;
        private const int ConnectRetry = 5;
        private const int KeepAlive = 5;
        private static SocketManager _socketManager;
        private ConnectionMultiplexer _connection;
        private static volatile ClusterRedis _instance;

        public static readonly object SyncLock = new object();
        public static readonly object SyncConnectionLock = new object();
        public static ClusterRedis Current
        {
            get
            {
                if (_instance == null)
                {
                    lock (SyncLock)
                    {
                        if (_instance == null)
                        {
                            _instance = new ClusterRedis(false);
                        }
                    }
                }
                return _instance;
            }
        }
        private ClusterRedis(bool checkConnect = false)
        {
            _socketManager = new SocketManager(GetType().Name);
            _connection = GetNewConnection(checkConnect);
        }
        private static ConnectionMultiplexer GetNewConnection(bool checkConnect = false)
        {
            var config = ConfigurationOptions.Parse(ConstKey.ClusterServerIp);

            config.ConnectRetry = ConnectRetry;
            config.AbortOnConnectFail = false;
            config.ConnectTimeout = IoTimeOut;
            config.KeepAlive = KeepAlive;
            config.SyncTimeout = SyncTimeout;
            config.AllowAdmin = true;
            config.SocketManager = _socketManager;

            var connection = ConnectionMultiplexer.ConnectAsync(config);

            if (!connection.Wait(config.ConnectTimeout >= (int.MaxValue / 2) ? int.MaxValue : config.ConnectTimeout * 2))
            {
                connection.ContinueWith(x =>
                {
                    try
                    {
                        GC.KeepAlive(x.Exception);
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }, TaskContinuationOptions.OnlyOnFaulted);
                throw new TimeoutException("Connect timeout");
            }
            var muxer = connection.Result;
            if (checkConnect)
            {
                if (!IsExistsMaster(muxer))
                {
                    throw new Exception("Server is not available");
                }
            }
            return muxer;
        }
        private static bool IsExistsMaster(ConnectionMultiplexer muxer)
        {
            EndPoint[] endpoints = muxer.GetEndPoints();
            bool result = true;
            int masterNodeCount = 0;

            foreach (var endpoint in endpoints)
            {
                var server = muxer.GetServer(endpoint);

                if (server.IsConnected)
                {
                    if (!server.IsSlave)
                    {
                        masterNodeCount++;
                    }
                }
            }

            if (masterNodeCount < ConstKey.ClusterNodeCount)
            {
                throw new InvalidOperationException($"The number of master endpoint is not equals cluster node number (found {masterNodeCount}/{ConstKey.ClusterNodeCount} nodes");
            }

            return result;
        }
        public ConnectionMultiplexer GetConnection
        {
            get
            {
                lock (SyncConnectionLock)
                {
                    if (_connection == null)
                        _connection = GetNewConnection(false);
                    if (!_connection.IsConnected)
                        _connection = GetNewConnection(false);

                    if (_connection.IsConnected)
                        return _connection;
                    return _connection;
                }
            }
        }

        public IDatabase CurrentConnection()
        {
            var connection = Current.GetConnection.GetDatabase();
            return connection;
        }
    }
}
