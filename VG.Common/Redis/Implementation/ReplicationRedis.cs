﻿using VG.Common.Redis.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StackExchange.Redis;

namespace VG.Common.Redis.Implementation
{
    sealed class ReplicationRedis : IRedisClient
    {
        private const int IoTimeOut = 50000;
        private const int SyncTimeout = 50000;
        private static SocketManager _socketManager;
        private ConnectionMultiplexer _connection;

        private static volatile ReplicationRedis _instance;
        private static readonly object SyncLock = new object();
        public static readonly object SyncConnectionLock = new object();
        private ReplicationRedis()
        {
            _socketManager = new SocketManager(GetType().Name);
            _connection = GetNewConnection();
        }
        public static ReplicationRedis Current
        {
            get
            {
                if (_instance == null)
                {
                    lock (SyncLock)
                    {
                        if (_instance == null)
                        {
                            _instance = new ReplicationRedis();
                        }
                    }
                }

                return _instance;
            }
        }
        private static ConnectionMultiplexer GetNewConnection()
        {
            var config = ConfigurationOptions.Parse(ConstKey.ServerWriteIp);
            config.KeepAlive = 5;
            config.SyncTimeout = SyncTimeout;
            config.AbortOnConnectFail = false;
            config.AllowAdmin = true;
            config.ConnectTimeout = IoTimeOut;
            config.SocketManager = _socketManager;
            var connection = ConnectionMultiplexer.ConnectAsync(config);
            var muxer = connection.Result;
            return muxer;
        }
        public ConnectionMultiplexer GetConnection
        {
            get
            {
                lock (SyncConnectionLock)
                {
                    if (_connection == null)
                        _connection = GetNewConnection();
                    if (!_connection.IsConnected)
                        _connection = GetNewConnection();

                    if (_connection.IsConnected)
                        return _connection;
                    return _connection;
                }
            }
        }
        public IDatabase CurrentConnection()
        {
            var connection = ReplicationRedis.Current.GetConnection.GetDatabase(ConstKey.RedisDB);
            return connection;
        }
    }
}
