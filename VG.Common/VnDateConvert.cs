﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VG.Common
{
    public class VnDateConvert
    {
        public static readonly string VALUE_JAN_TEXT_VN = "tháng 01";
        public static readonly string VALUE_FEB_TEXT_VN = "tháng 02";
        public static readonly string VALUE_MAR_TEXT_VN = "tháng 03";
        public static readonly string VALUE_APR_TEXT_VN = "tháng 04";
        public static readonly string VALUE_MAY_TEXT_VN = "tháng 05";
        public static readonly string VALUE_JUN_TEXT_VN = "tháng 06";
        public static readonly string VALUE_JUL_TEXT_VN = "tháng 07";
        public static readonly string VALUE_AUG_TEXT_VN = "tháng 08";
        public static readonly string VALUE_SEP_TEXT_VN = "tháng 09";
        public static readonly string VALUE_OCT_TEXT_VN = "tháng 10";
        public static readonly string VALUE_NOV_TEXT_VN = "tháng 11";
        public static readonly string VALUE_DEC_TEXT_VN = "tháng 12";

        public static string ConvertDateTime(DateTime dtTime, string strFormat)
        {
            string returnValue = string.Empty;
            switch (strFormat)
            {
                case "tt_dd_month_yyyy":
                    { returnValue = ConvertDateTime01(dtTime); }
                    break;
                case "tt_dd.mm.yyyy":
                    { returnValue = ConvertDateTime02(dtTime); }
                    break;
                case "tt":
                    { returnValue = ConvertDateTime03(dtTime); }
                    break;
                case "dd.mm.yyyy":
                    { returnValue = ConvertDateTime04(dtTime); }
                    break;
                case "Calculate":
                    { returnValue = ConvertDateTime05(dtTime); }
                    break;
            }
            return returnValue;
        }
        #region ConvertDateTimeChildFunction
        public static string ConvertDateTime05(DateTime dtTime)
        {
            DateTime dtNow = DateTime.Now;
            string rt = string.Empty;
            if (DateTime.Compare(dtNow, dtTime) > 0)
            {
                TimeSpan ts = dtNow.Subtract(dtTime);
                if (ts.Days < 5)
                {
                    if (ts.Days > 0) rt = ConvertDateTime06(dtNow) + string.Format(" ({0} ngày trước)", ts.Days);
                    else if (ts.Hours > 0) rt = ConvertDateTime06(dtNow) + string.Format(" ({0} giờ trước)", ts.Hours);
                    else if (ts.Minutes > 0) rt = ConvertDateTime06(dtNow) + string.Format(" ({0} phúttrước)", ts.Minutes);
                    else
                    {
                        rt = ConvertDateTime06(dtNow) + "(1 phút trước)";
                    }
                }
                else rt = ConvertDateTime04(dtTime);
            }
            else
            {
                rt = ConvertDateTime06(dtNow)+ "(1 phút trước)";
            }
            return rt;

        }

        public static string ConvertDateTime04(DateTime dtTime)
        {
            int intDay = dtTime.Day;
            int intMonth = dtTime.Month;
            int intYear = dtTime.Year;
            string strResult = intDay.ToString() + "/" + intMonth.ToString() + "/" + intYear.ToString();
            return strResult;
        }

        public static string ConvertDateTime03(DateTime dtTime)
        {
            string ttime = dtTime.ToShortTimeString();
            return ttime;
        }

        public static string ConvertDateTime02(DateTime dtTime)
        {
            string ttime = dtTime.ToShortTimeString();
            int intDay = dtTime.Day;
            int intMonth = dtTime.Month;
            int intYear = dtTime.Year;
            string strResult = ttime + " Ngày " + intDay.ToString() + "." + intMonth.ToString() + "." + intYear.ToString();
            return strResult;
        }

        public static string ConvertDateTime01(DateTime dtTime)
        {
            string ttime = dtTime.ToShortTimeString();
            int intDay = dtTime.Day;
            int intMonth = dtTime.Month;
            int intYear = dtTime.Year;
            string strReturnValue = "";

            switch (intMonth)
            {
                case 1: strReturnValue = VALUE_JAN_TEXT_VN; break;
                case 2: strReturnValue = VALUE_FEB_TEXT_VN; break;
                case 3: strReturnValue = VALUE_MAR_TEXT_VN; break;
                case 4: strReturnValue = VALUE_APR_TEXT_VN; break;
                case 5: strReturnValue = VALUE_MAY_TEXT_VN; break;
                case 6: strReturnValue = VALUE_JUN_TEXT_VN; break;
                case 7: strReturnValue = VALUE_JUL_TEXT_VN; break;
                case 8: strReturnValue = VALUE_AUG_TEXT_VN; break;
                case 9: strReturnValue = VALUE_SEP_TEXT_VN; break;
                case 10: strReturnValue = VALUE_OCT_TEXT_VN; break;
                case 11: strReturnValue = VALUE_NOV_TEXT_VN; break;
                case 12: strReturnValue = VALUE_DEC_TEXT_VN; break;
            }
            string strResult = ttime + " Ngày " + intDay.ToString() + " - " + strReturnValue + " - " + intYear.ToString();
            return strResult;
        }

        public static string ConvertDateTime10(DateTime dtTime)
        {
            try
            {
                int intDay = dtTime.Day;
                int intMonth = dtTime.Month;
                int intYear = dtTime.Year;
                string strReturnValue = "";

                switch (intMonth)
                {
                    case 1: strReturnValue = VALUE_JAN_TEXT_VN; break;
                    case 2: strReturnValue = VALUE_FEB_TEXT_VN; break;
                    case 3: strReturnValue = VALUE_MAR_TEXT_VN; break;
                    case 4: strReturnValue = VALUE_APR_TEXT_VN; break;
                    case 5: strReturnValue = VALUE_MAY_TEXT_VN; break;
                    case 6: strReturnValue = VALUE_JUN_TEXT_VN; break;
                    case 7: strReturnValue = VALUE_JUL_TEXT_VN; break;
                    case 8: strReturnValue = VALUE_AUG_TEXT_VN; break;
                    case 9: strReturnValue = VALUE_SEP_TEXT_VN; break;
                    case 10: strReturnValue = VALUE_OCT_TEXT_VN; break;
                    case 11: strReturnValue = VALUE_NOV_TEXT_VN; break;
                    case 12: strReturnValue = VALUE_DEC_TEXT_VN; break;
                }
                string strResult = "ngày " + intDay.ToString() + " " + strReturnValue + " năm " + intYear.ToString();
                return strResult;
            }
            catch
            {
                return "";
            }
        }

        public static string ConvertDateTime06(DateTime dtTime)
        {
            int intDay = dtTime.Day;
            int intMonth = dtTime.Month;
            string strReturnValue = "";

            switch (intMonth)
            {
                case 1: strReturnValue = VALUE_JAN_TEXT_VN; break;
                case 2: strReturnValue = VALUE_FEB_TEXT_VN; break;
                case 3: strReturnValue = VALUE_MAR_TEXT_VN; break;
                case 4: strReturnValue = VALUE_APR_TEXT_VN; break;
                case 5: strReturnValue = VALUE_MAY_TEXT_VN; break;
                case 6: strReturnValue = VALUE_JUN_TEXT_VN; break;
                case 7: strReturnValue = VALUE_JUL_TEXT_VN; break;
                case 8: strReturnValue = VALUE_AUG_TEXT_VN; break;
                case 9: strReturnValue = VALUE_SEP_TEXT_VN; break;
                case 10: strReturnValue = VALUE_OCT_TEXT_VN; break;
                case 11: strReturnValue = VALUE_NOV_TEXT_VN; break;
                case 12: strReturnValue = VALUE_DEC_TEXT_VN; break;
            }
            string strResult = intDay.ToString() + " " + strReturnValue;
            return strResult;
        }
        #endregion

        public static DateTime DateOnly(DateTime dtTime)
        {
            return new DateTime(dtTime.Year, dtTime.Month, dtTime.Day);
        }

        /// <summary>
        /// Thứ Năm, ngày 11 tháng 04 năm 2019
        /// </summary>
        /// <param name="dtTime"></param>
        /// <returns></returns>
        public static string ConvertDateTimeTempVN(DateTime dtTime)
        {
            string dayofWeek = dtTime.DayOfWeek.ToString();
            int intDay = dtTime.Day;
            int intMonth = dtTime.Month;
            int intYear = dtTime.Year;
            string dayWeekVN = "";
            string monthVN = "";
            string strReturnValue = "";

            switch (dayofWeek)
            {
                case "Friday": dayWeekVN = "Thứ Sáu"; break;
                case "Monday": dayWeekVN = "Thứ Hai"; break;
                case "Saturday": dayWeekVN = "Thứ Bẩy"; break;
                case "Sunday": dayWeekVN = "Chủ Nhật"; break;
                case "Thursday": dayWeekVN = "Thứ Năm"; break;
                case "Tuesday": dayWeekVN = "Thứ Ba"; break;
                case "Wednesday": dayWeekVN = "Thứ Tư"; break;
            }
            switch (intMonth)
            {
                case 1: monthVN = VALUE_JAN_TEXT_VN; break;
                case 2: monthVN = VALUE_FEB_TEXT_VN; break;
                case 3: monthVN = VALUE_MAR_TEXT_VN; break;
                case 4: monthVN = VALUE_APR_TEXT_VN; break;
                case 5: monthVN = VALUE_MAY_TEXT_VN; break;
                case 6: monthVN = VALUE_JUN_TEXT_VN; break;
                case 7: monthVN = VALUE_JUL_TEXT_VN; break;
                case 8: monthVN = VALUE_AUG_TEXT_VN; break;
                case 9: monthVN = VALUE_SEP_TEXT_VN; break;
                case 10: monthVN = VALUE_OCT_TEXT_VN; break;
                case 11: monthVN = VALUE_NOV_TEXT_VN; break;
                case 12: monthVN = VALUE_DEC_TEXT_VN; break;
            }

            return strReturnValue = dayWeekVN + ", ngày " + intDay + " " + monthVN + " năm " + intYear;
        }

        public static string ConvertDateTimeTempEN(DateTime dtTime)
        {
            string temp =  string.Format("{0} {1} {2} {3}", dtTime.DayOfWeek,  dtTime.Day, dtTime.ToString("MMMM"), dtTime.Year);
            return temp;
        }

        public static string ConvertDateTimeTempEN2(DateTime dtTime)
        {
            string temp = string.Format("{0} {1} {2}", dtTime.Day, dtTime.ToString("MMMM"), dtTime.Year);
            return temp;
        }
    }
}
