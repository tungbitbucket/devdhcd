﻿using DataService.DomainModel;
using SelectPdf;
using System;
using VG.Common;

namespace ConsoleApp.Helper
{
    public class PrintingHelper
    {
        public static void printInvitationGet(mQInvitation mqInvi)
        {
            string urlGet = string.Format(ConstKey.printInvitationGet,mqInvi.EventID, mqInvi.StockHolderID);
            int webPageWidth = 1024;
            SelectPdf.PdfPageSize pageSize = (SelectPdf.PdfPageSize)Enum.Parse(typeof(SelectPdf.PdfPageSize), "A4", true);
            SelectPdf.PdfPageOrientation pdfOrientation = (SelectPdf.PdfPageOrientation)Enum.Parse(typeof(SelectPdf.PdfPageOrientation), "Portrait", true);
            SelectPdf.HtmlToPdf converter = new SelectPdf.HtmlToPdf();

            converter.Options.PdfPageSize = pageSize;
            converter.Options.PdfPageOrientation = pdfOrientation;
            converter.Options.WebPageWidth = webPageWidth;
            SelectPdf.PdfDocument docuri = converter.ConvertUrl(urlGet);
            byte[] pdf = docuri.Save();

            Spire.Pdf.PdfDocument doc = new Spire.Pdf.PdfDocument();
            doc.LoadFromBytes(pdf);
            doc.Print();
        }

        public static void printInvitationPost(mQInvitation mqInvi)
        {
            HtmlToPdf converter = new HtmlToPdf();
            converter.Options.HttpPostParameters.Add("obj", mqInvi.ToJson(false));

            SelectPdf.PdfDocument docuri = converter.ConvertUrl(ConstKey.printInvitationPost);
            Spire.Pdf.PdfDocument doc = new Spire.Pdf.PdfDocument();
            doc.LoadFromBytes(docuri.Save());
            doc.Print();
        }

        public static void DownloadPDF(mQInvitation2 mqInvi)
        {
            string urlGet = "";
            string strPath = "D:/VRE_INVI/";
            if (mqInvi.sType == 1)
            {
                urlGet = string.Format(ConstKey.printInvitationGet, mqInvi.EventID, mqInvi.StockHolderID);
                strPath += "VN/";
            }
            else
            {
                urlGet = string.Format(ConstKey.printInvitation2Get, mqInvi.EventID, mqInvi.StockHolderID);
                strPath += "EN/";
            }

            int webPageWidth = 1024;
            SelectPdf.PdfPageSize pageSize = (SelectPdf.PdfPageSize)Enum.Parse(typeof(SelectPdf.PdfPageSize), "A4", true);
            SelectPdf.PdfPageOrientation pdfOrientation = (SelectPdf.PdfPageOrientation)Enum.Parse(typeof(SelectPdf.PdfPageOrientation), "Portrait", true);
            HtmlToPdf converter = new HtmlToPdf();
            converter.Options.PdfPageSize = pageSize;
            converter.Options.PdfPageOrientation = pdfOrientation;
            converter.Options.WebPageWidth = webPageWidth;
            SelectPdf.PdfDocument docuri = converter.ConvertUrl(urlGet);
            docuri.Save(strPath + mqInvi.StockHolderID + "_Invitation.pdf");
        }
    }
}
