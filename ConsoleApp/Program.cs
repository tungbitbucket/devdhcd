﻿using ConsoleApp.Helper;
using DataService;
using DataService.DomainModel;
using EasyNetQ;
using System;
using System.Collections.Generic;
using VG.Common;

namespace ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var bus = RabbitHutch.CreateBus(ConstKey.RabbitConnect))
            {
                Console.WriteLine("Job Connected:" + bus.IsConnected);

                #region Listening
                string services = ConstKey.ServiceCollections;
                if (services != "")
                {
                    string[] se = services.Split(',');
                    foreach(string s in se)
                    {
                        switch (s)
                        {
                            case "print":
                                {
                                    Console.WriteLine("-$ Start service print");
                                    bus.Subscribe<mQInvitation>("PrintInvitation", PrintInvitation);
                                }
                                break;
                            case "genbarcode":
                                {
                                    Console.WriteLine("-$ Start gen barcode");
                                    bus.Subscribe<mQGenbarcode>("GenBarcode", GenBarcode);
                                }
                                break;
                        }
                    }
                }
                else
                {
                    Console.WriteLine("No service define.");
                }
                #endregion

                Console.WriteLine("Listening for messages. Hit <return> to quit.");
                Console.ReadLine();
            }
        }

        static void PrintInvitation(mQInvitation obj)
        {
            Console.WriteLine("******* PrintInvitation : {0}", obj.ToJson(false));
            PrintingHelper.printInvitationGet(obj);
            Console.WriteLine("******* EndPrint *********");
        }

        static void DownloadInvitation(mQInvitation2 obj)
        {
            Console.WriteLine("******* DownloadInvitation : {0}", obj.ToJson(false));
            PrintingHelper.DownloadPDF(obj);
            Console.WriteLine("******* EndDownload *********");
        }

        static void GenBarcode(mQGenbarcode obj)
        {
            List<tblStockholderInvite> listNoneBarcode = DataAccess.GetStockholderInviteNoneBarcode(obj.EventID);
            foreach (tblStockholderInvite cus in listNoneBarcode)
            {
                Console.WriteLine("Gen for:" + cus.SHID);
                cus.BarCode = CommonHelper.BarcodeToBase64(CommonHelper.HashEncodeId(cus.SHID));
            }
            DataAccess.GenBarcodeStockHolderInviteBulk(listNoneBarcode);
        }
    }
}
